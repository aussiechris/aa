/* list.h */

typedef struct node_t {
	int             value;
	struct node_t  *next;
}               node_t;
typedef struct {
	int             count;
	struct node_t  *head;
	struct node_t  *tail;
}               list_t;

void            ListInit(list_t *);
int             ListInsert(list_t*, int);
void            ListDisplay(list_t *);
void            ListFree(list_t *);
