/* Sample application using a list (of integers) module */
#include <stdio.h>
#include <stdlib.h>

#include "list.h"

int
main()
{
	list_t          lst;
	int             num;

	ListInit(&lst);
	while (fscanf(stdin, "%d", &num) == 1) {
		{
			if (!ListInsert(&lst, num)) {
				fprintf(stderr, "ListInsert failed! Aborting data "
						"entry!\n");
				break;
			}
		}
	}

	ListDisplay(&lst);
	ListFree(&lst);

	return EXIT_SUCCESS;
}

