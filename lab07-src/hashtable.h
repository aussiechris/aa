/***************************************************************************
 *
 *   File            : libset.h
 *
 ***************************************************************************/

/*** Miscelaneous Utility Functions ***/
#ifndef _HASHTABLE_H
#define _HASHTABLE_H

#define END  				0
#define LIST				1
#define REMOVE	        	2
#define LOOKUP      		3

#ifdef __cplusplus
extern          "C" {
#endif				/** cplusplus **/

#include "libutil.h"

#define DEFAULT_HASH_SIZE        128
#define MAX_LOAD_FACTOR          0.75

typedef struct entry_t {
	char* key;
	char* value;
    struct entry_t* next;
} entry_t;

typedef struct {
	/* size of the hash table */
	int size;        
	/* number of items in the hash table */
	int items;       
	entry_t** table;  /* table entries */
} hashtable_t;

/* basic hash table functions */
hashtable_t* hash_create(int size);
void hash_free(hashtable_t* htable);
int hash_calc(hashtable_t* htable,char* key);
hashtable_t* hash_insert(hashtable_t* htable,char* key,char* value);
void hash_delete(hashtable_t* htable,char* key);
void hash_lookup(hashtable_t* htable,char* key);
hashtable_t* hash_rehash(hashtable_t* htable);
void hash_dump(hashtable_t* htable);

#ifdef __cplusplus
}
#endif				/** cplusplus **/

#endif				/** _HASHTABLE_H **/
