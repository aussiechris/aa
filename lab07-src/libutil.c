
#include "libutil.h"

/*
 * safe_malloc ()
 * 
 * Call calloc () and abort if the specified amount of memory cannot be
 * allocated.
 */
void           *
safe_malloc(size_t size)
{
	void           *mem_block = NULL;
	if ((mem_block = calloc(1, size)) == NULL) {
		fprintf(stderr, "ERROR: safe_malloc(%lu) cannot allocate memory.",
			(unsigned long)size);
		exit(EXIT_FAILURE);
	}
	return (mem_block);
}

/*
 * safe_realloc ()
 * 
 * Call realloc () and abort if the specified amount of memory cannot be
 * allocated.
 */
void           *
safe_realloc(void *old_mem, size_t new_size)
{
	if ((old_mem = realloc(old_mem, new_size)) == NULL) {
		fprintf(stderr, "ERROR: safe_realloc() cannot allocate"
			" %u blocks of memory.\n", (unsigned int) new_size);
		exit(EXIT_FAILURE);
	}
	return (old_mem);
}

/*
 * safe_strdup ()
 * 
 * Safe version of strdup avoid buffer overflow, etc.
 * 
 */
char           *
safe_strdup(const char *str)
{
	char           *copy = NULL;

	if (str == NULL) {
		fprintf(stderr, "ERROR safe_strdup(): str == NULL");
		exit(EXIT_FAILURE);
	}
	copy = safe_malloc((strlen(str) + 1) * sizeof(char));

	(void) strcpy(copy, str);

	return (copy);
}


/* this is Sieve of Eratosthenes : http://www.algorithmist.com/index.php/Prime_Sieve_of_Eratosthenes */
/* public domain code taken from somewhere */
#define MAXN  100000000  /* maximum value of N */
#define P1    1562501    /* = ceil(MAXN/64) */
#define P2    50000000   /* = ceil(MAXN/2) */
#define P3    5000       /* = ceil(ceil(sqrt(MAXN))/2) */

static unsigned int sieve[P1];

#define GET(b) ((sieve[(b)>>5]>>((b)&31))&1)
int isprime(int p) { return p==2 || (p>2 && (p&1)==1 && (GET((p-1)>>1)==0)); }

int
getprime(int n)
{
    int i;
    static unsigned int generate = 1;
    
    if(generate) {
        uint32_t i, j, k;
        memset(sieve, 0, sizeof(sieve));
        for (k = 1; k <= P3; k++)
            if (GET(k)==0) for(j=2*k+1,i=2*k*(k+1);i<P2;i+=j) sieve[i>>5]|=1<<(i&31);
        generate = 0;
    }
    
    /* find the next prime larger than n */
    for(i=n;i<MAXN;i++)
        if( isprime(i) ) return i;
        
    fprintf(stderr,"No prime found larger than %d\n",n);
    exit(-1);
}

