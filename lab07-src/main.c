/***************************************************************************
 *
 *   File        : main.c
 *
 ***************************************************************************/
#include "libutil.h"
#include "hashtable.h"

#include <stdio.h>

#define INPUT_BUFFER_SIZE 500

void
print_help(FILE* fp)
{
    fprintf(fp,"Available commands:\n");
    fprintf(fp,"    delete <key>\n");
    fprintf(fp,"    lookup <key>\n");
    fprintf(fp,"    list\n");
    fprintf(fp,"    end/exit/quit\n");
}

static int
get_next_operation(FILE *fp, char** key)
{
    char buffer[INPUT_BUFFER_SIZE];
    char command[INPUT_BUFFER_SIZE];
    char keybuf[INPUT_BUFFER_SIZE];
    int op = -1;
	
    while( op == -1 && fgets(buffer,INPUT_BUFFER_SIZE,fp) != NULL ) {
        if( sscanf(buffer,"%s",command) == 1 ) {

            if( strncmp(command,"delete",6) == 0 ) {
               if( sscanf(buffer,"%s %s\n",command,keybuf) == 2) {
                    op = REMOVE;
                    *key = safe_strdup(keybuf);
               }
            }
            else if( strncmp(command,"lookup",6) == 0 ) {
               if( sscanf(buffer,"%s %s\n",command,keybuf) == 2) {
                    op = LOOKUP;
                    *key = safe_strdup(keybuf);
               }
            }
            else if( strncmp(command,"list",4) == 0 ) op = LIST;
            else if( strncmp(command,"end",3) == 0 ) op = END;
            else if( strncmp(command,"exit",4) == 0 ) op = END;
            else if( strncmp(command,"quit",4) == 0 ) op = END;
	        else
	        {
                print_help(fp);
	        }
        }
        if(op == -1 ) {
		    fprintf(stderr, "Did not recognize command. Enter valid command.\n");
        }
	}
    return op;
}

int
readfromfile(FILE* file,char** key,char** value)
{
    char keybuf[INPUT_BUFFER_SIZE];
    char valuebuf[INPUT_BUFFER_SIZE];
    int debug;
    
    if( (debug=fscanf(file,"<%50[^>]><%500[^>]>\n", keybuf, valuebuf)) == 2 ) {
        *key = safe_strdup(keybuf);
        *value = safe_strdup(valuebuf);
        return 1;
    }

    return 0;
}

int
main(int argc, char **argv)
{
    FILE* dfile;
    hashtable_t* htable;
    char* key;
    char* value;
    int op;

    if( argc != 2 ) {
        fprintf(stderr,"Usage: %s <datafile.dat>\n",argv[0]);
        exit(-1);
    }
    
    /* create data empty set_t */
    fprintf(stdout,"creating hash table.\n");
    htable = hash_create(DEFAULT_HASH_SIZE);

	/* Use fscanf to read all data values into the hash table */
    if( (dfile = fopen(argv[1],"r")) == NULL) {
        fprintf(stderr,"Error opening input file.\n");
        hash_free(htable);
        exit(EXIT_FAILURE);
    }

    fprintf(stdout,"reading data.\n");
    while( readfromfile(dfile,&key,&value) == 1 ) {
        htable = hash_insert(htable,key,value);
        free (key);
        free (value);
    }
    fclose (dfile);
    
    print_help(stderr);
    
    /* enter the 'mini' shell */
	while ((op = get_next_operation(stdin, &key)) != END)
	{
		if (op == REMOVE) {
            hash_delete(htable, key);
            free(key);
        }
		else if (op == LOOKUP) {
            hash_lookup(htable, key);
            free(key);
        }
		else if (op == LIST) {
            hash_dump(htable);
        }
		else { fprintf(stderr, "Unexpected operation.\n"); exit(EXIT_FAILURE); }
	}

  hash_free(htable);

	return (EXIT_SUCCESS);
}

