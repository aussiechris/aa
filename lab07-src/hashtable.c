/***************************************************************************
 *
 *   File        : hashtable.c
 *
 ***************************************************************************/
#include "hashtable.h"

/*
 *  create the hash table and initialise the parameters
 */
hashtable_t*
hash_create(int size)
{
    hashtable_t* htable;

    fprintf(stderr,"hash_create(): please implement me!\n");
    exit(-1);
    
    return htable;
}

/*
 *  free the hash table 
 */
void
hash_free(hashtable_t* htable)
{
    fprintf(stderr,"hash_free(): please implement me!\n");
    exit(-1);
}

/*
 *  this is the hash function. implement as described in the lab sheet
 */
int
hash_calc(hashtable_t* htable,char* key)
{
    int hashval;

    fprintf(stderr,"hash_calc(): please implement me!\n");
    exit(-1);
    
    return hashval;
}
 
/*
 *  insert into hash table and rehash if loadfactor too high.
 */
hashtable_t*
hash_insert(hashtable_t* htable,char* key,char* value)
{
    fprintf(stderr,"hash_insert(): please implement me!\n");
    exit(-1);

    /* rehash if too full */
    
    /* calculate the hash function */
    
    
    /*linear probing */

    /* insert the value */

    /* return htable if we created a new hash table due to rehashing */
    return htable;
}

/*
 *  delete a key + value pair from the hash table
 */
void
hash_delete(hashtable_t* htable,char* key)
{
    fprintf(stderr,"hash_delete(): please implement me!\n");
    exit(-1);
    /* calculate the hash function */
    
    /* look up item through linear probing */
    
    /* delete if we found the item */
}

/*
 *  check for membership of a key
 */
void
hash_lookup(hashtable_t* htable,char* key)
{
    fprintf(stderr,"hash_lookup(): please implement me!\n");
    exit(-1);
    
    /* calculate the hash function */
    
    /* look up item through linear probing */
    

    /* output key value pair if we found the item */
}

/*
 *  resize the hash table so the load factor is ''normal'' again
 */
hashtable_t*
hash_rehash(hashtable_t* htable)
{
    hashtable_t* newtable;

    fprintf(stderr,"hash_rehash(): please implement me!\n");
    exit(-1);

    /* create new hash table */

    /* calculate size so load factor is okay */

    /* traverse old hash table and insert key value pairs into new hash table */

    /* delete old hash table */
    
    return newtable;
}

/*
 *  resize the hash table so the load factor is ''normal'' again
 */
void
hash_dump(hashtable_t* htable)
{
    fprintf(stderr,"hash_dump(): please implement me!\n");
    exit(-1);
    /* output all key + value pairs */
}


