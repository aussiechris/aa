/***************************************************************************
 *
 *   File        : libsort.c
 *
 ***************************************************************************/
#include "libsort.h"
#include <assert.h>
#include <stdlib.h>
#include <string.h>

static int
compare_int_for_qsort(const void *a, const void *b)
{
	return (*(int*)a - *(int*)b);
}

/*
   bubble sort 
 */
void
sort_bubble(int* array,int n)
{
    int i,j,tmp;
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n - 1; j++)
        {
            if( array[j] > array[j+1] ) {
                tmp = array[j+1];
                array[j+1] = array[j];
                array[j] = tmp;
            }
        }
    }
}

/*
   libc quick sort
 */
void
sort_qsort(int* array,int n)
{
    qsort(array,n,sizeof(int),compare_int_for_qsort);
}

/*
   cocktail / shaker sort
 */
void
sort_cocktail(int* array,int n)
{
    fprintf(stderr,"sort_cocktail(): please implement me!\n");
}


/*
   merge sort
 */
void
sort_merge(int* array,int n)
{
    fprintf(stderr,"sort_merge(): please implement me!\n");
}


/*
 */
void
sort_quick(int* array,int n)
{
    fprintf(stderr,"sort_quick(): please implement me!\n");
}

