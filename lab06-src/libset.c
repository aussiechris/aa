/***************************************************************************
 *
 *   File        : libset.c
 *
 ***************************************************************************/
#include "libset.h"
#include <assert.h>

/** Basic Set Operations **/


/*
 * insert a new item into the set and adjust the size of the underlying
 * dynamic memory block if needed
 */
int
set_insert(set_t * set, int item, int *set_size)
{
	assert(set != NULL);

	/* check for lock before insert. This makes it a static dictionary */
	if (set->lock == TRUE) {
		fprintf(stderr, "Set is locked!\n");
		return (FALSE);
	}
	/* increase the size of the array if there is not enough space left */
	if (set->items == *set_size) {
		/* safe realloc has to be used here! */
		set->data = (int *) safe_realloc(set->data, *set_size * 2 
                                * sizeof(int));
		*set_size = *set_size * 2;
	}
	/* store the max item value in n_max.  not mandatory */
	if (item > set->n_max) {
		set->n_max = item;
	}
	/* add the item and increase the item counter */
	set->data[set->items] = item;
	set->items++;

	return (TRUE);
}

/*
 * create and initialize a set_t data structure of size 'size'
 */
set_t          *
set_create(int size)
{
	set_t          *newset;

	assert(size > 0);

	/* allocate the data structure first */
	newset = (set_t *) safe_malloc(sizeof(set_t));
	/*
	 * then allocate the dynamic memory block and initialize the other
	 * variables
	 */
	newset->data = (int *) safe_malloc(sizeof(int) * size);
	newset->items = 0;
	newset->lock = 0;
	newset->n_max = 0;

	return newset;
}

/* free all memory used by the set */
void
set_destroy(set_t * set)
{
	/* free data first */
	free(set->data);
	/* free memory used by the set data structure itself */
	free(set);
	return;
}

/*
 * freeze the set - reallocate the dynamic memory block in the set to a
 * contiguous memory block of the exact size of the array and 'lock' the data
 * structure
 */
int
set_freeze(set_t * set)
{
	/*
	 * reallocate to match the number of items in the data structure.
	 * safe_realloc has to be used here.
	 */
	set->data = safe_realloc(set->data, set->items * sizeof(int));
	set->lock = (TRUE);
	/* lock the set -> static dictionary */
	if (!set->data) {
		return (FALSE);
	}
	return (TRUE);
}
