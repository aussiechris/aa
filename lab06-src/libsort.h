/***************************************************************************
 *
 *   File            : libsort.h
 *
 ***************************************************************************/

/*** Miscelaneous Utility Functions ***/
#ifndef _LIBSORT_H_
#define _LIBSORT_H_

#ifdef __cplusplus
extern          "C" {
#endif			/** cplusplus **/

#include "libutil.h"

void sort_bubble(int* array,int n);
void sort_qsort(int* array,int n);
void sort_cocktail(int* array,int n);
void sort_merge(int* array,int n);
void sort_quick(int* array,int n);

#ifdef __cplusplus
}
#endif				/** cplusplus **/

#endif				/** _LIBSORT_H_ **/
