
#include "libutil.h"

/*
 * safe_malloc ()
 * 
 * Call calloc () and abort if the specified amount of memory cannot be
 * allocated.
 */
void           *
safe_malloc(size_t size)
{
	void           *mem_block = NULL;
	if ((mem_block = calloc(1, size)) == NULL) {
		fprintf(stderr, "ERROR: safe_malloc(%lu) cannot allocate memory.",
			(unsigned long)size);
		exit(EXIT_FAILURE);
	}
	return (mem_block);
}

/*
 * safe_realloc ()
 * 
 * Call realloc () and abort if the specified amount of memory cannot be
 * allocated.
 */
void           *
safe_realloc(void *old_mem, size_t new_size)
{
	if ((old_mem = realloc(old_mem, new_size)) == NULL) {
		fprintf(stderr, "ERROR: safe_realloc() cannot allocate"
			" %u blocks of memory.\n", (unsigned int) new_size);
		exit(EXIT_FAILURE);
	}
	return (old_mem);
}

/*
 * safe_strdup ()
 * 
 * Safe version of strdup avoid buffer overflow, etc.
 * 
 */
char           *
safe_strdup(const char *str)
{
	char           *copy = NULL;

	if (str == NULL) {
		fprintf(stderr, "ERROR safe_strdup(): str == NULL");
		exit(EXIT_FAILURE);
	}
	copy = safe_malloc((strlen(str) + 1) * sizeof(char));

	(void) strcpy(copy, str);

	return (copy);
}
