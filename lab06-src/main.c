/***************************************************************************
 *
 *   File        : main.c
 *
 ***************************************************************************/
#include "libutil.h"
#include "libset.h"
#include "libsort.h"

#include <stdio.h>
#include <sys/time.h>
#include <math.h>

#define GETOPT_FINISHED -1

enum mode_t {
	BUB,
	QSORT,
	COCKTAIL,
	MERGE,
	QUICK
};
               
typedef struct {
	char           *input_file;
	enum mode_t     mode;
    int             verbose;
} clo_t;

clo_t          *init_clo(void);
void            destroy_clo(clo_t * clo);
clo_t          *parse_args(int argc, char **argv);

static void
print_usage(const char *program)
{
	fprintf(stderr, "USAGE: %s [options]\n", program);
	fprintf(stderr, "  -m sort method [bubble, qsort, merge, cocktail, quick]\n");
    fprintf(stderr, "  -i input file\n");
	fprintf(stderr, "  -v verbose");
	fprintf(stderr, "\n");
	fprintf(stderr, "EXAMPLE: %s -m quick -i random.txt\n",
		program);
	fprintf(stderr, "\n");
	return;
}

/** Initialize resources for the command line options **/
clo_t          *
init_clo(void)
{
	clo_t*rv = NULL;
	rv = (clo_t *) safe_malloc(sizeof(clo_t));
	rv->input_file = NULL;
	rv->mode = BUB;
    rv->verbose = FALSE;
	return (rv);
}

/** Free all resources allocated for the command line options **/
void
destroy_clo(clo_t * noret)
{
	if (!noret) {
		fprintf(stderr, "WARNING: command line options does not exist!\n");
		return;
	}
	if (noret->input_file) {
		free(noret->input_file);
	}
	free(noret);
}

clo_t          *
parse_args(int argc, char **argv)
{
	int             opt = GETOPT_FINISHED;
	clo_t          *iopts = init_clo();

	if (argc <= 1) {
		print_usage(argv[0]);
        exit(-1);
	}
	while ((opt = getopt(argc, argv, "i:m:v")) != GETOPT_FINISHED) {
		switch (opt) {
		case 'i':
			iopts->input_file = safe_strdup(optarg);
			break;
		case 'm':
			if (strcmp(optarg, "bubble") == 0) {
				iopts->mode = BUB;
			} else if (strcmp(optarg, "qsort") == 0) {
				iopts->mode = QSORT;
			} else if (strcmp(optarg, "cocktail") == 0) {
				iopts->mode = COCKTAIL;
			} else if (strcmp(optarg, "merge") == 0) {
				iopts->mode = MERGE;
			} else if (strcmp(optarg, "quick") == 0) {
				iopts->mode = QUICK;
			} else {
				fprintf(stderr, "ERROR: mode <%s> unknown!\n", optarg);
				exit(EXIT_FAILURE);
			}
			break;
		case 'v':
			iopts->verbose = TRUE;
			break;
		default:
			print_usage(argv[0]);
			exit(EXIT_FAILURE);
		}
	}
	return (iopts);
}

int
main(int argc, char **argv)
{
    FILE* dfile;
    set_t* dataset;
    int dsize;
    int i;
    int read;

	/* Parse Arguments */
	clo_t* iopts = parse_args(argc, argv);

    /* create data empty set_t */
    dsize = DEFAULT_SET_SIZE;
    dataset = set_create(dsize);

	/* Use fscanf to read all data values into new set_t */
    if( (dfile = fopen( iopts->input_file,"r")) == NULL) {
        fprintf(stderr,"Error opening input file.\n");
        set_destroy(dataset);
        exit(EXIT_FAILURE);
    }

    while( (fscanf(dfile,"%d\n",&read))==1 ) {
        set_insert(dataset,read,&dsize);
    }

	/*
	 * Freeze Set - reallocate so the set uses one contiguous memory
	 * block of the exact size
	 */
    set_freeze(dataset);

    /* switch the modes and do the sorting */
    switch(iopts->mode) {
        case BUB:
            sort_bubble(dataset->data,dataset->items);
        break;
        case QSORT:
            sort_qsort(dataset->data,dataset->items);
        break;
        case COCKTAIL:
            sort_cocktail(dataset->data,dataset->items);
        break;
        case MERGE:
            sort_merge(dataset->data,dataset->items);
        break;
        case QUICK:
            sort_quick(dataset->data,dataset->items);
        break;
        default:
            fprintf(stderr,"Error: Unknown mode\n"); 
            set_destroy(dataset);
            exit(EXIT_FAILURE);
    }

    if(iopts->verbose) {
        for(i=0;i<dataset->items;i++) {
            fprintf(stdout,"%d\n",dataset->data[i]);
        }
    }
    
	/* Make sure you free all malloc'd resources and close open files */
	destroy_clo(iopts);

    /* free all sets */
    set_destroy(dataset);

	return (EXIT_SUCCESS);
}

