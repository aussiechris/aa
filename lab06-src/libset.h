/***************************************************************************
 *
 *   File            : libset.h
 *
 ***************************************************************************/

/*** Miscelaneous Utility Functions ***/
#ifndef _LIBSET_H_
#define _LIBSET_H_

#ifdef __cplusplus
extern          "C" {
#endif				/** cplusplus **/

#include "libutil.h"

#define DEFAULT_SET_SIZE        128

	/** Basic Set Type **/
	typedef struct {
		int            *data;	/* set elements */
		int             items;	/* number of items in the set */
		int             n_max;	/* value of the maximum item */
		int             lock;	/* set state */
	}               set_t;

	/** Basic Set Operations **/
	int             set_insert(set_t * set, int item, int *set_size);
	set_t          *set_create(int size);
	void            set_destroy(set_t * set);
	int             set_freeze(set_t * set);

#ifdef __cplusplus
}
#endif				/** cplusplus **/

#endif				/** _LIBSET_H_ **/
