/***************************************************************************
* COSC1100 - Algorithms and Analysis
* Summer Semester 2014 Assignment #2 - Graph Structures
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1100
* Program Code     : BP094
***************************************************************************/

#include "linked_list.h"

void populate_pages(page_t *pages, pageidlist_t *pageid_list)
{
    int i=0,j=0,pidarraypos;
    pageid_t *thispid;
    
    
    /* iterate through all page id's */
    for(i=0; i<PID_HASH_SIZE; i++)
    {   
        if(pageid_list->table[i].count>0)
        {
            thispid=pageid_list->table[i].next;
            while(thispid)
            {
                pidarraypos=thispid->pageid-1;
                pages[pidarraypos].url=malloc(strlen(thispid->pageurl)+1);
                pages[pidarraypos].pagerank=0.5;
                strcpy(pages[pidarraypos].url,thispid->pageurl);
                thispid=thispid->next;
                j++;
            }
        }
    }
    printf("Created %i unique pages in the database\n",j);
}

void add_links(page_t *pages, filestructhead_t *data, pageidlist_t* pageid_list)
{
    filestruct_t* this=data->head;
    pageid_t *from, *to;
    int linkcount=0;
    while(this)
    {
        /* get the page and outgoing link */
        from = pageid_exists(this->pageurl, pageid_list);
        to =  pageid_exists(this->linkurl, pageid_list);
        /* add non self links */
        if(from->pageid!=to->pageid && add_link(pages,from->pageid,to->pageid))
            linkcount++;
        this=this->next;
    }
    printf("Added %i links to the database\n",linkcount);
}

BOOLEAN add_link(page_t *pages, int page_from, int page_to)
{
    int count, i;
    page_t *from, *to;
    BOOLEAN valid_link=TRUE;
    
    
    from=&pages[page_from-1];
    to=&pages[page_to-1];
        
    /* check for duplicate links */
    for (i=0;i<from->outlink_count;i++)
    {
        if(from->outlink[i]==page_to)
        {
            /* link is a duplicate, not valid */
            valid_link=FALSE;
        }
    }
    
    /* insert unique links */
    if(valid_link)
    {
        /* add outgoing link from page_from */
        count=++from->outlink_count;
        from->outlink=realloc(from->outlink,(sizeof(int)*(count)));
        assert(from->outlink);
        from->outlink[count-1]=page_to;
        
        /* add incoming link to page_to */
        count=++to->inlink_count;
        to->inlink=realloc(to->inlink,(sizeof(int)*(count)));
        assert(to->inlink);
        to->inlink[count-1]=page_from;
        return TRUE;
    }
    return FALSE;
}

/* get anchors and put them in the words database */
void get_words(wordlist_t *words, filestructhead_t *data, pageidlist_t* pageid_list)
{
    filestruct_t* this=data->head;
    char    *word;
    int     word_count=0,unique_words=0,to_page;
    
    
    /* for all entries in data, get the anchor text */
    while(this)
    {   
        /* if there is anchor text, tokenize first word */
        if((word = strtok (this->anchortext," \r\n")))
        {
            to_page = (pageid_exists(this->linkurl, pageid_list))->pageid;
            /* add the first word */
            if(add_word(word,words,to_page))
                unique_words++;
            word_count++;
            
            /* tokenize and add remaining words */
            
            while((word = strtok (NULL," \r\n")))
            {
                if(add_word(word,words,to_page))
                    unique_words++;
                word_count++;
            }
        }
        this=this->next;
    }
    
    printf("Added %i anchor words to the database\n",word_count);
    printf("Added %i unique words to the database\n",unique_words);
}

BOOLEAN add_word(char *word, wordlist_t *words_list, int doc)
{
    /* TODO */

    word_t *thisword, *newword;
    int hash;
    BOOLEAN duplicate_word=FALSE, add_index=TRUE;
    invindex_t *index, *newindex;
    
    /* hash the word */
    hash=hash_string(word, WORDS_HASH_SIZE);
    thisword = words_list->table[hash].next;
    
    if((word_exists(word, thisword)))
        duplicate_word=TRUE;
      
    if(duplicate_word)
    {
        /* count document frequency for duplicate words */
        index=thisword->head;
        while(index)
        {
            if (index->document==doc)
            {
                index->freq++;
                add_index=FALSE;
                break;
            }
            else
            {
                index=index->next;
            }
        }
    }
    else
    {
        /* new word found! */
        
        /* update collision stats */
        if(words_list->table[hash].count)
        {
            words_list->collisions++;
            words_list->table[hash].collisions++;
            if (words_list->maxcollisions < words_list->table[hash].collisions)
                words_list->maxcollisions = words_list->table[hash].collisions;
        }
        
        /* allocate memory for this word_t struct in the table hash*/
        if(!(newword=calloc(1,sizeof(word_t))))
        {
            printf("Cant malloc word_t\n");
            exit(EXIT_FAILURE);
        }
                
        /* allocate memory new word string */
        if(!(newword->word=malloc((strlen(word)+1)*sizeof(char*))))
        {
            printf("Cant malloc word char*\n");
            exit(EXIT_FAILURE);
        }
        
        /* set data for this word */
        strcpy(newword->word,word);
        newword->next = words_list->table[hash].next;
        words_list->table[hash].next = newword;
        
        /* update stats */
        words_list->count++;
        words_list->table[hash].count++;  
        if (words_list->maxcount < words_list->table[hash].count)
            words_list->maxcount = words_list->table[hash].count;
        add_index=TRUE;
        thisword=newword;
    }
    
    /* no links from this word to this page yet, add a node in the inverted index */
    if(add_index)
    {
        /* add new inverted index */
        if(!(newindex=malloc(sizeof(invindex_t))))
        {
            printf("Cant malloc inverted index \n");
            exit(EXIT_FAILURE);
        }
        newindex->document=doc;
        newindex->freq=1;
        newindex->next=thisword->head;
        thisword->head=newindex;
        
        /* update word frequency counts */
        thisword->freq++;
        thisword->head->freq++;
    }
        
    /* return TRUE if this is a unique word */
    if(duplicate_word)
        return FALSE;
    else
        return TRUE;
}

void print_words_table(wordlist_t *words_list)
{
    int i, j=0;
    word_t *thisword;
    invindex_t *thisindex;
    
    /* free the words database */
    for(i=0; i<WORDS_HASH_SIZE; i++)
    {   
        printf("---- Hash %i ----\n",i);
        if(words_list->table[i].count>0)
        {
            thisword=words_list->table[i].next;
            while(thisword)
            {
                printf("HashID: %i ",i);
                printf("Word: %s x %i ",thisword->word,thisword->freq);
                /* print inverted index list */
                thisindex=thisword->head;
                printf("Index: ");
                while(thisindex)
                    {
                        printf("<%i,%i> ",thisindex->document,thisindex->freq);
                        thisindex=thisindex->next;
                    }
                j++;
                thisword=thisword->next;
                printf("\n");
            }
        }
        
    }
    printf("Printed %i words\n",j);
}

word_t* word_exists(char *word, word_t *thisword)
{
        while(thisword)
        {
            if(!strcmp(word,thisword->word))
            {
                return thisword;
            }
            thisword=thisword->next;
        }
    
    return NULL;
}
