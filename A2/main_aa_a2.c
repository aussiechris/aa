/***************************************************************************
* COSC1100 - Algorithms and Analysis
* Summer Semester 2014 Assignment #2 - Graph Structures
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1100
* Program Code     : BP094
***************************************************************************/
/*TODO - BUG - page ID  338 (small_graph.txt line 44) 	http://www.shortcourses.rmit.edu.au/search_course_interest_discpline.php?codeid=566&cbs=95d5c90b9bb3a6cafc4020d2c7ad1853
does not follow same pre address format and needs to be treated differently */

#include "main_aa_a2.h"

int main()
{
    filestructhead_t* datalist=NULL;
    int total_links,unique_pages,input_status=0;
    pageidlist_t* pageid=NULL;
    page_t* pages=NULL;
    wordlist_t* words=NULL;
    char* user_input=NULL;
    
    /* initialize data structures */
    init(&datalist,&pageid,&user_input);
    
    
    /* load data to temporary data structure*/
    printf("Welcome to Chrissle web search!\n");
    printf("Please wait while we load the data...\n\n");
    printf("Loading data file\n");
    if(!(total_links=load_data_file(datalist, DATA_FILE, pageid)))
    {
        printf("Oh Noes! There was an error loading data!\nExiting...\n");
        return EXIT_FAILURE;
    }
    
    printf("Loaded %i lines from %s OK!\n",total_links,DATA_FILE);
    
    
    /* assign pageid's for all pages */
    printf("Assigning page IDs\n");
    printf("Indexed %i predefined page IDs...\n",(pageid->count));
    if(!(assign_pageid(datalist, pageid)))
    {
        printf("Oh Noes! There was an error assigning page IDs!\nExiting...\n");
        return EXIT_FAILURE;
    }
    unique_pages=pageid->count;
    printf("Indexed a total of %i page IDs OK!\n",unique_pages);
    
    /* initialise pages database */
    init_pages(&pages,pageid->count);
    
    /* populate page database from ID hash table */
    printf("Setting up pages database\n");
    populate_pages(pages, pageid);
    
    printf("Setting up links\n");
    add_links(pages, datalist, pageid);
    
    /* initialise words database */
    init_words(&words);
    
    /* get words and insert them into the database */
    printf("Setting up words database\n");
    get_words(words, datalist, pageid);
    
    /*printf("-- WORDS HASH TABLE --\n");
    printf("Table size: %i\n",WORDS_HASH_SIZE);
    printf("Total words: %i\n",words->count);
    printf("Max words: %i\n",words->maxcount);
    printf("Total collisions: %i\n",words->collisions);
    printf("Max collisions: %i\n",words->maxcollisions);
    
    print_words_table(words);*/
    
    /* free temp memory */
    printf("Freeing temporary memory\n");
    free_temp_memory(datalist,pageid);
    
    /* run page rank */
    printf("Ranking pages\n");
    
    printf("\n\nReady to search!\n");
    do
    {
    
        if(!user_input)
            user_input = malloc(sizeof(char)*USER_INPUT_BUFFER);
    
        if(input_status==INPUT_SUCCESS)
        {
            /* return results here */
            search_for(user_input, pages, words);
        }
        printf("Please enter a word (or words): ");
    }
    while((input_status=get_user_input(user_input)));
    printf("\n\nExiting program...\n\n");
    
    /* free memory */
    printf("Freeing memory\n");    
    free_memory(pages,words,unique_pages,user_input);
    
    printf("Goodbye!\n");
    return EXIT_SUCCESS;
}
