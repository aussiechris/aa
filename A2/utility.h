/***************************************************************************
* COSC1100 - Algorithms and Analysis
* Summer Semester 2014 Assignment #2 - Graph Structures
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1100
* Program Code     : BP094
***************************************************************************/
#ifndef UTILITY_H
#define UTILITY_H

#include "main_aa_a2.h"

/* function prototypes go here */
void init(filestructhead_t **data, pageidlist_t **pageid_list, char** user_input);
void    init_pages(page_t **pages, int pageqty);
void    init_words(wordlist_t **words);
int     load_data_file(filestructhead_t *data, char * datafile, pageidlist_t* pageid_list);
int     assign_pageid(filestructhead_t *data, pageidlist_t* pageid_list);
void    free_temp_memory(filestructhead_t *data, pageidlist_t *pageid_list);
void    free_memory(page_t *pages, wordlist_t *words, int pagecount, char* user_input);
int     get_user_input(char* user_input);
void    read_rest_of_line(void);
char* string_to_lower(char* input);

#endif