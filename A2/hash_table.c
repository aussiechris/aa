/***************************************************************************
* COSC1100 - Algorithms and Analysis
* Summer Semester 2014 Assignment #2 - Graph Structures
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1100
* Program Code     : BP094
***************************************************************************/

#include "hash_table.h"
/**
 * The hash function from assignment1 example solution.
 *
 */
int hash_string(char* input, int hashsize)
{
    unsigned int hashval = 0x811c9dc5;
    int i = 0;
    
    while ( i < strlen( input ) )
    {
        hashval += ( int ) input[ i ] * 0x01000193 * hashval;
        
        i++;
    }
    
    hashval = hashval % hashsize;
    
    return (int)hashval;
}

/* hash table functions for pageids */
pageid_t* insertID(pageidlist_t* pageid_list, char* url, int ID)
{
    pageid_t *thispid;
    int hash;
    
    hash=hash_string(url, PID_HASH_SIZE);
    if (pageid_list->table[hash].next)
    {
        thispid=pageid_list->table[hash].next;
        while(thispid->next)
        {
            thispid=thispid->next;
        }
        pageid_list->collisions++;
        pageid_list->table[hash].collisions++;
        if (pageid_list->maxcollisions < pageid_list->table[hash].collisions)
            pageid_list->maxcollisions = pageid_list->table[hash].collisions;
            
        if(!(thispid->next=malloc(sizeof(pageid_t))))
        {
            printf("Cant malloc hashID\n");
            exit(EXIT_FAILURE);
        }
        thispid=thispid->next;
    }
    else
    {
        if(!(pageid_list->table[hash].next=malloc(sizeof(pageid_t))))
        {
            printf("Cant malloc hashID\n");
            exit(EXIT_FAILURE);
        }
        thispid=pageid_list->table[hash].next;
    }
    
    if(!(thispid->pageurl=malloc((strlen(url)+1)*sizeof(char*))))
    {
        printf("Cant malloc hashID\n");
        exit(EXIT_FAILURE);
    }
    
    strcpy(thispid->pageurl,url);
    thispid->pageid=ID;
    thispid->next=NULL;
  
    
    
    pageid_list->count++;
    pageid_list->table[hash].count++;  
    if (pageid_list->maxcount < pageid_list->table[hash].count)
        pageid_list->maxcount = pageid_list->table[hash].count;
    return thispid;
}

pageid_t * pageid_exists(char* url, pageidlist_t* pageid_list)
{
    pageid_t *thispid;
    int hash;
    
    hash=hash_string(url,PID_HASH_SIZE);
    
    if (pageid_list->table[hash].next)
    {   
        /* page(s) exist in this hash position, search for a match */
        thispid=pageid_list->table[hash].next;
        while(thispid)
        {
            if (!strcmp(thispid->pageurl,url))
            {
                /* match found */
                return thispid;
            }
            thispid=thispid->next;
        }
    }
    /* no matches in the hash table */
    return NULL;  
}

void free_pageids(pageidlist_t* pageid_list)
{
    int i=0,j=0;
    pageid_t *thispid, *prevpid;
    
    /* free page id's structures */
    for(i=0; i<PID_HASH_SIZE; i++)
    {   
        if(pageid_list->table[i].count>0)
        {
            thispid=pageid_list->table[i].next;
            while(thispid)
            {
                prevpid=thispid;
                thispid=thispid->next;
                free(prevpid->pageurl);
                free(prevpid);
                j++;
            }
        }
        
    }
    printf("Freed %i page IDs\n",j);
    free(pageid_list->table);
    free(pageid_list);
}