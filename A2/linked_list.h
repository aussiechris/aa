/***************************************************************************
* COSC1100 - Algorithms and Analysis
* Summer Semester 2014 Assignment #1 - Data Structure Performance
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1100
* Program Code     : BP094
***************************************************************************/
#ifndef LLIST_H
#define LLIST_H

#include "main_aa_a2.h"

filestruct_t*   initialize_list();
BOOLEAN         load_list(filestruct_t* this, char * line);
void            free_list(filestructhead_t* head);

#endif