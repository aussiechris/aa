/***************************************************************************
* COSC1100 - Algorithms and Analysis
* Summer Semester 2014 Assignment #2 - Graph Structures
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1100
* Program Code     : BP094
***************************************************************************/

#include "main_aa_a2.h"

void init(filestructhead_t **data, pageidlist_t **pageid_list, char** user_input)
{
    /* malloc data structures */
    *data = calloc(1,sizeof(filestructhead_t));
    *pageid_list = calloc(1,sizeof(pageidlist_t));
    (*pageid_list)->table = calloc(PID_HASH_SIZE,sizeof(pageidtable_t));
    assert(*data);
    assert(*pageid_list);
    assert((*pageid_list)->table);
    assert(user_input);
}

void init_pages(page_t **pages, int pageqty)
{
    *pages = calloc(pageqty,sizeof(page_t));
    assert(*pages);
}

void    init_words(wordlist_t **words)
{
    *words = calloc(1,sizeof(wordlist_t));
    (*words)->table = calloc(WORDS_HASH_SIZE,sizeof(wordtable_t));
    assert(*words);
    assert((*words)->table);
}
    
int load_data_file(filestructhead_t *data, char * datafile, pageidlist_t* pageid_list)
{
    /*****************************************************
    **load_data_file()
    * Create a temporary database and populate it with data
    * from the supplied datafile.
    *
    * Returns a pointer to the database once it is populated
    *
    */
    
    char line[INPUT_BUFFER+1];
    FILE *fp;
    filestruct_t *this;
    int lastpageid=0;
    
    this = data->head;    
    data->count=0;
    
    /* Open data file and return any errors */
    if (!(fp = fopen(datafile, "r")))
    {
        printf ("Error opening data file %s\n",datafile);
        return 0;
    }
    printf ("Successfully opened %s\n",datafile);
    
    /* Insert data from data file into linked list */
    while (fgets(line, INPUT_BUFFER, fp))
    {
        /* check for null char */
        if(line[0])
        {
            /* Initialise this node */
            if(!(this=initialize_list()))
            {
                printf ("Error initalizing a node in the linked list.\n");
                return 0;
            }
            
            /* load data into this node */
            if(!load_list(this, line))
            {
                printf ("Error loading data to the linked list.\n");
                return 0;
            }
            /* insert this node into the list */
            this->next = data->head;
            data->head = this;
            
            /*check for unique ID's*/
            if(this->pageid!=lastpageid)
            {
                /* unique id found - hash and store ID */
                insertID(pageid_list,this->pageurl,this->pageid);
            }
            lastpageid=this->pageid;
            data->count++;
        }            
    }
    fclose(fp);
        
    return data->count;
}

int assign_pageid(filestructhead_t *data, pageidlist_t* pageid_list)
{
    int preIDcount;
    filestruct_t* thisdata;
    
    preIDcount=pageid_list->count;
    
    /* set first node */
    thisdata = data->head;
    /* search for pages with undefined pageIDs */
    while(thisdata)
    {
        if(!pageid_exists(thisdata->linkurl, pageid_list))
        {
            /* unique id found - hash and store ID */
            insertID(pageid_list,thisdata->linkurl,pageid_list->count+1);
        }
        thisdata = thisdata->next;
    }
    
    printf("Indexed %i undefined page IDs...\n",(pageid_list->count-preIDcount));
    /*printf("There were %i collisions\n",pageid_list->collisions);
    printf("There were %i max collisions on a single hash\n",pageid_list->maxcollisions);
    printf("There were %i max nodes on a single hash\n",pageid_list->maxcount);*/
    
    return pageid_list->count;
}

void free_temp_memory(filestructhead_t *data, pageidlist_t *pageid_list)
{
    /* free pageID table */
    free_pageids(pageid_list);
    /* free temp data */
    free_list(data);
}

void free_memory(page_t *pages, wordlist_t *words, int pagecount, char* user_input)
{
    int i, j=0;
    word_t *thisword, *prevword;
    invindex_t *thisindex, *previndex;
    
    /* free the pages database */
    for(i=0; i<pagecount; i++)
    {   
        free(pages[i].url);
        free(pages[i].outlink);
        free(pages[i].inlink);
        j++;
    }
    free(pages);
    printf("Freed %i pages\n",j);
    
    /* free the words database */
    j=0;
    for(i=0; i<WORDS_HASH_SIZE; i++)
    {   
        if(words->table[i].count>0)
        {
            thisword=words->table[i].next;
            while(thisword)
            {
                prevword=thisword;
                thisword=thisword->next;
                thisindex=prevword->head;
                while(thisindex)
                    {
                        previndex=thisindex;
                        thisindex=thisindex->next;
                        free(previndex);
                    }
                free(prevword->word);
                free(prevword);
                j++;
            }
        }
        
    }
    printf("Freed %i words\n",j);
    /* TODO - free each word */
    free(words->table);
    free(words);
    free(user_input);
}

/**************************************************************************
* get_user_input() - get and validate length of user input
* Returns int with user input success status
* Clears buffer if the user enters more than the specified character length
**************************************************************************/
int get_user_input(char* user_input)
{
    char temp_user_input[USER_INPUT_BUFFER];
    char *stripped_input;
    
    /* Get user input and check it isn't empty */
    if(fgets(temp_user_input, USER_INPUT_BUFFER, stdin) == NULL 
            || temp_user_input[0] == '\n')
    {
        /* If the user entered nothing - return to menu and exit */
        return EXIT_PROGRAM;
    }
    /* check for buffer overflow (i.e. \n character exists) */
    else if(
        temp_user_input[strlen(temp_user_input)-1] 
        != '\n' )
    {
        /* Buffer has overflown! Report error */ 
        printf("\nYou entered too many characters!\
                \nClearing the input buffer\n");
        /* Clear buffer */
        read_rest_of_line();
        return INPUT_ERROR;
    }
    /* remove \n char and trailing string contents */
    stripped_input = strtok(temp_user_input,"\n");assert(user_input);
    strcpy(user_input,stripped_input);
    return INPUT_SUCCESS;
}

/***************************************************************************
* read_rest_of_line() - reads characters from the input buffer until 
* all characters have been cleared. You will need to call this in 
* association with fgets().
***************************************************************************/
void read_rest_of_line(void)
{
    int ch;
    while(ch=getc(stdin), ch!=EOF && ch!='\n');
    clearerr(stdin);
}

/***************************************************************************
* string_to_lower() - makes string lower case
***************************************************************************/
char* string_to_lower(char* input)
{
    int     i;
    
    for(i=0;input[i];i++)
    {
        input[i]=tolower(input[i]);
    }
    
    return input;
}