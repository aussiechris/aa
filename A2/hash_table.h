/***************************************************************************
* COSC1100 - Algorithms and Analysis
* Summer Semester 2014 Assignment #2 - Graph Structures
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1100
* Program Code     : BP094
***************************************************************************/
#ifndef HTABLE_H
#define HTABLE_H

#include "main_aa_a2.h"

/* function prototypes go here */
int hash_string(char* input, int hashsize);
pageid_t*   insertID(pageidlist_t* pageid_list, char* url, int ID);
pageid_t *  pageid_exists(char* url, pageidlist_t* pageid_list);
void        free_pageids(pageidlist_t* pageid_list);
 
#endif