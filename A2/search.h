/***************************************************************************
* COSC1100 - Algorithms and Analysis
* Summer Semester 2014 Assignment #2 - Graph Structures
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1100
* Program Code     : BP094
***************************************************************************/
#ifndef SEARCH_H
#define SEARCH_H

#include "main_aa_a2.h"
void search_for(char* search_term, page_t* pages, wordlist_t* words);
invindex_t* add_search_result(int doc, int freq);
invindex_t* remove_next_result(invindex_t* prev);
invindex_t* store_search_results(invindex_t *results, word_t *thisword);
invindex_t* compare_search_results(invindex_t *results_a, word_t *thisword);
#endif