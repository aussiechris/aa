/***************************************************************************
* COSC1100 - Algorithms and Analysis
* Summer Semester 2014 Assignment #2 - Graph Structures
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1100
* Program Code     : BP094
***************************************************************************/

#include "linked_list.h"

filestruct_t* initialize_list()
{
    /*****************************************************
    **initialize_list()
    * Allocate memory for and initialize values of our 
    * linked list
    * 
    */
    
    filestruct_t* list;
    
    if(!(list = malloc(sizeof(filestruct_t))))
        return NULL;
    
    list -> pageid=0;
    list -> pageurl=NULL;
    list -> linkurl=NULL;
    list -> anchortext=NULL;
    list -> next=NULL;
    return list;
}

BOOLEAN load_list(filestruct_t* this, char * line)
{
    /*****************************************************
    **load_list()
    * Allocate memory for and load values into our 
    * linked list
    * 
    */
    
    char* item=NULL;
    size_t itemsize;
    
    /* parse each line and insert it into the linked list */
    if(!(item = strtok (line,"\t")))
        return FALSE;
    this->pageid= (int)strtol(item,NULL,10);
    
    if(!(item = strtok (NULL,"\t")))
        return FALSE;
    itemsize = ((strlen(item+BASE_URL_LEN)+1)*sizeof(char));
    if(!(this->pageurl = malloc(itemsize)))
        return FALSE;
    strcpy(this->pageurl,item+BASE_URL_LEN);
    
    if(!(item = strtok (NULL,"\t")))
        return FALSE;
    itemsize = ((strlen(item+BASE_URL_LEN)+1)*sizeof(char));
    if(!(this->linkurl = malloc(itemsize)))
        return FALSE;
    strcpy(this->linkurl,item+BASE_URL_LEN);
    
    if(!(item = strtok (NULL,"\t\r\n")))
        return TRUE;
    itemsize = ((strlen(item)+1)*sizeof(char));
    if(!(this->anchortext = malloc(itemsize)))
        return FALSE;
    strcpy(this->anchortext,string_to_lower(item));
    return TRUE;
}

void free_list(filestructhead_t* head)
{
    filestruct_t* next, *this=head->head;
    int i=0;
    while(this)
    {
        next=this->next;
        free(this->pageurl);
        free(this->linkurl);
        free(this->anchortext);
        free(this);
        this=next;
        i++;
    }
    printf("Freed %i lines of temporary data\n",i);
    free(head);
}
