/***************************************************************************
* COSC1100 - Algorithms and Analysis
* Summer Semester 2014 Assignment #2 - Graph Structures
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1100
* Program Code     : BP094
***************************************************************************/
#ifndef DATA_H
#define DATA_H

#include "main_aa_a2.h"

void    populate_pages(page_t *pages, pageidlist_t *pageid_list);
void    add_links(page_t *pages, filestructhead_t *data, pageidlist_t* pageid_list);
BOOLEAN add_link(page_t *pages, int page_from, int page_to);
void    get_words(wordlist_t *words, filestructhead_t *data, pageidlist_t* pageid_list);
BOOLEAN add_word(char *word, wordlist_t *words_list, int doc);
void    print_words_table(wordlist_t *words_list);
word_t* word_exists(char *word, word_t *thisword);
#endif