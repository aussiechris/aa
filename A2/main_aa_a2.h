/***************************************************************************
* COSC1100 - Algorithms and Analysis
* Summer Semester 2014 Assignment #2 - Graph Structures
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1100
* Program Code     : BP094
***************************************************************************/
#ifndef MAIN_AA_A2_H
#define MAIN_AA_A2_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <sys/time.h>
#include <ctype.h>

/* specifies the file names to load */
#define DATA_FILE "data/graph.txt"
#define BASE_URL_LEN 22
#define BASE_URL "http://www.rmit.edu.au"

/* Specifies the input buffer for reading files */
#define INPUT_BUFFER 1000
#define MAX_ITEM 600

/* Specifies the input buffer for user entry */
#define USER_INPUT_BUFFER 128

/* specifies error codes for user input */
#define EXIT_PROGRAM 0
#define INPUT_ERROR 1
#define INPUT_SUCCESS 2

/* specifies size of hash tables */
#define PID_HASH_SIZE 10008
#define WORDS_HASH_SIZE 10008

/* structure for boolean type */
/* supplied as example code by Paul */
/* from PT last semester */
typedef enum truefalse
{
    FALSE, TRUE
} BOOLEAN;

/* structure for holding info about each page and it's graph structure */
typedef struct page
{
    char*   url;
    double  pagerank;
    int     pagesink;
    int     inlink_count;
    int     outlink_count;
    int*    inlink;
    int*    outlink;
} page_t;

/* structure for inverted index list */
typedef struct invindex
{
    int     document;
    int     freq;
    struct  invindex* next;
} invindex_t;

/* structures for holding info about each anchor word */
typedef struct word
{
    char*           word;
    int             freq;
    invindex_t*     head;
    struct  word*   next;
} word_t;

typedef struct wordtable
{
    int         count;
    int         collisions;
    word_t*   next;
} wordtable_t;

typedef struct wordlist
{
    int         count;
    int         collisions;
    int         maxcount;
    int         maxcollisions;
    wordtable_t*   table;
} wordlist_t;

/* structures for holding data temporarily on loading */
typedef struct filestruct
{
    int     pageid;
    char*   pageurl;
    char*   linkurl;
    char*   anchortext;
    struct  filestruct* next;
} filestruct_t;

typedef struct filestructhead
{
    int             count;
    filestruct_t*   head;
} filestructhead_t;

/* hash table structures for indexing url to page id's */
typedef struct pageid
{
    int             pageid;
    char*           pageurl;
    struct pageid*  next;
} pageid_t;

typedef struct pageidtable
{
    int         count;
    int         collisions;
    pageid_t*   next;
} pageidtable_t;

typedef struct pageidlist
{
    int         count;
    int         collisions;
    int         maxcount;
    int         maxcollisions;
    pageidtable_t*   table;
} pageidlist_t;



#include "utility.h"
#include "hash_table.h"
#include "linked_list.h"
#include "data.h"
#include "search.h"
#endif