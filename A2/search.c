/***************************************************************************
* COSC1100 - Algorithms and Analysis
* Summer Semester 2014 Assignment #2 - Graph Structures
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1100
* Program Code     : BP094
***************************************************************************/
#include "search.h"

void search_for(char* search_term, page_t* pages, wordlist_t* words)
{
    char* word;
    int i=0, hash;
    word_t* thisword;
    invindex_t* results=NULL, *thisresult=NULL, *newresult=NULL, *prevresult=NULL;
    BOOLEAN all_terms_found=TRUE;
    /* tokenize the words in the search text*/
    
    search_term=string_to_lower(search_term);
    printf("Search: \"%s\"\n",search_term);
    word = strtok (search_term," \r\n");
    do
    {
        /* hash the word */
        hash=hash_string(word, WORDS_HASH_SIZE);
        if((thisword=word_exists(word, words->table[hash].next)))
        {
        
            if(!results)
            {   
                /* store first words results in a list */
                results=store_search_results(results, thisword);
            }
            else
            {
                /* compare the previous results with the new results */
                /* save the results in a list */
                newresult=compare_search_results(results, thisword);
                
                /* free the old list */
                thisresult=results;
                while(thisresult)
                {
                    prevresult=thisresult;
                    thisresult=thisresult->next;
                    free(prevresult);
                }
                results=newresult;
            }
        }
        else
        {
            printf("The word \"%s\" doesn't link to any pages\n",word);
            all_terms_found=FALSE;
            break;
        }
        
    }
    while((word = strtok (NULL," \r\n")));
    
    
    /* print off results */
    if(results)
        thisresult=results;
    while(thisresult)
    {
        if(all_terms_found)
        {   
            printf("thisresult %i\n",thisresult->document-1);
            printf("%i. %s%s\n",++i,BASE_URL,pages[thisresult->document-1].url);
        }
        prevresult=thisresult;
        thisresult=thisresult->next;
        free(prevresult);
    }
    printf("Found %i results\n",i);
}

/***************************************************************************
* add_search_result() - create a search list node
    takes document ID and word frequency as parameters
    returns the address of a malloc'd invindex_t node
***************************************************************************/
invindex_t* add_search_result(int doc, int freq)
{
    invindex_t* result;
    result=malloc(sizeof(invindex_t));
    assert(result);
    result->document=doc;
    result->freq=freq;
    result->next=NULL;
    return result;
}

/***************************************************************************
* store_search_results() - stores search results from the first search term
* returns a pointer to the list of results
***************************************************************************/
invindex_t* store_search_results(invindex_t *results, word_t *thisword)
{
    int resultcount=0;
    invindex_t *thisindex=NULL, *thisresult=NULL, *newresult=NULL;
    thisindex=thisword->head;
    
    /* allocate memory to store first search result */
    results=add_search_result(thisindex->document,thisindex->freq);
    results->next=NULL;
    thisresult=results;
    thisindex=thisindex->next;
    resultcount++;
    while(thisindex)
    {
        /* allocate memory to store additional search results */
        newresult=add_search_result(thisindex->document,thisindex->freq);
        newresult->next=results;
        results=newresult;
        thisresult=results;
        thisindex=thisindex->next;
        resultcount++;
    }
    return results;
}

/***************************************************************************
* compare_search_results() - compares search results from two lists
* returns a pointer to a new list of common pages
***************************************************************************/
invindex_t* compare_search_results(invindex_t *results_a, word_t *thisword)
{
    invindex_t *results=NULL, *thisresult_a=NULL, *thisresult_b=NULL, *newresult=NULL;
    
    thisresult_a=results_a;
    while(thisresult_a)
    {
        thisresult_b=thisword->head;
        while(thisresult_b)
        {
            if (thisresult_a->document==thisresult_b->document)
            {
                if(!results)
                {
                    /* allocate memory to store first search result */
                    results=add_search_result(thisresult_b->document,thisresult_b->freq);
                    results->next=NULL;
                }
                else
                {
                     /* allocate memory to store additional search results */
                    newresult=add_search_result(thisresult_b->document,thisresult_b->freq);
                    newresult->next=results;
                    results=newresult;
                }
            }
            thisresult_b=thisresult_b->next;
        }
        thisresult_a=thisresult_a->next;
    }
    
    return results;
}