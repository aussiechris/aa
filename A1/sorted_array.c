/***************************************************************************
* COSC1100 - Algorithms and Analysis
* Summer Semester 2014 Assignment #1 - Data Structure Performance
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1100
* Program Code     : BP094
***************************************************************************/

#include "sorted_array.h"


BOOLEAN initialize_array(database_t * db)
{
    /*****************************************************
    **initialize_array()
    * Allocate memory in our array for the number of names 
    * in the linked list and initialize our array.
    * 
    */
        
    /* allocate memory for and initialize the array head */
    if(!(db->sortedarray = malloc(sizeof(struct arrayhead_t))))
        return FALSE;
    db->sortedarray->memsize = sizeof(sizeof(struct arrayhead_t));
    db->sortedarray->count = 0;
    
    /* allocate memory for the array */
    if(!(db->sortedarray->array = malloc((db->linkedlist->count)*sizeof(array_t))))
        return FALSE;
    db->sortedarray->memsize += (db->linkedlist->count)*sizeof(array_t);
    
    return TRUE;
}

void free_array(database_t * db)
{
    /*****************************************************
    **free_array()
    * Free memory used by the array
    * 
    */
    int i;
    
    /* free data from the array */
    for(i=0;i<db->sortedarray->count;i++)
    {   
        free(db->sortedarray->array[i].name);
        free(db->sortedarray->array[i].origin);
        free(db->sortedarray->array[i].meaning);
    }
    
    /* free the array and head */
    free(db->sortedarray->array);
    free(db->sortedarray);
}

BOOLEAN insert_array(database_t * db, char * name, char * origin, char * meaning)
{
    /*****************************************************
    **insert_array()
    * Insert the supplied data into the array
    * 
    */
    
    /* allocate memory for this entry */
    if(!(db->sortedarray->array[db->sortedarray->count].name = malloc(strlen(name)+1)))
        return FALSE;
    db->sortedarray->memsize += strlen(name)+1;
    if(!(db->sortedarray->array[db->sortedarray->count].origin = malloc(strlen(origin)+1)))
        return FALSE;
    db->sortedarray->memsize += strlen(origin)+1;
    if(!(db->sortedarray->array[db->sortedarray->count].meaning = malloc(strlen(meaning)+1)))
        return FALSE;;
    db->sortedarray->memsize += strlen(meaning)+1;
    
    /* insert data into the array */
    strcpy(db->sortedarray->array[db->sortedarray->count].name,name);
    strcpy(db->sortedarray->array[db->sortedarray->count].origin,origin);
    strcpy(db->sortedarray->array[db->sortedarray->count].meaning,meaning);
    db->sortedarray->count += 1;
    
    return TRUE;
}

void bubble_sort_array (database_t * db)
{
    /*****************************************************
    **bubble_sort_array()
    * Bubble sort algorithm to sort the array
    * 
    */
    
    int i, length;
    unsigned long starttime, endtime;
    struct array_t swap;
    BOOLEAN swapped;
    
    length = db->sortedarray->count;

    printf("Sorting array of %i elements using Bubble Sort\n",length);
    starttime = gettime();
    do
    {
        swapped = FALSE;
        /* for each element in the array */
        for (i = 0; i < length-1; i++)
        {
            /*printf("Sorting element %i\n",i);*/
            /* compare the name of this element with the next element */
            if(strcmp(db->sortedarray->array[i].name, db->sortedarray->array[i+1].name)>0)
            {   
                /*printf("Swapping element %i and %i\n",i, i+1); */
                /* swap the elements if they are out of order */
                swap.name = db->sortedarray->array[i].name;
                db->sortedarray->array[i].name = db->sortedarray->array[i+1].name;
                db->sortedarray->array[i+1].name = swap.name;
                
                swap.origin = db->sortedarray->array[i].origin;
                db->sortedarray->array[i].origin = db->sortedarray->array[i+1].origin;
                db->sortedarray->array[i+1].origin = swap.origin;
                
                swap.meaning = db->sortedarray->array[i].meaning;
                db->sortedarray->array[i].meaning = db->sortedarray->array[i+1].meaning;
                db->sortedarray->array[i+1].meaning = swap.meaning;
                
                swapped = TRUE;
            }
        }
        /* reduce the size of the problem by 1 and repeat */
        length--;
    }
    /* continue until we don't need to do any more swaps - sorted! */
    while (swapped);
    
    /* display statistics about sort */
    endtime = gettime();
    printf("Finished sorting on pass %i\n",db->sortedarray->count-length);
    printf("Sort took %.2f sec\n", (double)(endtime-starttime)/1E6);
}
void comb_sort_array (database_t * db)
{
    /*****************************************************
    **comb_sort_array()
    * Comb sort algorithm to sort the array
    * 
    */
    
    int i, length, comb, passno;
    unsigned long starttime, endtime;
    struct array_t swap;
    BOOLEAN swapped;
    
    length = db->sortedarray->count;
    comb = length/1.3;
    passno=0;

    printf("Sorting array of %i elements using Comb Sort\n",length);
    starttime = gettime();
    do
    {
        swapped = FALSE;
        /* for each element in the array */
        for (i = 0; i < length-comb; i++)
        {
            /* compare the name of this element with the comb element */
            if(strcmp(db->sortedarray->array[i].name, db->sortedarray->array[i+comb].name)>0)
            {
                /* swap the elements if they are out of order */
                swap.name = db->sortedarray->array[i].name;
                db->sortedarray->array[i].name = db->sortedarray->array[i+comb].name;
                db->sortedarray->array[i+comb].name = swap.name;
                
                swap.origin = db->sortedarray->array[i].origin;
                db->sortedarray->array[i].origin = db->sortedarray->array[i+comb].origin;
                db->sortedarray->array[i+comb].origin = swap.origin;
                
                swap.meaning = db->sortedarray->array[i].meaning;
                db->sortedarray->array[i].meaning = db->sortedarray->array[i+comb].meaning;
                db->sortedarray->array[i+comb].meaning = swap.meaning;
                
                swapped = TRUE;
            }
        }
        /* reduce the size of the comb and repeat */
        if(comb>1)
            comb = comb/1.3;
        passno++;
    }
    /* continue until we don't need to do any more swaps - sorted! */
    while (swapped);
        
    /* display statistics about sort */
    endtime = gettime();
    db->sortedarray->sort_count = passno;
    db->sortedarray->sort_time = (double)(endtime-starttime)/1E6;
    printf("Finished sorting on pass %i\n",db->sortedarray->sort_count);
    printf("Sort took %.10f sec\n", db->sortedarray->sort_time);
    
    /* check array is sorted */
    for(i=0;i<length-1;i++)
    {
        if((strcmp(db->sortedarray->array[i].name,db->sortedarray->array[i+1].name))>0)
        {
            printf("Sorting error! Element at %i is higher than %i!\n",i,i+1);
            printf("%s > %s\n",db->sortedarray->array[i].name,db->sortedarray->array[i+1].name);
        }
    }
}

void search_array(database_t * db, char* name)
{

    /*****************************************************
    **search_array()
    * Search an unsorted array for the given name
    * 
    */

    int i, match=0;
    
    /* for each position in the array */
    for(i=0;i<db->sortedarray->count;i++)
    {   
        if(strcmp(name,db->sortedarray->array[i].name)==0)
        {
            /* match found - output data */
            if(match==0)
                printf("\nName: %s\n",db->sortedarray->array[i].name);
            printf("Origin: %s\n",db->sortedarray->array[i].name);
            printf("Meaning: %s\n",db->sortedarray->array[i].name);
            match++;
        }
    }
    
    /* notify of failed search */
    if(match==0)
        printf("No match found\n");
    else if (match==1)
        printf("%i match found\n\n",match);
    else
        printf("%i matches found\n\n",match);
}

void search_sorted_array(database_t * db, char* name)
{

    /*****************************************************
    **search_sorted_array()
    * Search a sorted array for the given name
    * 
    */

    int i, match=0;
    
    /* for each position in the array */
    for(i=0;i<db->sortedarray->count;i++)
    {   
        if(strcmp(name,db->sortedarray->array[i].name)==0)
        {
            /* match found - output data */
            if(match==0)
                printf("\nName: %s\n",db->sortedarray->array[i].name);
            printf("Origin: %s\n",db->sortedarray->array[i].name);
            printf("Meaning: %s\n",db->sortedarray->array[i].name);
            match++;
        }
        /* stop if we go past a match */
        else if(strcmp(name,db->sortedarray->array[i].name)<0)
            break;
        
    }
    
    /* notify of failed search */
    if(match==0)
        printf("No match found\n");
    else if (match==1)
        printf("%i match found\n\n",match);
    else
        printf("%i matches found\n\n",match);
}