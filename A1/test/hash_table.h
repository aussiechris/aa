/***************************************************************************
* COSC1100 - Algorithms and Analysis
* Summer Semester 2014 Assignment #1 - Data Structure Performance
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1100
* Program Code     : BP094
***************************************************************************/
#ifndef HTABLE_H
#define HTABLE_H

#include "main_aa_a1.h"

#define HASHTABLESIZE 397
 
typedef struct hashnode_t
{
    struct hashnode_t *next;
    char* name;
    char* origin;
    char* meaning;
} hashnode_t;

typedef struct hashtable_t
{
    struct hashnode_t *next;
} hashtable_t;

typedef struct hashhead_t
{
    int count;
    int memsize;
    int collisions;
    double unsorted_search;
    double sorted_search;
    double sort_time;
    struct hashtable_t *hash;
} hashhead_t;

BOOLEAN initialize_hash(database_t * db);
void free_hash(database_t * db);
void hash_stats(database_t * db);
BOOLEAN insert_hash(database_t * db, char * name, char * origin, char * meaning);
void bubble_sort_hash (database_t * db);
void comb_sort_hash (database_t * db);
void search_hash(database_t * db, char* name);
void search_sorted_hash(database_t * db, char* name);

#endif