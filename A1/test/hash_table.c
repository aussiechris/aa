/***************************************************************************
* COSC1100 - Algorithms and Analysis
* Summer Semester 2014 Assignment #1 - Data Structure Performance
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1100
* Program Code     : BP094
***************************************************************************/

#include "hash_table.h"

BOOLEAN initialize_hash(database_t * db)
{
    /*****************************************************
    **initialize_hash()
    * Allocate memory in our hash table and initialize the 
    * table.
    * 
    */
        
    int i;
    
    /* allocate memory for the hash table head*/
    if(!(db->hashtable = malloc(sizeof(struct hashhead_t))))
        return FALSE;
    db->hashtable->memsize = sizeof(sizeof(struct hashhead_t));
    db->hashtable->count = 0;
    db->hashtable->collisions = 0;
    
    /* allocate memory for the hash table */
    if(!(db->hashtable->hash = malloc(HASHTABLESIZE*sizeof(struct hashtable_t))))
        return FALSE;
    db->hashtable->memsize += (HASHTABLESIZE*sizeof(struct hashtable_t));
    
    /* initialize pointers */
    for(i=0;i<HASHTABLESIZE;i++)
    {
        db->hashtable->hash[i].next = NULL;
    }
    
    return TRUE;
}


BOOLEAN insert_hash(database_t * db, char * name, char * origin, char * meaning)
{
    /*****************************************************
    **insert_hash()
    * Insert the supplied data into the hash table
    * 
    */
    
    int hashvalue =0, namelength = strlen(name);
    int i;
    struct hashnode_t *this, *next;
    BOOLEAN collision = FALSE;
    
    /* calculate the hash value */
    for(i=0;i<namelength;i++)
    {
        hashvalue+=name[i];
    }
    hashvalue = hashvalue%HASHTABLESIZE;
    
    /* check for collisions */
    if((this = db->hashtable->hash[hashvalue].next))
    {
        /* collision! find first empty node */ 
        collision = TRUE;      
        next = this->next;
        while(next)
        {
            this = this->next;
            next = this->next;
        }
        /* found first available node, allocate memory */
        if(!(this->next = malloc(sizeof(struct hashnode_t))))
            return FALSE;
        db->hashtable->memsize += sizeof(struct hashnode_t);
        this = this->next;
    }
    else
    {
        /* no collisions, use the head node */
        if(!(db->hashtable->hash[hashvalue].next = malloc(sizeof(struct hashnode_t))))
            return FALSE;
        db->hashtable->memsize += sizeof(struct hashnode_t);
        this = db->hashtable->hash[hashvalue].next;
    }
    
    /* allocate memory for this node */
    if(!(this->name = malloc(strlen(name)+1)))
        return FALSE;
    db->hashtable->memsize += strlen(name)+1;
    if(!(this->origin = malloc(strlen(origin)+1)))
        return FALSE;
    db->hashtable->memsize += strlen(origin)+1;
    if(!(this->meaning = malloc(strlen(meaning)+1)))
        return FALSE;;
    db->hashtable->memsize += strlen(meaning)+1;
    
    /* insert data into the node */
    strcpy(this->name,name);
    strcpy(this->origin,origin);
    strcpy(this->meaning,meaning);
    this->next = NULL;
    db->hashtable->count += 1;
    if(collision)
        db->hashtable->collisions++;
    return TRUE;
}

void free_hash(database_t * db)
{
    /*****************************************************
    **free_hash()
    * Free memory used by the hash table
    * 
    */
    int i;
    struct hashnode_t *this, *next;
    
    /* check for data in each hash table position */
    for(i=0;i<HASHTABLESIZE;i++)
    {
        if((this = db->hashtable->hash[i].next))
        {
            /* there is data in this position */
            /* free used nodes */
            while(this)
            {
                next = this->next;
                free(this->name);
                free(this->origin);
                free(this->meaning);
                free(this);
                this = next;
            }
        }
    }
    free(db->hashtable->hash);
    free(db->hashtable);
}

void hash_stats(database_t * db)
{
    /*****************************************************
    **hash_stats()
    * Calculate collision stats for the hash table
    * 
    */    
    
    int min,max,count,i;
    struct hashnode_t *this, *next;
    
    min = db->hashtable->count;
    max = 0;
    /* for each position in the hash table */
    for(i=0;i<HASHTABLESIZE;i++)
    {
        /* find if there is data in this position */
        count=0;
        if((this = db->hashtable->hash[i].next))
        {
            /* there is data in this position */
            /* count nodes */
            while(this)
            {
                next = this->next;
                count++;
                this = next;
            }
        }
        if(count>max)
            max = count;
        if(count<min)
            min = count;
        /*
        printf("H%i - %i nodes\n",i,count);
        */
    }
    printf("Min hash collisions = %i\nMax hash collisions = %i\n",min,max);
}

void search_hash(database_t * db, char* name)
{
    /*****************************************************
    **search_hash()
    * Search an unsorted hash table for the given name
    * 
    */
    int i, match=0, hashvalue =0, namelength = strlen(name);
    struct hashnode_t *this;
    
    /* calculate the hash value */
    for(i=0;i<namelength;i++)
    {
        hashvalue+=name[i];
    }
    hashvalue = hashvalue%HASHTABLESIZE;
    
    /* check for data in this hash table position */
    if((this = db->hashtable->hash[hashvalue].next))
    {
        /* there is data in this position */
        /* search for name */
        while(this)
        {
            if(strcmp(name,this->name)==0)
            {
                /* match found - output data */
                if(match==0)
                    printf("\nName: %s\n",this->name);
                printf("Origin: %s\n",this->origin);
                printf("Meaning: %s\n",this->meaning);
                match++;
            }
            this = this->next;
        }
    }
}

void search_sorted_hash(database_t * db, char* name)
{
    /*****************************************************
    **search_sorted_hash()
    * Search a sorted hash table for the given name
    * 
    */
    int i, match=0, hashvalue =0, namelength = strlen(name);
    struct hashnode_t *this;
    
    /* calculate the hash value */
    for(i=0;i<namelength;i++)
    {
        hashvalue+=name[i];
    }
    hashvalue = hashvalue%HASHTABLESIZE;
    
    /* check for data in this hash table position */
    if((this = db->hashtable->hash[hashvalue].next))
    {
        /* there is data in this position */
        /* search for name */
        while(this)
        {
            if(strcmp(name,this->name)==0)
            {
                /* match found - output data */
                if(match==0)
                    printf("\nName: %s\n",this->name);
                printf("Origin: %s\n",this->origin);
                printf("Meaning: %s\n",this->meaning);
                match++;
            }
            /* stop if we go past a match */
            else if(strcmp(name,this->name)<0)
                break;
            this = this->next;
        }
    }
}
