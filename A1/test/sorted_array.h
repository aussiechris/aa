/***************************************************************************
* COSC1100 - Algorithms and Analysis
* Summer Semester 2014 Assignment #1 - Data Structure Performance
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1100
* Program Code     : BP094
***************************************************************************/
#ifndef SARRAY_H
#define SARRAY_H

#include "main_aa_a1.h"

typedef struct array_t
{
    char* name;
    char* origin;
    char* meaning;
} array_t;

typedef struct arrayhead_t
{
    int count;
    int memsize;
    double unsorted_search;
    double sorted_search;
    double sort_time;
    int sort_count;
    struct array_t*array;
} arrayhead_t;

BOOLEAN initialize_array(database_t * db);
void free_array(database_t * db);
BOOLEAN insert_array(database_t * db, char * name, char * origin, char * meaning);
void bubble_sort_array (database_t * db);
void comb_sort_array (database_t * db);
void search_array(database_t * db, char* name);
void search_sorted_array(database_t * db, char* name);


#endif