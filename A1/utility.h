/***************************************************************************
* COSC1100 - Algorithms and Analysis
* Summer Semester 2014 Assignment #1 - Data Structure Performance
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1100
* Program Code     : BP094
***************************************************************************/
#ifndef UTILITY_H
#define UTILITY_H

#include "main_aa_a1.h"


/* function prototypes go here */
unsigned long gettime();
BOOLEAN initialize_db(database_t * db);
BOOLEAN load_names_file(char * namesfile, database_t * db);
BOOLEAN free_db(database_t * db);
BOOLEAN load_search_file(char * searchfile, database_t * db);
void free_search(database_t * db);
void search_names(database_t * db);
#endif