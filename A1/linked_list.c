/***************************************************************************
* COSC1100 - Algorithms and Analysis
* Summer Semester 2014 Assignment #1 - Data Structure Performance
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1100
* Program Code     : BP094
***************************************************************************/

#include "linked_list.h"

BOOLEAN initialize_list(database_t * db)
{
    /*****************************************************
    **initialize_list()
    * Allocate memory for and initialize values of our 
    * linked list in the database db linked list
    * 
    */
    
    if(!(db->linkedlist = malloc(sizeof(list_t))))
        return FALSE;
    db->linkedlist->count=0;
    db->linkedlist->memsize=sizeof(list_t);
    db->linkedlist->head=NULL;
    return TRUE;
}

BOOLEAN load_list(database_t* db, char* line)
{
    /*****************************************************
    **load_list()
    * Load data from line into the linked list stored in 
    * the database db
    * 
    */

    char* item=NULL;
    size_t itemsize;
    struct node_t * new;
    
    /* insert a new empty node at the start of our linked list */
    if(!(new = malloc(sizeof(struct node_t))))
        return FALSE;
    db->linkedlist->memsize+=sizeof(struct node_t);
    db->linkedlist->count++;
    new->next = db->linkedlist->head;
    db->linkedlist->head = new;
    
    /* parse each line and insert it into the linked list */
    item = strtok (line,"\t");
    itemsize = ((strlen(item)+1)*sizeof(char));
    if(!(new->name = malloc(itemsize)))
        return FALSE;
    db->linkedlist->memsize+=itemsize;
    strcpy(new->name,item);
        
    item = strtok (NULL,"\t");
    itemsize = ((strlen(item)+1)*sizeof(char));
    if(!(new->origin = malloc(itemsize)))
        return FALSE;
    db->linkedlist->memsize+=itemsize;
    strcpy(new->origin,item);
    
    item = strtok (NULL,"\r\n");
    itemsize = ((strlen(item)+1)*sizeof(char));
    if(!(new->meaning = malloc(itemsize)))
        return FALSE;
    db->linkedlist->memsize+=itemsize;
    strcpy(new->meaning,item);
    
    return TRUE;
}

void free_list(database_t * db)
{
    /*****************************************************
    **free_list()
    * Free all data saved in the linked list
    * 
    */
    
    struct node_t * next;
    struct node_t * this;
    
    /* set first node */
    this = db->linkedlist->head;
    
    /* free each node and it's data */
    while(this)
    {
        next = this->next;
        free(this->name);
        free(this->origin);
        free(this->meaning);
        free(this);
        this = next;
    }
    /* free the main list_t data */
    free(db->linkedlist);
}


void bubble_sort_list (database_t * db)
{
    /*****************************************************
    **bubble_sort_list()
    * Bubble sort algorithm to sort the linked list
    * 
    */
    
    int i, length, passes=0;
    unsigned long starttime, endtime;
    struct node_t *this, *next, *prev;
    BOOLEAN swapped;
    
    length = db->linkedlist->count;

    printf("Sorting linked list of %i elements using Bubble Sort\n",length);
    starttime = gettime();
    do
    {
        swapped = FALSE;
        /* set first node */
        this = db->linkedlist->head;
        
        /* for each element in the list */
        for (i = 0; i < length-1; i++)
        {
        
            next = this->next;
            /*
            printf("Sorting element %i\n",i);
            */
            /* compare the name of this element with the next element */
            if(strcmp(this->name, next->name)>0)
            {   
                if(!i)
                {
                    /* deal with head node */
                    db->linkedlist->head = next;
                }
                else
                {
                    /* deal with other nodes */
                    prev->next = next;
                }
                /* swap the elements if they are out of order */
                this->next = next->next;
                next->next = this;
                swapped = TRUE;
                this = next;
                next = this->next;
            }
            
            prev = this;
            this = next;
            next = this->next;
        }
        /* reduce the size of the problem by 1 and repeat */
        length--;
        passes++;
    }
    /* continue until we don't need to do any more swaps - sorted! */
    while (swapped);
    
    /* display statistics about sort */
    endtime = gettime();
    db->linkedlist->sort_count = passes;
    db->linkedlist->sort_time = (double)(endtime-starttime)/1E6;
    printf("Finished sorting on pass %i\n",db->linkedlist->sort_count);
    printf("Sort took %.2f sec\n", db->linkedlist->sort_time);
    
    /* check list is sorted */
    this = db->linkedlist->head;
    for(i=0;this->next;i++)
    {
        next = this->next;
        if((strcmp(this->name,next->name))>0)
        {
            printf("Sorting error! Element at %i is higher than %i!\n",i,i+1);
            printf("%s > %s\n",this->name,next->name);
        }
        this = next;
    }
    
}

void comb_sort_list (database_t * db)
{
    /*****************************************************
    **comb_sort_list()
    * Comb sort algorithm to sort the linked list
    * 
    */
    
    int i, j, length, comb, passes=0;
    unsigned long starttime, endtime;
    struct node_t *this, *next, *prev,*combthis, *combnext, *combprev;
    BOOLEAN swapped;
    
    length = db->linkedlist->count;
    comb = length/2;

    printf("Sorting linked list of %i elements using Comb Sort\n",length);
    starttime = gettime();
    do
    {
        swapped = FALSE;
        
        /* set first node */
        this = db->linkedlist->head;
        next = this->next;
        combthis = this;
        
        /* iterate to comb position */
        for(j=0;j<comb;j++)
        {
            combprev=combthis;
            combthis=combthis->next;
            combnext=combthis->next;
        }
        
        /* for each element in the list */
        for (i = 0; combnext; i++)
        {
            /* compare the name of this element with the next element */
            if(strcmp(this->name, combthis->name)>0)
            {
            
                /* items are out of order! Swap this and combthis */
                /* set the pointers before the first swap point*/
                if(!i)
                {
                    /* deal with head node */
                    db->linkedlist->head=combthis;
                }
                else
                {
                    /* deal with subsequent nodes */
                    prev->next=combthis;
                }
                
                /* swap other pointers */
                if(comb==1)
                {
                    /* swap pointers next to each other */
                    combthis->next=this;
                }
                else
                {
                    /* swap pointers not next to each other */
                    combthis->next=next;
                }
                
                combprev->next=this;
                this->next=combnext;
                this = combthis;
                combthis = combprev->next;
                
                swapped = TRUE;
            }
            
            /* move pointers forward */
            prev = this;
            this = this->next;
            next = this->next->next;
            combprev = combthis;
            combthis = combthis->next;
            combnext = combthis->next->next;
        }
        /* reduce the size of the comb and repeat */
        if(comb>1)
        {
            comb = comb/1.3;
        }
        
        printf("pass = %i - i = %i\n",passes,i);
        passes++;
    }
    /* continue until we don't need to do any more swaps - sorted! */
    while (comb>1&&swapped);
    
    
    /* display statistics about sort */
    endtime = gettime();
    printf("Finished sorting on pass %i\n",passes);
     printf("Sort took %.10f sec\n", (double)(endtime-starttime)/1E6);
    
    /* check list is sorted */
    
    this = db->linkedlist->head;
    for(i=0;this->next;i++)
    {
        next = this->next;
        if((strcmp(this->name,next->name))>0)
        {
            printf("Sorting error! Element at %i is higher than %i!\n",i,i+1);
            printf("%s > %s\n",this->name,next->name);
        }
        this = next;
    }
    
}

void search_list(database_t * db, char* name)
{

    /*****************************************************
    **search_list()
    * Search an unsorted linked list for the given name
    * 
    */
    
    struct node_t * this;
    int match=0;
    
    /* set first node */
    this = db->linkedlist->head;
        
    /* search through all nodes for a match - worst case */
    while(this)
    {
        
        if(strcmp(name,this->name)==0)
        {
            /* match found - output data */
            if(match==0)
                printf("\nName: %s\n",this->name);
            printf("Origin: %s\n",this->origin);
            printf("Meaning: %s\n",this->meaning);
            match++;
        }
        this = this->next;
    }
    
    /* notify of failed search */
    if(match==0)
        printf("No match found\n");
    else if (match==1)
        printf("%i match found\n\n",match);
    else
        printf("%i matches found\n\n",match);
    
}

void search_sorted_list(database_t * db, char* name)
{

    /*****************************************************
    **search_sorted_list()
    * Search a sorted linked list for the given name
    * 
    */
    
    struct node_t * this;
    int match=0;
    
    /* set first node */
    this = db->linkedlist->head;
        
    /* search through all nodes for a match */
    while(this)
    {
        
        if(strcmp(name,this->name)==0)
        {
            /* match found - output data */
            if(match==0)
                printf("\nName: %s\n",this->name);
            printf("Origin: %s\n",this->origin);
            printf("Meaning: %s\n",this->meaning);
            match++;
        }
        /* stop if we go past a match */
        else if(strcmp(name,this->name)<0)
            break;
        this = this->next;
    }
    
    /* notify of failed search */
    if(match==0)
        printf("No match found\n");
    else if (match==1)
        printf("%i match found\n\n",match);
    else
        printf("%i matches found\n\n",match);
    
}