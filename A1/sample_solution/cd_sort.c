/****************************************************************************
 * COSC 2123 - Algorithms & Analysis
 * Assignment       : 1
 * Filename         : cd_sort.c
 * Authors          : s9863998 - Tom Kaczocha
 *                  : 
 *                  : 
 ****************************************************************************/

#include "cd_sort.h"
#include "cd_util.h"

/**
 * Sorts a list of elements using Bubble Sort.
 * 
 * @param void** list  list of elements to sort
 * @param in32_t count number of elements
 * @param *comp  comp  element compare function
 * 
 * @return void** sorted list
 */
void** sort_bubble( void** list, int32_t count, void ( *comp )( void*, void* ) )
{
    int32_t i  = 0;
    int32_t j  = 0;
    
    for ( i = 0; i < ( count - 1 ); i++ )
    {
        for ( j = 0; j < ( count - ( i + 1 ) ); j++ )
        {
            comp( ( list[ j + 1 ] ), ( list[ j ] ) );
        }
    }
    
    return list;
}