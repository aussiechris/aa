/****************************************************************************
 * COSC 2123 - Algorithms & Analysis
 * Assignment       : 1
 * Filename         : main.c
 * Authors          : s9863998 - Tom Kaczocha
 *                  : 
 *                  : 
 ****************************************************************************/

#include "cd_common.h"

#include "cd_util.h"

#include "cd_time.h"
#include "cd_array.h"
#include "cd_list.h"
#include "cd_hash.h"

array_t* init_array( char** names, uint32_t count );
list_t*  init_list ( char** names, uint32_t count );
hash_t*  init_hash ( char** names, uint32_t count, uint32_t size );

float search_array( array_t* array, char** terms, uint32_t count );
float search_list ( list_t*  list,  char** terms, uint32_t count );
float search_hash ( hash_t*  hash,  char** terms, uint32_t count );

/**
 * Application option flags.
 */
int32_t cd_options;

/* sort times */
times_t* times_sort_array;
times_t* times_sort_list;
times_t* times_sort_hash;

/**
 * Program main entry point.
 * 
 * @param int    argc number of args
 * @param char** argv list of args
 * 
 * @return int program status
 */
int main( int argc, char** argv )
{
    array_t*  array       = NULL;
    list_t*   list        = NULL;
    hash_t*   hash        = NULL;
    
    char**    names       = NULL;
    char**    terms       = NULL;
    char*     names_file  = NULL;
    char*     search_file = NULL;
    uint64_t  start_time  = 0;
    uint64_t  end_time    = 0;
    uint32_t  n_count     = 0;
    uint32_t  s_count     = 0;
    uint32_t  hash_size   = INITIAL_SIZE;
    char*     result      = NULL;
    
    float times[ 4 ];
    
    start_time = time_gettime_usec();
    
    /***************************************************************************
     * 
     * INIT
     * 
     **************************************************************************/
    
    hash_size = get_options( argc, argv, &names_file, &search_file );
    
    /* read in names */
    names = get_items( names_file, &n_count );

    if ( NULL == names )
    {
        printf( "Failed to get names list.\n" );

        exit( EXIT_FAILURE );
    }

    times_sort_array = time_init_list();
    times_sort_list  = time_init_list();
    times_sort_hash  = time_init_list();
    
    array = init_array( names, n_count );
    list  = init_list ( names, n_count );
    hash  = init_hash ( names, n_count, hash_size );
    
    free_list( n_count, names ); /* cleanup names data */
    
    
    /***************************************************************************
     * 
     * SEARCH
     * 
     **************************************************************************/
    
    /* read in search terms */
    terms = get_items( search_file, &s_count );

    if ( NULL == terms )
    {
        printf( "Failed to get search terms list.\n" );
        
        exit( EXIT_FAILURE );
    }
    
    end_time = time_gettime_usec();
    
    times[ 0 ] = time_get_diff_sec( start_time, end_time );
    
    times[ 1 ] = search_array( array, terms, s_count ); /* search array */
    times[ 2 ] = search_list ( list,  terms, s_count ); /* search linked list */
    times[ 3 ] = search_hash ( hash,  terms, s_count ); /* search hash table */
    
    free_list( s_count, terms ); /* clean up search terms */
    
    
    /***************************************************************************
     * 
     * REPORT
     * 
     **************************************************************************/
    {
        int32_t   hash_min = -1;
        int32_t   hash_max = -1;
        
        result = malloc( sizeof( char ) * MAX_SIZE );
        
        /* get clustering */
        hash_get_clustering( hash, &hash_min, &hash_max );
        
        buffer_add( "\n" );

        sprintf( result, "%30s %f\n", "Setup Time -",  times[ 0 ] );
        buffer_add( result );

        sprintf( result, "%30s %f\n", "Array Search Time -",  times[ 1 ] );
        buffer_add( result );

        sprintf( result, "%30s %f\n", "List Search Time -",   times[ 2 ] );
        buffer_add( result );

        sprintf( result, "%30s %f\n\n", "Hash Search Time -", times[ 3 ] );
        buffer_add( result );
        
        sprintf( result, "%30s %f\n\n", "Total Time -", ( times[ 0 ] + times[ 1 ] + times[ 2 ] + times[ 3 ] ) );
        buffer_add( result );

        sprintf( result, "%30s %lu Kb\n", "Array Size -", array->tsize / MEGABITE );
        buffer_add( result );

        sprintf( result, "%30s %lu Kb\n", "List Size -",  list->tsize  / MEGABITE );
        buffer_add( result );

        sprintf( result, "%30s %lu Kb\n", "Hash Size -",  hash->tsize  / MEGABITE );
        buffer_add( result );

        /* sort times */
        
        if ( TRUE == OPTION_SET( CD_SORT_TIME ) && TRUE == OPTION_SET( CD_ARRAY_SORT ) )
        {
            sprintf( result, "\n%30s %f\n", "Array Sort Time -", time_get_list_total( times_sort_array ) / MILLION );
            buffer_add( result );
        }

        if ( TRUE == OPTION_SET( CD_SORT_TIME ) && TRUE == OPTION_SET( CD_LIST_SORT ) )
        {
            sprintf( result, "%30s %f\n", "List Sort Time -", time_get_list_total( times_sort_list ) / MILLION );
            buffer_add( result );
        }
        
        if ( TRUE == OPTION_SET( CD_SORT_TIME ) )
        {
            sprintf( result, "%30s %f\n", "Hash Sort Time -", time_get_list_total( times_sort_hash ) / MILLION );
            buffer_add( result );
        }
        
        /* clustering */
        buffer_add( "\nClustering\n" );
        buffer_add( "------------\n" );
    
        sprintf( result, "MIN %d\n", hash_min );
        buffer_add( result );
        
        sprintf( result, "MAX %d\n", hash_max );
        buffer_add( result );
        
        sprintf( result, "Hash Table Size: %d\n", hash->size );
        buffer_add( result );
        
        buffer_add( "------------\n\n" );
    }
    
    /* save search results to file */
    save_results();
    
    free( names_file  ); /* free names file array */
    free( search_file ); /* free search terms file array */
    free( result      ); /* free the result variable */
    
    array_free( array ); /* free sorted array structure */
    list_free ( list  ); /* free linked list structure */
    hash_free ( hash  ); /* free hash table structure */
    
    time_free_list( times_sort_array );
    time_free_list( times_sort_list  );
    time_free_list( times_sort_hash  );
    
    return EXIT_SUCCESS;
}


/*******************************************************************************
 * 
 * INIT STRUCTURES
 * 
 ******************************************************************************/

/**
 * Initialises the array structure.
 * 
 * @param char** names the names
 * 
 * @return array_t* the new array pointer
 */
array_t* init_array( char** names, uint32_t count )
{
    array_t* array = NULL;
    uint32_t i     = 0;
    
    array = array_create( INITIAL_SIZE );
    
    while ( i < count )
    {
        if ( FALSE == array_insert( array, names[ i ] ) )
        {
            printf( "Failed to insert entry into array\n" );
            
            exit( EXIT_FAILURE );
        }
        
        i++;
    }
    
    return array;
}

/**
 * Initialises the list structure.
 * 
 * @param char**   names list of names data
 * @param uint32_t count count of names
 * 
 * @return list_t* new list pointer
 */
list_t* init_list( char** names, uint32_t count )
{
    list_t* list = NULL;
    uint32_t i   = 0;
    
    list = list_create();
    
    i = 0;
    
    while ( i < count )
    {
        if ( FALSE == list_insert( list, names[ i ] ) )
        {
            printf( "Failed to insert entry into list\n" );
            
            exit( EXIT_FAILURE );
        }
        
        i++;
    }
    
    return list;
}

/**
 * Initialises the hash structure.
 * 
 * @param char** names list of names data
 * @param uint32_t count count of names
 * @param uint32_t size  size of hash table
 * 
 * @return hash_t* new hash table pointer
 */
hash_t* init_hash( char** names, uint32_t count, uint32_t size )
{
    hash_t*   hash = NULL;
    uint32_t  i    = 0;
    int32_t** hash_distrib = NULL;
    
    hash = hash_create( size );
    
    while ( i < count )
    {
        if ( FALSE == hash_insert( hash, names[ i ] ) )
        {
            printf( "Failed to insert entry into hash table.\n" );
            
            exit( EXIT_FAILURE );
        }
        
        i++;
    }
    
    /* get distribution */
    hash_distrib = hash_get_distribution( hash );
    
    /* write distribution to file */
    write_distribution( hash, hash_distrib );
    
    hash_free_distribution( hash, hash_distrib ); /* cleanup hash stats */
    
    return hash;
}


/*******************************************************************************
 * 
 * SEARCH
 * 
 ******************************************************************************/

/**
 * Searches the array for all search terms.
 * 
 * @param array_t* array the array structure
 * @param char**   terms list of search terms
 * @param uint32_t count count of terms to search
 * 
 * @return float search time in seconds
 */
float search_array( array_t* array, char** terms, uint32_t count )
{
    char*    result = NULL;
    int32_t  index  = 0;
    int32_t  i      = 0;
    int64_t  start  = 0;
    int64_t  end    = 0;
    BOOL     found  = FALSE;
    
    result = malloc( sizeof( char ) * MAX_SIZE );
    
    if ( TRUE == OPTION_SET( CD_VERBOSE ) )
    {
        printf( "\nArray Search\n" );
        printf( "-------------\n" );
    }
    
    sprintf( result, "\nArray Search\n-------------\n" );
    buffer_add( result );
    
    start = time_gettime_usec();
    
    while ( i < count )
    {
        entry_t* entry = NULL;
        
        found = FALSE;
        
        if ( TRUE == OPTION_SET( CD_ARRAY_BIN ) )
        {
            index = array_locate_binary( array, terms[ i ] );
        }
        else
        {
            index = array_locate_sequential( array, terms[ i ] );
        }
        
        if ( TRUE == OPTION_SET( CD_ARRAY_SORT ) )
        {
            while( NOT_FOUND != index && index < array->count &&
                TRUE == entry_comp( terms[ i ], array->items[ index ] ) )
            {
                entry = array->items[ index ];

                sprintf( result, PRINT_FOUND, entry->name, entry->origin, entry->meaning );
                buffer_add( result );

                found = TRUE;

                entry = NULL;

                index++;
            }
        }
        else
        {
            while ( index < array->count )
            {
                if ( TRUE == entry_comp( terms[ i ], array->items[ index ] ) )
                {
                    entry = array->items[ index ];

                    sprintf( result, PRINT_FOUND, entry->name, entry->origin, entry->meaning );
                    buffer_add( result );

                    found = TRUE;

                    entry = NULL;
                }
                
                index++;
            }
        }
        
        if ( FALSE == found )
        {
            sprintf( result, PRINT_NOT_FOUND, terms[ i ] );
            buffer_add( result );
        }
        
        i++;
    }
    
    end = time_gettime_usec();
    
    free( result ); /* free result variable */
    
    return time_get_diff_sec( start, end );
}

/**
 * Searches the linked list for all search terms.
 * 
 * @param list_t*  list  the linked list structure
 * @param char**   terms list of search terms
 * @param uint32_t count count of terms to search
 * 
 * @return float search time in seconds
 */
float search_list( list_t* list, char** terms, uint32_t count )
{
    char*    result = NULL;
    int32_t  i      = 0;
    int64_t  start  = 0;
    int64_t  end    = 0;
    BOOL     found  = FALSE;
    
    result = malloc( sizeof( char ) * MAX_SIZE );
    
    if ( TRUE == OPTION_SET( CD_VERBOSE ) )
    {
        printf( "\nList Search\n" );
        printf( "--------------\n" );
    }
    
    sprintf( result, "\nList Search\n-------------\n" );
    buffer_add( result );
    
    start = time_gettime_usec();
    
    while ( i < count )
    {
        entry_t* entry = NULL;
        
        found = FALSE;
        
        entry = list_locate( list, terms[ i ] );
        
        if ( TRUE == OPTION_SET( CD_LIST_SORT ) )
        {
            while ( NULL != entry && TRUE == entry_comp( terms[ i ], entry ) )
            {
                sprintf( result, PRINT_FOUND, entry->name, entry->origin, entry->meaning );
                buffer_add( result );

                entry = entry->next;

                found = TRUE;
            }

            if ( FALSE == found )
            {
                sprintf( result, PRINT_NOT_FOUND, terms[ i ] );
                buffer_add( result );
            }
        }
        else
        {
            if ( NULL != entry )
            {
                while ( NULL != entry )
                {
                    if ( TRUE == entry_comp( terms[ i ], entry ) )
                    {
                        sprintf( result, PRINT_FOUND, entry->name, entry->origin, entry->meaning );
                        buffer_add( result );
                    }
                    
                    entry = entry->next;
                }
            }
            else
            {
                sprintf( result, PRINT_NOT_FOUND, terms[ i ] );
                buffer_add( result );
            }
        }
        
        i++;
    }
    
    end = time_gettime_usec();
    
    free( result ); /* free result variable */
    
    return time_get_diff_sec( start, end );
}

/**
 * Searches the hash table for all search terms.
 * 
 * @param hash_t*  hash  the hash table structure
 * @param char**   terms list of search terms
 * @param uint32_t count count of temrs to search
 * 
 * @return float search time in seconds
 */
float search_hash( hash_t* hash, char** terms, uint32_t count )
{
    char*    result = NULL;
    int32_t  i      = 0;
    int64_t  start  = 0;
    int64_t  end    = 0;
    BOOL     found  = FALSE;
    
    result = malloc( sizeof( char ) * MAX_SIZE );
    
    if ( TRUE == OPTION_SET( CD_VERBOSE ) )
    {
        printf( "\nHash Search\n" );
        printf( "--------------\n" );
    }
    
    sprintf( result, "\nHash Search\n-------------\n" );
    buffer_add( result );
    
    start = time_gettime_usec();
    
    while ( i < count )
    {
        entry_t* entry = NULL;
        
        found = FALSE;
        
        entry = hash_locate( hash, terms[ i ] );
        
        if ( TRUE == OPTION_SET( CD_HASH_SORT ) )
        {
            while ( NULL != entry && TRUE == entry_comp( terms[ i ], entry ) )
            {
                sprintf( result, PRINT_FOUND, entry->name, entry->origin, entry->meaning );
                buffer_add( result );

                entry = entry->next;

                found = TRUE;
            }

            if ( FALSE == found )
            {
                sprintf( result, PRINT_NOT_FOUND, terms[ i ] );
                buffer_add( result );
            }
        }
        else
        {
            if ( NULL != entry )
            {
                while ( NULL != entry )
                {
                    if ( TRUE == entry_comp( terms[ i ], entry ) )
                    {
                        sprintf( result, PRINT_FOUND, entry->name, entry->origin, entry->meaning );
                        buffer_add( result );
                    }
                    
                    entry = entry->next;
                }
            }
            else
            {
                sprintf( result, PRINT_NOT_FOUND, terms[ i ] );
                buffer_add( result );
            }
        }
        
        i++;
    }
    
    end = time_gettime_usec();
    
    free( result ); /* free result variable */
    
    return time_get_diff_sec( start, end );
}