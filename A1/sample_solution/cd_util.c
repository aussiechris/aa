/****************************************************************************
 * COSC 2123 - Algorithms & Analysis
 * Assignment       : 1
 * Filename         : cd_util.c
 * Authors          : s9863998 - Tom Kaczocha
 *                  : 
 *                  : 
 ****************************************************************************/

#include "cd_util.h"

/**
 * Parses the command line arguments and returns required arguments.
 * 
 * @param int    argc   number of command line arguments
 * @param char** argv   command line arguments
 * @param char** names  pointer to names file
 * @param char** search pointer to search file
 * 
 * @return int hash size
 */
uint32_t get_options( int argc, char** argv, char** names, char** search )
{
    int c = 0;
    int hash_size = INITIAL_SIZE;
    int option_index = 0;
    
    static struct option long_options[] = {
        { "names",      required_argument, 0, 'n' },
        { "search",     required_argument, 0, 's' },
        { "hash",       required_argument, 0, 'z' },
        { "hash_fnv",   no_argument,       0, 'f' },
        { "hash_asc",   no_argument,       0, 'a' },
        { "array_sort", no_argument,       0, 'x' },
        { "list_sort",  no_argument,       0, 'l' },
        { "hash_sort",  no_argument,       0, 'r' },
        { "sequential", no_argument,       0, 'o' },
        { "binary",     no_argument,       0, 'b' },
        { "times",      no_argument,       0, 't' },
        { "verbose",    no_argument,       0, 'v' },
        { "help",       no_argument,       0, 'h' },
        { 0, 0, 0, 0, },
    };
    
    /* set DEFAULT flags */
    cd_options = 
              CD_ARRAY_BIN   /* use binary search */
            | CD_HASH_FNV;    /* use FNV hash function */
            
    
    while( -1 != ( c = getopt_long( argc, argv, "n:s:z:hbovfalrtx", long_options, &option_index ) ) )
    {
        switch( c )
        {
            case 'n':
                *names = str_dup( optarg );
                break;
            case 's':
                *search = str_dup( optarg );
                break;
            case 'z':
                hash_size = atol( optarg );
                break;
            case 'f': /* fnv hash function */
                /* cd_options |= CD_HASH_FNV; */
                break;
            case 'a': /* asci hash function - (toggle fnv) */
                cd_options ^= CD_HASH_FNV;
                break;
            case 'b': /* binary search */
                /* cd_options |= CD_ARRAY_BIN; */
                break;
            case 'o': /* sequential search - (toggle binary) */
                cd_options ^= CD_ARRAY_BIN;
                break;
            case 'v':
                cd_options |= CD_VERBOSE;
                break;
            case 'x': /* sorted array */
                cd_options |= CD_ARRAY_SORT;
                break;
            case 'l': /* sorted linked list */
                cd_options |= CD_LIST_SORT;
                break;
            case 'r':
                cd_options |= CD_HASH_SORT;
                break;
            case 't':
                cd_options |= CD_SORT_TIME;
                break;
            case 'h':
                usage();
                exit( EXIT_SUCCESS );
                break;
            case '?':
                perror( "Something is not quite right!\n" );
                exit( EXIT_FAILURE );
                break;
            default:
                break;
        }
    }
    
    if ( NULL == *names || NULL == *search )
    {
        usage();
        
        exit( EXIT_FAILURE );
    }
    
    /* Binary Search Check */
    if ( TRUE == OPTION_SET( CD_ARRAY_BIN ) )
    {
        cd_options |= CD_ARRAY_SORT;
    }
    
    return hash_size;
}

/**
 * Reads lines from the file and returns a list.
 * 
 * @param char*     file_name names file name
 * @param uint32_t* count     number of items read
 * 
 * @return char** list of lines
 */
char** get_items( char* file_name, uint32_t *count )
{
    char   buffer[ MAX_SIZE ];
    FILE*  file  = NULL;
    char** items = NULL;
    size_t size  = INITIAL_SIZE;
	
    file = fopen( file_name, "r" );
	
    if ( NULL == file )
    {
        exit( EXIT_FAILURE );
    }
	
    items = malloc( sizeof( char* ) * INITIAL_SIZE );
	
    while ( NULL != fgets( buffer, MAX_SIZE, file ) )
    {
        if ( strlen( buffer ) > 0 )
        {
            if ( '\n' == buffer[ strlen( buffer ) - 1 ] )
            {
                buffer[ strlen( buffer ) - 1 ] = '\0';
            }
            
            if ( 0 == strlen( buffer ) ) continue;
        }
        else
        {
            continue;
        }
        
        items[ *count ] = str_dup( buffer );
        
        (*count)++;

        if ( *count == size )
        {
            char** tmp = NULL;
            
            size = ( 2 * size );

            tmp = realloc( items, sizeof( char* ) * size );

            if ( NULL == tmp )
            {
                exit( EXIT_FAILURE );
            }

            items = tmp;
        }
    }

    fclose( file );

    return items;
}

/**
 * Free data memory.
 * 
 * @param uint32_t count number of items
 * @param char**   items string items list
 */
void free_list( uint32_t count, char** items )
{
    uint32_t i = 0;

    if ( NULL == items )
    {
        return;
    }

    while ( i < count )
    {
        free( items[ i ] );
        i++;
    }

    free( items );
}

/**
 * String duplication helper function.
 * 
 * @param char* str the string to duplicate on the heap
 * 
 * @return char* pointer to the new string
 */
char* str_dup( char* str )
{
    char* new_str = NULL;
    
    if ( NULL == str )
    {
        return NULL;
    }
    
    new_str = malloc( sizeof( char ) * strlen( str ) + 1 );
    strcpy( new_str, str );
    
    new_str[ strlen( str ) ] = '\0';
    
    if ( new_str[ strlen( str ) ] != '\0' )
    {
        printf( "New string is missing null terminator.\n" );
        
        exit( EXIT_FAILURE );
    }
    
    return new_str;
}

/**
 * Creates a string on the heap from passed in fragments.
 * 
 * @param int32_t     count number of arguments
 * @param const char* fmt   string format
 * @param const char* ...   argument list
 * 
 * @return char* string
 */
char* str_create( int32_t count, const char* fmt, ... )
{
    char*   str  = NULL;
    char*   tmp;
    va_list args;
    int32_t len  = 0;
    int32_t i    = 0;
    
    if ( NULL == fmt )
    {
        return NULL;
    }
    
    len = strlen( fmt );
    
    va_start( args, fmt );
    
    while ( i < count )
    {
        tmp = va_arg( args, char * );
        
        len += strlen( tmp );
        
        i++;
    }
    
    va_end( args );
    
    if ( NULL == ( str = malloc( sizeof( char ) * ( len + 1 ) ) ) )
    {
        return NULL;
    }
    
    va_start( args, fmt );
    
    vsprintf( str, fmt, args );
    
    va_end( args );
    
    return str;
}

/**
 * Displays the program usage information.
 */
void usage()
{
    printf( "\nAlgorithms & Analysis - Assignment 1\n" );
    printf( "Copyright (C) Tom Kaczocha\n" );
    printf( "This is free software: you are free to change and redistribute it.\n" );
    printf( "There is NO WARRANTY, to the extent permitted by law.\n\n" );
    
    printf( "Written by Tom Kaczocha.\n\n" );
    
    printf( "Usage: aa --names <names.file> --search <search.file>\n\n" );
    
    printf( "\t--names      (-n) - (REQUIRED) Names file\n" );
    printf( "\t--search     (-s) - (REQUIRED) Search file\n\n" );
    
    printf( "Optional:\n\n" );
    
    printf( "\t--hash       (-z) - Hash table size\n" );
    printf( "\t--hash_fnv   (-f) - FNV-1a hash function (DEFAULT)\n" );
    printf( "\t--hash_asc   (-a) - ASCI hash function\n" );
    
    printf( "\n" );
    
    printf( "\t--binary     (-b) - Binary array search (DEFAULT)\n" );
    printf( "\t--sequential (-o) - Sequential array search\n" );
    
    printf( "\n" );
    
    printf( "\t--array_sort (-x) - Use sorted array\n" );
    printf( "\t                    NOTE: If --binary option is set, this will always be set to TRUE\n" );
    printf( "\t--list_sort  (-l) - Use sorted list\n" );
    
    printf( "\n" );
    
    printf( "\t--time       (-t) - Measure & show sort times\n" );
    
    printf( "\n" );
    
    printf( "\nOther:\n\n" );
    
    printf( "\t--help       (-h) - Displays this help screen\n" );
    printf( "\t--verbose    (-v) - Display results to screen\n\n" );
    
    printf( "\n\n" );
}

/**
 * Get power of two.
 * 
 * @param int64_t number the number
 * 
 * @return int32_t count
 */
int32_t get_power2( int64_t number )
{
    int64_t no    = number;
    int32_t count = 0;
    
    while ( ( no / 2  ) > 0 )
    {
        no = no / 2;
        count++;
    }
    
    return count;
}