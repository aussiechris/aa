/****************************************************************************
 * COSC 2123 - Algorithms & Analysis
 * Assignment       : 1
 * Filename         : cd_time.c
 * Authors          : s9863998 - Tom Kaczocha
 *                  : 
 *                  : 
 ****************************************************************************/

#include "cd_time.h"
#include "cd_util.h"

/**
 * Returns the current time since EPOC.
 * 
 * @return int64_t time stamp in micro-seconds
 */
struct timeval* time_gettime()
{
    struct timeval* time;
    
    time = malloc( sizeof( struct timeval ) );
    
    gettimeofday( time, NULL );
    
    return time;
}

/**
 * Returns the current time since EPOC.
 * 
 * @return int64_t time stamp in micro-seconds
 */
int64_t time_gettime_usec()
{
    struct timeval time;
    
    gettimeofday( &time, NULL );
    
    return ( time.tv_sec * 1E6 ) + time.tv_usec;
}

/**
 * Returns the current time since EPOC in seconds.
 * 
 * @return int64_t time stamp in seconds
 */
float time_gettime_sec()
{
    return time_gettime_usec() / 1E6;
}

float time_time_to_sec( struct timeval* time )
{
    return time->tv_sec + ( time->tv_usec / 1E6 );
}

int64_t time_time_to_usec( struct timeval* time )
{
    return ( time->tv_sec * 1E6 ) + time->tv_usec;
}

/**
 * Calculates the time difference between two time stamps.
 * 
 * @param int64_t start start time stamp
 * @param int64_t end   end time stamp
 * 
 * @return int64_t time span in micro-seconds
 */
int64_t time_get_diff( int64_t start, int64_t end )
{
    return end - start;
}

/**
 * Calculates the time difference between two time stamps.
 * 
 * @param int64_t start start time stamp
 * @param int64_t end   end time stamp
 * 
 * @return float time span in seconds
 */
float time_get_diff_sec( int64_t start, int64_t end )
{
    return time_get_diff( start, end ) / 1E6;
}


/*******************************************************************************
 * 
 * Timeval List Structure
 * 
 ******************************************************************************/

/**
 * Initialises a times_t list.
 * 
 * @return times_t* times list
 */
times_t* time_init_list()
{
    times_t* times = NULL;
    
    times = malloc( sizeof( times_t ) );
    
    if ( NULL == times )
    {
        printf( "Failed to initialise times list." );
        
        exit( EXIT_FAILURE );
    }
    
    times->count = 0;
    times->max   = MAX_SIZE;
    times->times = malloc( sizeof( struct timeval* ) * MAX_SIZE );
    
    return times;
}

/**
 * Adds a timeval structure to times_t list.
 * 
 * @param times_t*        times list of times
 * @param struct timeval* time  new time to add
 * 
 * @return times_t* list of times
 */
times_t* time_add_time( times_t* times, struct timeval* time )
{
    if ( times->count == times->max )
    {
        struct timeval** temp = NULL;
        
        temp = realloc( times->times, sizeof( struct timeval* ) * ( times->max * 2 ) );
        
        if ( NULL == temp )
        {
            printf( "Failed to reallocate memory for times list." );
            
            exit( EXIT_FAILURE );
        }
        
        times->times = temp;
        times->max = times->max * 2;
    }
    
    times->times[ times->count ] = time;
    times->count++;
    
    return times;
}

/**
 * Calculates and returns the total time for list.
 * 
 * @param times_t* times list of times
 * 
 * @return int64_t total in micro-seconds
 */
int64_t time_get_list_total( times_t* times )
{
    int64_t total = 0;
    int32_t count = 0;
    
    while ( count < times->count )
    {
        int64_t start = 0;
        int64_t end   = 0;
        
        start = time_time_to_usec( times->times[ count ]     );
        end   = time_time_to_usec( times->times[ count + 1 ] );
        
        total += time_get_diff( start, end );
        
        count += 2;
    }
    
    return total;
}

/**
 * Deallocates memory for times_t list.
 * 
 * @param times_t* times list of times
 */
void time_free_list( times_t* times )
{
    int32_t count = 0;
    
    while ( count < times->count )
    {
        free( times->times[ count ] );
        count++;
    }
    
    free( times->times );
    free( times );
}