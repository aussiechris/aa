/****************************************************************************
 * COSC 2123 - Algorithms & Analysis
 * Assignment       : 1
 * Filename         : test.c
 * Authors          : s9863998 - Tom Kaczocha
 *                  : 
 *                  : 
 ****************************************************************************/

#include "cd_array.h"
#include "cd_test.h"

#include "tests/cd_array_tests.h"
#include "tests/cd_list_tests.h"
#include "tests/cd_hash_tests.h"
#include "tests/cd_common_tests.h"
#include "tests/cd_time_tests.h"
#include "tests/cd_util_tests.h"
#include "tests/cd_sort_tests.h"

/**
 * Application option flags.
 */
int32_t cd_options;

/* sort times */
times_t* times_sort_array;
times_t* times_sort_list;
times_t* times_sort_hash;

int main( int argc, char** argv)
{
    
    printf( "\n" );
    
    test_runner_init();
    
    /* UTIL Tests */
    test_add( "util", "test_str_dup",    test_str_dup    );
    test_add( "util", "test_str_create", test_str_create );
    test_add( "util", "test_get_power2", test_get_power2 );
    
    
    /* ARRAY Tests */
    test_add( "array", "test_array_create", test_array_create );
    test_add( "array", "test_array_insert", test_array_insert );
    test_add( "array", "test_array_resize", test_array_resize );
    
    test_add( "array", "test_array_insert_null",         test_array_insert_null );
    test_add( "array", "test_array_insert_empty_string", test_array_insert_empty_string );
    
    test_add( "array", "test_array_sort", test_array_sort );
    
    test_add( "array", "test_array_locate",   test_array_locate );
    test_add( "array", "test_array_locate_non_existent", test_array_locate_non_existent );
    test_add( "array", "test_array_locate_null",         test_array_locate_null         );
    test_add( "array", "test_array_locate_empty",        test_array_locate_empty        );
    
    
    /* LIST Tests */
    test_add( "list", "test_list_create",   test_list_create );
    test_add( "list", "test_list_insert",   test_list_insert );
    
    test_add( "list", "test_list_insert_null",         test_list_insert_null );
    test_add( "list", "test_list_insert_empty_string", test_list_insert_empty_string );
    
    test_add( "list", "test_list_locate",   test_list_locate );
    test_add( "list", "test_list_locate_non_existent", test_list_locate_non_existent );
    test_add( "list", "test_list_locate_null",         test_list_locate_null         );
    test_add( "list", "test_list_locate_empty",        test_list_locate_empty        );
    
    
    /* HASH Tests */
    test_add( "hash", "test_hash_create", test_hash_create );
    test_add( "hash", "test_hash_insert", test_hash_insert );
    
    test_add( "hash", "test_hash_insert_null",         test_hash_insert_null         );
    test_add( "hash", "test_hash_insert_empty_string", test_hash_insert_empty_string );
    
    test_add( "hash", "test_hash_locate",              test_hash_locate              );
    test_add( "hash", "test_hash_locate_non_existent", test_hash_locate_non_existent );
    test_add( "hash", "test_hash_locate_null",         test_hash_locate_null         );
    test_add( "hash", "test_hash_locate_empty",        test_hash_locate_empty        );
    
    
    /* ENTRY Tests */
    test_add( "common", "test_entry_create",       test_entry_create       );
    test_add( "common", "test_entry_create_null",  test_entry_create_null  );
    test_add( "common", "test_entry_create_empty", test_entry_create_empty );
    
    test_add( "common", "test_entry_compare", test_entry_compare );
    test_add( "common", "test_buffer_add",    test_buffer_add    );

    test_add( "options", "test_set_default_options",     test_set_default_options     );
    test_add( "options", "test_set_options_none",        test_set_options_none        );
    test_add( "options", "test_set_options_verbose",     test_set_options_verbose     );
    test_add( "options", "test_set_option_array_binary", test_set_option_array_binary );
    test_add( "options", "test_set_option_hash_fnv",     test_set_option_hash_fnv     );
    test_add( "options", "test_set_option_array_sort",   test_set_option_array_sort   );
    test_add( "options", "test_set_option_list_sort",    test_set_option_list_sort    );
    
    test_add( "options", "test_set_option_not_verbose",      test_set_option_not_verbose     );
    test_add( "options", "test_set_option_not_array_binary", test_set_option_not_array_binary );
    test_add( "options", "test_set_option_not_hash_fnv",     test_set_option_not_hash_fnv     );
    test_add( "options", "test_set_option_not_array_sort",   test_set_option_not_array_sort   );
    test_add( "options", "test_set_option_not_list_sort",    test_set_option_not_list_sort    );
    
    test_add( "options", "test_set_option_binary_sort",           test_set_option_binary_sort           );
    test_add( "options", "test_set_option_toggle_seq_search",     test_set_option_toggle_seq_search     );
    test_add( "options", "test_set_option_toggle_asci_hash_func", test_set_option_toggle_asci_hash_func );
    
    /* TIME Tests */
    test_add( "time", "test_time_get_time", test_time_get_time );
    test_add( "time", "test_time_get_diff", test_time_get_diff );
    
    /* SORT Tests */
    test_add( "sort", "test_sort_bubble_int",     test_sort_bubble_int     );
    test_add( "sort", "test_sort_bubble_strings", test_sort_bubble_strings );
    
    test_run();
    
    test_results();
    
    return EXIT_SUCCESS;
}