/****************************************************************************
 * COSC 2123 - Algorithms & Analysis
 * Assignment       : 1
 * Filename         : cd_hash.c
 * Authors          : s9863998 - Tom Kaczocha
 *                  : 
 *                  : 
 ****************************************************************************/

#include "cd_hash.h"
#include "cd_time.h"

extern times_t* times_sort_array;
extern times_t* times_sort_list;
extern times_t* times_sort_hash;

/**
 * Creates a new hash table.
 * 
 * @param uint32_t size table size
 * 
 * @return hash_t* new hash table
 */
hash_t* hash_create( uint32_t size )
{
    hash_t* hash = NULL;
    
    hash = malloc( sizeof( hash_t ) );
    
    if ( NULL == hash )
    {
        fprintf( stderr, "Failed to create new hash.\n" );
        
        exit( EXIT_FAILURE );
    }
    
    hash->size   = size;
    hash->count  = 0;
    hash->table  = calloc( 1, sizeof( entry_t* ) * size );
    
    hash->tsize  = sizeof( hash_t );
    hash->tsize += sizeof( entry_t* ) * size;
    
    if ( NULL == hash->table )
    {
        fprintf( stderr, "Failed to create new hash table.\n" );
        
        exit( EXIT_FAILURE );
    }
    
    return hash;
}

/**
 * The hash function.
 * 
 * @param hash_t* hash the hash table
 * @param char*   name entry key
 * 
 * @return uint32_t table index
 */
uint32_t hash_calc_asc( hash_t* hash, char* name )
{
    uint32_t hashval = 0x811c9dc5;
    uint32_t i       =  0;
    
    while ( i < strlen( name ) )
    {
        hashval += ( int ) name[ i ] * 0x01000193;
        
        i++;
    }
    
    hashval = hashval & ( ( 1 << get_power2( ( int64_t )hash->size ) ) - 1 );
    /*hashval = hashval % hash->size;*/
    
    return hashval;
}

/**
 * FNV Hash Function.
 * 
 * @param hash
 * @param name
 * 
 * @source http://stackoverflow.com/questions/34595/what-is-a-good-hash-function
 * 
 * @return uint32_t the hash value
 */
uint32_t hash_calc_fnv( hash_t* hash, char* name )
{
    unsigned hashval = 0x811c9dc5;
    uint32_t i = 0;
    
    for ( i = 0; i < strlen( name ); i++ )
    {
        hashval += ( hashval ^ name[ i ] ) * 0x01000193;
    }
    
    hashval = hashval & ( ( 1 << get_power2( ( int64_t )hash->size ) ) - 1 );
    
    return hashval;
}

/**
 * Inserts a new entry into the hash table.
 * 
 * @param hash_t* hash  the hash table
 * @param char*   input the entry data
 * 
 * @return BOOL TRUE on success, FALSE on error
 */
BOOL hash_insert( hash_t* hash, char* input )
{
    entry_t* new      = NULL;
    entry_t* previous = NULL;
    entry_t* current  = NULL;
    uint32_t hashval  = 0;
    
    if ( NULL == input || ( 0 == strlen( input ) ) )
    {
        return FALSE;
    }
    
    new = entry_create( input );
    
    if ( TRUE == OPTION_SET( CD_HASH_FNV ) )
    {
        hashval = hash_calc_fnv( hash, new->name );
    }
    else
    {
        hashval = hash_calc_asc( hash, new->name );
    }
    
    if ( NULL == hash->table[ hashval ] )
    {
        hash->table[ hashval ] = new;
    }
    else
    {
        current = hash->table[ hashval ];
        
        if ( TRUE == OPTION_SET( CD_HASH_SORT ) )
        {
            if ( TRUE == OPTION_SET( CD_SORT_TIME ) )
            {
                time_add_time( times_sort_hash, time_gettime() );
            }
            
            while ( current != NULL && strcmp( new->name, current->name ) >= 0 )
            {
                previous = current;
                current  = current->next;
            }

            /* first element */
            if ( NULL == previous )
            {
                new->next = hash->table[ hashval ];
                hash->table[ hashval ] = new;
            }

            /* last element */
            else if ( current == NULL )
            {
                previous->next = new;
            }

            /* middle element */
            else
            {
                new->next = previous->next;
                previous->next = new;
            }

            if ( TRUE == OPTION_SET( CD_SORT_TIME ) )
            {
                time_add_time( times_sort_hash, time_gettime() );
            }
        }
        else
        {
            new->next = current;
            hash->table[ hashval ] = new;
        }
    }
    
    hash->count++;
    hash->tsize += new->size;
    
    return TRUE;
}

/**
 * Find a key in the hash table.
 * 
 * @param hash_t* hash the hash table
 * @param char*   name the name ( key) to search for
 * 
 * @return entry_t* the entry or NULL if not found
 */
entry_t* hash_locate( hash_t* hash, char* name )
{
    uint32_t hashval = -1;
    entry_t* current = NULL;
    
    if ( NULL == name || ( 0 == strlen( name ) ) )
    {
        return NULL;
    }
    
    if ( TRUE == OPTION_SET( CD_HASH_FNV ) )
    {
        hashval = hash_calc_fnv( hash, name );
    }
    else
    {
        hashval = hash_calc_asc( hash, name );
    }
    
    if ( NULL != hash->table[ hashval ] )
    {
        current = hash->table[ hashval ];
        
        while ( current != NULL && ( FALSE == entry_comp( name, current ) ) )
        {
            current = current->next;
        }

        return current;
    }
    
    return NULL;
}

/**
 * Deallocates hash table memory.
 * 
 * @param hash_t* hash the hash table
 */
void hash_free( hash_t* hash )
{
    uint32_t i = 0;
    entry_t* current = NULL;
    entry_t* next    = NULL;
    
    while ( i < hash->size )
    {
        current = hash->table[ i ];
        
        while ( NULL != current )
        {
            next = current->next;
            
            entry_free( current );
            
            current = next;
        }
        
        i++;
    }
    
    free( hash->table );
    free( hash );
}

/**
 * Calculates the hash clustering.
 * 
 * @param hash_t*  hash the hash table
 * @param int32_t* min  min member int pointer
 * @param int32_t* max  max member int pointer
 */
void hash_get_clustering( hash_t* hash, int32_t* min, int32_t* max )
{
    entry_t* current = NULL;
    uint32_t i       = 0;
    
    while ( i < hash->size )
    {
        int32_t count = 0;
        
        current = hash->table[ i ];
        
        while ( NULL != current )
        {
            current = current->next;
            
            count++;
        }
        
        if ( count > *max )
        {
            *max = count;
        }
        
        if ( count < *min || ( -1 == *min ) )
        {
            *min = count;
        }
        
        i++;
    }
}

/**
 * Returns the hash distribution arrary.
 * 
 * @param hash_t* hash the hash table
 * 
 * @return int32_t** distribution array
 */
int32_t** hash_get_distribution( hash_t* hash )
{
    entry_t*  current = NULL;
    int32_t** dist    = NULL;
    int32_t*  row     = NULL;
    uint32_t  i       = 0;
    
    dist = malloc( sizeof( int32_t* ) * hash->size );
 
    if ( NULL == dist )
    {
        printf( "Failed to allocate memory for hash distribution table." );
        
        exit( EXIT_FAILURE );
    }
    
    while ( i < hash->size )
    {
        int32_t count = 0;
        
        current = hash->table[ i ];
        
        while ( NULL != current )
        {
            current = current->next;
            
            count++;
        }
        
        row = malloc( sizeof( int32_t ) * 2 );
        
        row[ 0 ]  = i;
        row[ 1 ]  = count;
        
        dist[ i ] = row;
        
        i++;
    }
    
    return dist;
}

/**
 * Deallocates distribution array memory.
 * 
 * @param hash_t*   hash the hash table
 * @param int32_t** dist the distribution array
 */
void hash_free_distribution( hash_t* hash, int32_t** dist )
{
    uint32_t i = 0;
    
    while ( i < hash->size )
    {
        free( dist[ i ] );
        
        i++;
    }
    
    free( dist );
}

/**
 * Writes the hash distribution to a file.
 * 
 * @param hash_t*   hash the hash table
 * @param int32_t** dist the distribution array
 */
void write_distribution( hash_t* hash, int32_t** dist )
{
    FILE* file;
    uint32_t i = 0;
    char* str = NULL;
    char* file_name = NULL;
    
    file_name = malloc( sizeof( char ) * MAX_SIZE );
    
    sprintf( file_name, "data/distibution-%d.txt", hash->size );
    
    file = fopen( file_name, "w" );
    
    str = malloc( sizeof( char ) * MAX_SIZE );
    
    /* output the header row */
    sprintf( str, "%s\t%s\n", "Index", "Members" );
    fputs( str, file );
    
    while ( i < hash->size )
    {
        sprintf( str, "%d\t%d\n", dist[ i ][ 0 ], dist[ i ][ 1 ] );
        
        fputs( str, file );
        
        i++;
    }
    
    fclose( file );
    
    free( str );
    free( file_name );
}