/****************************************************************************
 * COSC 2123 - Algorithms & Analysis
 * Assignment       : 1
 * Filename         : cd_list.c
 * Authors          : s9863998 - Tom Kaczocha
 *                  : 
 *                  : 
 ****************************************************************************/

#include "cd_list.h"
#include "cd_time.h"

extern times_t* times_sort_array;
extern times_t* times_sort_list;
extern times_t* times_sort_hash;

/**
 * Creates a new linked list.
 * 
 * @return list_t* list pointer
 */
list_t* list_create()
{
    list_t* list = NULL;
    
    list = malloc( sizeof( list_t ) );
    
    if ( NULL == list )
    {
        fprintf( stderr, "Failed to create new linked list.\n" );
        
        exit( EXIT_FAILURE );
    }
    
    list->count = 0;
    list->tsize  = sizeof( list_t );
    list->head  = NULL;
    list->tail  = NULL;
    
    return list;
}

/**
 * Inserts a new entry into list.
 * 
 * @param list_t* list  the linked list
 * @param char*   input entry input
 * 
 * @return BOOL TRUE on success, FALSE on error
 */
BOOL list_insert( list_t* list, char* input )
{
    entry_t* current  = NULL;
    entry_t* previous = NULL;
    entry_t* new      = NULL;
    
    if ( NULL == input || ( 0 == strlen( input ) ) )
    {
        return FALSE;
    }
    
    new = entry_create( input );
    
    current = list->head;
    
    if ( NULL == current )
    {
        list->head = new;
        list->tail = new;
    }
    else
    {
        if ( TRUE == OPTION_SET( CD_LIST_SORT ) )
        {
            if ( TRUE == OPTION_SET( CD_SORT_TIME ) )
            {
                time_add_time( times_sort_list, time_gettime() );
            }
            
            while ( NULL != current && strcmp( new->name, current->name ) >= 0 )
            {
                previous = current;
                current  = current->next;
            }

            /* first element */
            if ( NULL == previous )
            {
                new->next  = list->head;
                list->head = new;
            }

            /* last element */
            else if ( NULL == current )
            {
                previous->next = new;
                list->tail     = new;
            }

            /* middle element */
            else
            {
                new->next = previous->next;
                previous->next = new;
            }
            
            if ( TRUE == OPTION_SET( CD_SORT_TIME ) )
            {
                time_add_time( times_sort_list, time_gettime() );
            }
        }
        else
        {
            list->tail->next = new;
            list->tail       = new;
        }
    }
    
    list->count++;
    list->tsize += new->size;
    
    return TRUE;
}

/**
 * Find a key in the linked list table.
 * 
 * @param list_t* list the linked list
 * @param char*   name the name (key) we're looking for
 * 
 * @return entry_t* the entry or NULL if not found
 */
entry_t* list_locate( list_t* list, char* name )
{
    entry_t* current = NULL;
    
    if ( NULL == name || ( 0 == strlen( name ) ) )
    {
        return NULL;
    }
    
    current = list->head;
    
    while ( current != NULL && ( FALSE == entry_comp( name, current ) ) )
    {
        current = current->next;
    }
    
    return current;
}

/**
 * Deallocate linked list memory.
 * 
 * @param list_t* list the linked list
 */
void list_free( list_t* list )
{
    entry_t* current = NULL;
    entry_t* next    = NULL;
    
    current = list->head;
    
    while ( NULL != current )
    {
        next = current->next;
        
        entry_free( current );
        
        current = next;
    }
    
    free( list );
}