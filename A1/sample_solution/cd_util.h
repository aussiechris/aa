/****************************************************************************
 * COSC 2123 - Algorithms & Analysis
 * Assignment       : 1
 * Filename         : cd_util.h
 * Authors          : s9863998 - Tom Kaczocha
 *                  : 
 *                  : 
 ****************************************************************************/

#ifndef CD_UTIL_H
#define	CD_UTIL_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdarg.h>
#include <getopt.h>
    
#include "cd_hash.h"
#include "cd_array.h"

#define MAX_SIZE     1024

uint32_t get_options( int, char**, char**, char**  );
char**   get_items  ( char*, uint32_t *count       );
void     free_list  ( uint32_t count, char** items );

char*    str_dup    ( char* str );
char*    str_create( int32_t count, const char* fmt, ... );

void     usage();

int32_t get_power2( int64_t );

#ifdef	__cplusplus
}
#endif

#endif	/* CD_UTIL_H */