/****************************************************************************
 * COSC 2123 - Algorithms & Analysis
 * Assignment       : 1
 * Filename         : cd_test.h
 * Authors          : s9863998 - Tom Kaczocha
 *                  : 
 *                  : 
 ****************************************************************************/

#ifndef CD_TEST_H
#define	CD_TEST_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "cd_common.h"

/*#include "execinfo.h"*/

typedef void ( *test_func_ptr )( void );

typedef struct cd_test
{
    test_func_ptr test;
    char*         test_name;
    char*         test_group;
    uint64_t      start_time;
    uint64_t      end_time;
    char*         message;
    short         status;
} test_t;

/*
#ifndef Boolean
enum Boolean { FALSE, TRUE };
typedef enum Boolean BOOL;
#endif
*/

typedef enum Sort_By { GROUP = 0, NAME = 1 } TestSort;

extern test_t** tests;


void    test_add   ( char*, char*, void ( *test )( void ) );
test_t* test_create( char*, char*, test_func_ptr );

void test_run();
void test_runner_init();
void test_results();
void test_sort( TestSort );
int test_comp( void**, void** );

void test_name_check ( void*, void* );
void test_group_check( void*, void* );

void assert_str_equals( char*, char* );
void assert_str_not_null( char* );

void assert_str_len_equals( char*, int32_t );
void assert_str_len_greater_then( char*, int32_t );

void assert_not_null( void* );
void assert_null( void* );

void assert_true ( BOOL );
void assert_false( BOOL );

void assert_int_equals( int32_t, int32_t );
void assert_int_not_equals( int32_t, int32_t );
void assert_int_greater_then( int32_t, int32_t );

void assert_long_equals( int64_t, int64_t );
void assert_long_not_equals( int64_t, int64_t );
void assert_long_greater_then( int64_t, int64_t );

#ifdef	__cplusplus
}
#endif

#endif	/* CD_TEST_H */