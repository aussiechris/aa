/****************************************************************************
 * COSC 2123 - Algorithms & Analysis
 * Assignment       : 1
 * Filename         : cd_array.h
 * Authors          : s9863998 - Tom Kaczocha
 *                  : 
 *                  : 
 ****************************************************************************/

#ifndef CD_ARRAY_H
#define	CD_ARRAY_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "cd_common.h"
#include "cd_sort.h"

typedef struct array
{
    uint32_t  size;
    uint32_t  count;
    uint64_t  tsize;
    entry_t** items;
} array_t;

array_t* array_create ( uint32_t           );
BOOL     array_resize ( array_t*, uint32_t );
BOOL     array_insert ( array_t*, char*    );
array_t* array_sort   ( array_t*          );
void     array_display( array_t*           );
array_t* array_remove ( array_t*, char*    );
void     array_free   ( array_t*           );

int32_t  array_locate_sequential( array_t*, char* );
int32_t  array_locate_binary    ( array_t*, char* );

#ifdef	__cplusplus
}
#endif

#endif	/* CD_ARRAY_H */