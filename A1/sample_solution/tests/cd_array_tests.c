/****************************************************************************
 * COSC 2123 - Algorithms & Analysis
 * Assignment       : 1
 * Filename         : cd_array_tests.c
 * Authors          : s9863998 - Tom Kaczocha
 *                  : 
 *                  : 
 ****************************************************************************/

#include "cd_array_tests.h"

/**
 * Test creating an array structure.
 */
void test_array_create()
{
    /* GIVEN */
    array_t* array = NULL;
    uint32_t size  = 0;
    
    /* WHEN */
    array = array_create( 100 );
    
    /* THEN */
    assert_not_null( array );
    assert_not_null( array->items   );
    
    assert_int_equals( 100, array->size  );
    assert_int_equals(   0, array->count );
    
    size  = sizeof( array_t  );
    size += sizeof( entry_t* ) * 100;
    
    assert_int_equals( size, array->tsize );
    
    array_free( array );
}

/**
 * Test inserting entries into array.
 */
void test_array_insert()
{
    /* GIVEN */
    array_t* array = NULL;
    entry_t* entry = NULL;
    
    array = array_create( 2 );
    
    /* WHEN - THEN */
    assert_true( array_insert( array, "Tom\tEnglish\tTwin\n" ) );
    
    assert_int_equals( 1, array->count );
    
    entry = array->items[ 0 ];
    
    assert_not_null( entry );
    assert_not_null( entry->name );
    assert_not_null( entry->origin );
    assert_not_null( entry->meaning );
    
    /* WHEN - THEN */
    assert_true( array_insert( array, "John\tEnglish\tSome other meaning\n" ) );
    
    assert_int_equals( 2, array->count );
    
    entry = array->items[ 1 ];
    
    assert_not_null( entry );
    assert_not_null( entry->name );
    assert_not_null( entry->origin );
    assert_not_null( entry->meaning );
    
    array_free( array );
}

/**
 * Test inserting NULL value.
 */
void test_array_insert_null()
{
    /* GIVEN */
    array_t* array = NULL;
    
    array = array_create( 2 );
    
    assert_not_null( array );
    
    /* WHEN - THEN */
    assert_false( array_insert( array, NULL ) );
    
    assert_int_equals( 0, array->count );
    
    array_free( array );
}

/**
 * Test inserting empty string value.
 */
void test_array_insert_empty_string()
{
    /* GIVEN */
    array_t* array = NULL;
    
    array = array_create( 2 );
    
    assert_not_null( array );
    
    /* WHEN - THEN */
    assert_false( array_insert( array, "" ) );
    
    assert_int_equals( 0, array->count );
    
    array_free( array );
}

/**
 * Test array resize.
 */
void test_array_resize()
{
    /* GIVEN */
    array_t* array = NULL;
    
    array = array_create( 2 );
    
    assert_not_null( array );
    assert_int_equals( 2, array->size );
    
    /* WHEN - THEN */
    assert_true( array_resize( array, 10 ) );
    
    assert_not_null( array );
    assert_int_equals( 10, array->size );
    
    array_free( array );
}

/**
 * Test array sort.
 */
void test_array_sort()
{
    /* GIVEN */
    array_t* array = NULL;
    
    array = array_create( 5 );
    
    assert_not_null( array );
    
    cd_options |= CD_ARRAY_SORT;
    
    _array_insert_entries( array );
    
    /* WHEN */
    /* sorted on insert */
    
    /* THEN */
    assert_str_equals( "Adam",     array->items[ 0 ]->name );
    assert_str_equals( "Danielle", array->items[ 1 ]->name );
    assert_str_equals( "Paul",     array->items[ 2 ]->name );
    assert_str_equals( "Tom",      array->items[ 3 ]->name );
    
    assert_int_equals( 4, array->count );
    
    array_free( array );
}

/**
 * Test array locate.
 */
void test_array_locate()
{
    /* GIEN */
    array_t* array = NULL;
    entry_t* entry = NULL;
    int32_t  index = 0;
    
    array = array_create( INITIAL_SIZE );
    
    assert_not_null( array );
    
    _array_insert_entries( array );
    
    assert_int_equals( 4, array->count );
    
    /* WHEN */
    index = array_locate_binary( array, "Tom" );
    
    /* THEN */
    assert_not_null( array->items[ index ] );
    
    if ( NULL == entry ) { return; }
    
    assert_str_equals( "Tom",     entry->name );
    assert_str_equals( "English", entry->origin );
    assert_str_equals( "Fourth",  entry->meaning );
    
    entry = NULL;
    
    assert_null( entry );
    
    /* WHEN */
    index = array_locate_sequential( array, "Tom" );
    
    /* THEN */
    assert_not_null( array->items[ index ] );
    
    if ( NULL == entry ) { return; }
    
    assert_str_equals( "Tom",     entry->name );
    assert_str_equals( "English", entry->origin );
    assert_str_equals( "Fourth",  entry->meaning );
    
    array_free( array );
}

/**
 * Test array locate non-existent entry.
 */
void test_array_locate_non_existent()
{
    /* GIVEN */
    array_t*  array  = NULL;
    
    array = array_create( INITIAL_SIZE );
    
    assert_not_null( array );
    
    _array_insert_entries( array );
    
    assert_int_equals( 4, array->count );
    
    /* WHEN - THEN */
    assert_int_equals( -1, array_locate_binary    ( array, "Sarrah" ) );
    assert_int_equals( -1, array_locate_sequential( array, "Sarrah" ) );
    
    array_free( array );
}

/**
 * Test hash locate with NULL key.
 */
void test_array_locate_null()
{
    /* GIVEN */
    array_t*  array  = NULL;
    
    array = array_create( INITIAL_SIZE );
    
    assert_not_null( array );
    
    _array_insert_entries( array );
    
    assert_int_equals( 4, array->count );
    
    /* WHEN - THEN */
    assert_int_equals( -1, array_locate_binary    ( array, "Sarrah" ) );
    assert_int_equals( -1, array_locate_sequential( array, "Sarrah" ) );
    
    array_free( array );
}

/**
 * Test array locate with empty key.
 */
void test_array_locate_empty()
{
    /* GIVEN */
    array_t*  array  = NULL;
    
    array = array_create( INITIAL_SIZE );
    
    assert_not_null( array );
    
    _array_insert_entries( array );
    
    assert_int_equals( 4, array->count );
    
    /* WHEN - THEN */
    assert_int_equals( -1, array_locate_binary    ( array, "Sarrah" ) );
    assert_int_equals( -1, array_locate_sequential( array, "Sarrah" ) );
    
    array_free( array );
}


/*******************************************************************************
 * 
 * TEST HELPERS
 * 
 ******************************************************************************/

/**
 * Helper function to insert default entries into array.
 * 
 * @param array_t* array the array
 */
void _array_insert_entries( array_t* array )
{
    assert_true( array_insert( array, "Danielle\tEnglish\tSecond\n" ) );
    assert_true( array_insert( array, "Adam\tEnglish\tFirst \n"    ) );
    assert_true( array_insert( array, "Paul\tEnglish\tThird\n"     ) );
    assert_true( array_insert( array, "Tom\tEnglish\tFourth\n" ) );
}