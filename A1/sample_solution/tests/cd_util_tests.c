/****************************************************************************
 * COSC 2123 - Algorithms & Analysis
 * Assignment       : 1
 * Filename         : cd_util_tests.c
 * Authors          : s9863998 - Tom Kaczocha
 *                  : 
 *                  : 
 ****************************************************************************/

#include "cd_util_tests.h"

/**
 * Test string duplication.
 */
void test_str_dup()
{
    /* GIVEN */
    char* str = NULL;
    
    /* WHEN */
    str = str_dup( "New String" );
    
    /* THEN */
    assert_str_not_null( str );
    
    assert_str_equals( "New String", str );
    
    free( str );
}

/**
 * Test string creation.
 */
void test_str_create()
{
    /* GIVEN */
    char* str = NULL;
    
    /* WHEN */
    str = str_create( 4, "%s %s %s %s", "This", "is", "the", "string" );
    
    /* THEN */
    assert_str_not_null( str );
    
    assert_str_equals( "This is the string", str );
    
    free( str);
}

void test_get_power2()
{
    /* GIVEN */
    int64_t number[ 7 ] = { 2, 4, 8, 16, 32, 64, 128 };
    int32_t result[ 7 ];
    
    /* WHEN */
    result[ 0 ] = get_power2( number[ 0 ] );
    result[ 1 ] = get_power2( number[ 1 ] );
    result[ 2 ] = get_power2( number[ 2 ] );
    result[ 3 ] = get_power2( number[ 3 ] );
    result[ 4 ] = get_power2( number[ 4 ] );
    result[ 5 ] = get_power2( number[ 5 ] );
    result[ 6 ] = get_power2( number[ 6 ] );
    
    /* THEN */
    assert_int_equals( 1, result[ 0 ] );
    assert_int_equals( 2, result[ 1 ] );
    assert_int_equals( 3, result[ 2 ] );
    assert_int_equals( 4, result[ 3 ] );
    assert_int_equals( 5, result[ 4 ] );
    assert_int_equals( 6, result[ 5 ] );
    assert_int_equals( 7, result[ 6 ] );
}