
#include "cd_time_tests.h"

/**
 * Test time get time.
 * 
 * Make sure that the time is at least 1,000,000 ( 1 MILLION )
 * in size.
 */
void test_time_get_time()
{
    /* GIVEN */
    uint64_t current_time = 0;
    
    /* WHEN */
    current_time = time_gettime_usec();
    
    /* THEN */
    assert_long_greater_then( current_time, 1000000 );
}

/**
 * Test time get time diff.
 */
void test_time_get_diff()
{
    /* GIVEN */
    int64_t start = 1000000;
    int64_t end   = 1100000;
    int64_t diff  = 0;
    
    /* WHEN */
    diff = time_get_diff( start, end );
    
    /* THEN */
    assert_long_equals( 100000, diff );
}