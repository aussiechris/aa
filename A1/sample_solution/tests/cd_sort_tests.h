/****************************************************************************
 * COSC 2123 - Algorithms & Analysis
 * Assignment       : 1
 * Filename         : cd_sort_tests.h
 * Authors          : s9863998 - Tom Kaczocha
 *                  : 
 *                  : 
 ****************************************************************************/

#ifndef CD_SORT_TESTS_H
#define	CD_SORT_TESTS_H

#include "../cd_test.h"
#include "../cd_common.h"
#include "../cd_sort.h"
#include "../cd_util.h"

#ifdef	__cplusplus
extern "C" {
#endif

void test_sort_bubble_int();
void test_sort_bubble_strings();

void int_check( void*, void* );
void str_check( void*, void* );

#ifdef	__cplusplus
}
#endif

#endif	/* CD_SORT_TESTS_H */