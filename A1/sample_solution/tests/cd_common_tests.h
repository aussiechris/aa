/****************************************************************************
 * COSC 2123 - Algorithms & Analysis
 * Assignment       : 1
 * Filename         : cd_common_tests.h
 * Authors          : s9863998 - Tom Kaczocha
 *                  : 
 *                  : 
 ****************************************************************************/

#ifndef CD_COMMON_TESTS_H
#define	CD_COMMON_TESTS_H

#include "../cd_test.h"
#include "../cd_common.h"

#ifdef	__cplusplus
extern "C" {
#endif

void test_entry_create();
void test_entry_create_null();
void test_entry_create_empty();

void test_entry_compare();
void test_buffer_add();

void test_set_options_none();
void test_set_options_verbose();
void test_set_option_array_binary();
void test_set_option_hash_fnv();
void test_set_option_array_sort();
void test_set_option_list_sort();

void test_set_option_not_verbose();
void test_set_option_not_array_binary();
void test_set_option_not_hash_fnv();
void test_set_option_not_array_sort();
void test_set_option_not_list_sort();

void test_set_default_options();
void test_set_option_binary_sort();
void test_set_option_toggle_seq_search();
void test_set_option_toggle_asci_hash_func();

#ifdef	__cplusplus
}
#endif

#endif	/* CD_COMMON_TESTS_H */