/****************************************************************************
 * COSC 2123 - Algorithms & Analysis
 * Assignment       : 1
 * Filename         : cd_time_tests.c
 * Authors          : s9863998 - Tom Kaczocha
 *                  : 
 *                  : 
 ****************************************************************************/

#ifndef CD_TIME_TESTS_H
#define	CD_TIME_TESTS_H

#include "../cd_common.h"
#include "../cd_time.h"
#include "../cd_test.h"

#ifdef	__cplusplus
extern "C" {
#endif

void test_time_get_time();
void test_time_get_diff();

#ifdef	__cplusplus
}
#endif

#endif	/* CD_TIME_TESTS_H */