/****************************************************************************
 * COSC 2123 - Algorithms & Analysis
 * Assignment       : 1
 * Filename         : cd_list_tests.c
 * Authors          : s9863998 - Tom Kaczocha
 *                  : 
 *                  : 
 ****************************************************************************/

#include "cd_list_tests.h"

/**
 * Test list create
 */
void test_list_create()
{
    /* GIVEN */
    list_t* list  = NULL;
    
    /* WHEN */
    list = list_create( );
    
    /* THEN */
    assert_not_null( list );
    assert_null( list->head );
    assert_null( list->tail );
    
    assert_int_equals( 0, list->count );
    
    assert_int_equals( sizeof( list_t ), list->tsize );
    
    list_free( list );
}

/**
 * Test list insert with valid input.
 */
void test_list_insert()
{
    /* GIVEN */
    list_t* list  = NULL;
    
    list = list_create( );
    
    assert_not_null( list );
    
    /* WHEN - THEN */
    assert_true( list_insert( list, "Tom\tEnglish\tThe meaning\n" ) );
    
    assert_int_equals( 1, list->count );
    
    list_free( list );
}


/**
 * Test inserting NULL value.
 */
void test_list_insert_null()
{
    /* GIVEN */
    list_t* list  = NULL;
    
    list = list_create( );
    
    assert_not_null( list );
    
    /* WHEN - THEN */
    assert_false( list_insert( list, NULL ) );
    
    assert_int_equals( 0, list->count );
    
    list_free( list );
}

/**
 * Test inserting empty string value.
 */
void test_list_insert_empty_string()
{
    /* GIVEN */
    list_t* list  = NULL;
    
    list = list_create( );
    
    assert_not_null( list );
    
    /* WHEN - THEN */
    assert_false( list_insert( list, "" ) );
    
    assert_int_equals( 0, list->count );
    
    list_free( list );
}

/**
 * Test list locate.
 */
void test_list_locate()
{
    /* GIVEN */
    list_t*  list  = NULL;
    entry_t* entry = NULL;
    
    list = list_create( );
    
    assert_not_null( list );
    
    _list_insert_entries( list );
    
    assert_int_equals( 3, list->count );
    
    /* WHEN */
    entry = list_locate( list, "Tom" );
    
    /* THEN */
    assert_not_null( entry );
    
    if ( NULL == entry ) { return; }
    
    assert_str_equals( "Tom",         entry->name );
    assert_str_equals( "English",     entry->origin );
    assert_str_equals( "The meaning", entry->meaning );
    
    list_free( list );
}

/**
 * Test list locate non-existent entry.
 */
void test_list_locate_non_existent()
{
    /* GIVEN */
    list_t*  list  = NULL;
    
    list = list_create();
    
    assert_not_null( list );
    
    _list_insert_entries( list );
    
    assert_int_equals( 3, list->count );
    
    /* WHEN - THEN */
    assert_null( list_locate( list, "Sarrah" ) );
    
    list_free( list );
}

/**
 * Test list locate with NULL key.
 */
void test_list_locate_null()
{
    /* GIVEN */
    list_t*  list  = NULL;
    
    list = list_create( INITIAL_SIZE );
    
    assert_not_null( list );
    
    _list_insert_entries( list );
    
    assert_int_equals( 3, list->count );
    
    /* WHEN - THEN */
    assert_null( list_locate( list, NULL ) );
    
    list_free( list );
}

/**
 * Test list locate with empty key.
 */
void test_list_locate_empty()
{
    /* GIVEN */
    list_t*  list  = NULL;
    entry_t* entry = NULL;
    
    list = list_create( INITIAL_SIZE );
    
    assert_not_null( list );
    
    _list_insert_entries( list );
    
    assert_int_equals( 3, list->count );
    
    /* WHEN */
    entry = list_locate( list, "" );
    
    /* THEN */
    assert_null( entry );
    
    list_free( list );
}


/*******************************************************************************
 * 
 * TEST HELPERS
 * 
 ******************************************************************************/

/**
 * Helper function to insert default entries into list.
 * 
 * @param list_t* hash the hash
 */
void _list_insert_entries( list_t* list )
{
    assert_true( list_insert( list, "Tom\tEnglish\tThe meaning\n" ) );
    assert_true( list_insert( list, "John\tEnglish\tThe meaning" ) );
    assert_true( list_insert( list, "Marry\tEnglish\tThe meaning\n" ) );
}