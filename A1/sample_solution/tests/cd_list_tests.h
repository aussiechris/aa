/****************************************************************************
 * COSC 2123 - Algorithms & Analysis
 * Assignment       : 1
 * Filename         : cd_list_tests.h
 * Authors          : s9863998 - Tom Kaczocha
 *                  : 
 *                  : 
 ****************************************************************************/

#ifndef CD_LIST_TESTS_H
#define	CD_LIST_TESTS_H

#include "../cd_test.h"
#include "../cd_list.h"

#ifdef	__cplusplus
extern "C" {
#endif

void test_list_create();
void test_list_insert();

void test_list_insert_null();
void test_list_insert_empty_string();

void test_list_locate();
void test_list_locate_non_existent();
void test_list_locate_null();
void test_list_locate_empty();

void _list_insert_entries( list_t* list );

#ifdef	__cplusplus
}
#endif

#endif	/* CD_LIST_TESTS_H */