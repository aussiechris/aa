/****************************************************************************
 * COSC 2123 - Algorithms & Analysis
 * Assignment       : 1
 * Filename         : cd_hash_tests.c
 * Authors          : s9863998 - Tom Kaczocha
 *                  : 
 *                  : 
 ****************************************************************************/

#include "cd_hash_tests.h"

/**
 * Test creating hash.
 */
void test_hash_create()
{
    /* GIVEN */
    hash_t* hash  = NULL;
    uint32_t init_size = 4;
    uint32_t size = 0;
    
    /* WHEN */
    hash = hash_create( init_size );
    
    /* THEN */
    assert_not_null( hash );
    assert_int_equals( 0,         hash->count );
    assert_int_equals( init_size, hash->size  );
    
    size  = sizeof( hash_t );
    size += sizeof( entry_t* ) * init_size;
    
    assert_int_equals( size, hash->tsize );
    
    hash_free( hash );
}

/**
 * Test hash insert with valid input.
 */
void test_hash_insert()
{
    /* GIVEN */
    hash_t* hash       = NULL;
    uint32_t init_size = 4;
    
    hash = hash_create( init_size );
    
    assert_not_null( hash );
    
    /* WHEN - THEN */
    assert_true( hash_insert( hash, "Tom\tEnglish\tThe meaning\n" ) );
    
    assert_int_equals( 1, hash->count );
    
    hash_free( hash );
}

/**
 * Test inserting NULL value.
 */
void test_hash_insert_null()
{
    /* GIEN */
    hash_t* hash       = NULL;
    uint32_t init_size = 4;
    
    hash = hash_create( init_size );
    
    assert_not_null( hash );
    
    /* WHEN - THEN */
    assert_false( hash_insert( hash, NULL ) );
    
    assert_int_equals( 0, hash->count );
    
    hash_free( hash );
}

/**
 * Test inserting empty string value.
 */
void test_hash_insert_empty_string()
{
    /* GIVEN */
    hash_t* hash       = NULL;
    uint32_t init_size = 4;
    
    hash = hash_create( init_size );
    
    assert_not_null( hash );
    
    /* WHEN - THEN */
    assert_false( hash_insert( hash, "" ) );
    
    assert_int_equals( 0, hash->count );
    
    hash_free( hash );
}

/**
 * Test hash locate.
 */
void test_hash_locate()
{
    /* GIVEN */
    hash_t*  hash  = NULL;
    entry_t* entry = NULL;
    
    hash = hash_create( INITIAL_SIZE );
    
    assert_not_null( hash );
    
    _hash_insert_entries( hash );
    
    assert_int_equals( 3, hash->count );
    
    /* WHEN */
    entry = hash_locate( hash, "Tom" );
    
    /* THEN */
    assert_not_null( entry );
    
    if ( NULL == entry ) { return; }
    
    assert_str_equals( "Tom",         entry->name );
    assert_str_equals( "English",     entry->origin );
    assert_str_equals( "The meaning", entry->meaning );
    
    hash_free( hash );
}

/**
 * Test hash locate non-existent entry.
 */
void test_hash_locate_non_existent()
{
    /* GIVEN */
    hash_t*  hash  = NULL;
    
    hash = hash_create( INITIAL_SIZE );
    
    assert_not_null( hash );
    
    _hash_insert_entries( hash );
    
    assert_int_equals( 3, hash->count );
    
    /* WHEN - THEN */
    assert_null( hash_locate( hash, "Sarrah" ) );
    
    hash_free( hash );
}

/**
 * Test hash locate with NULL key.
 */
void test_hash_locate_null()
{
    /* GIVEN */
    hash_t*  hash  = NULL;
    
    hash = hash_create( INITIAL_SIZE );
    
    assert_not_null( hash );
    
    _hash_insert_entries( hash );
    
    assert_int_equals( 3, hash->count );
    
    /* WHEN - THEN */
    assert_null( hash_locate( hash, NULL ) );
    
    hash_free( hash );
}

/**
 * Test hash locate with empty key.
 */
void test_hash_locate_empty()
{
    /* GIVEN */
    hash_t*  hash  = NULL;
    entry_t* entry = NULL;
    
    hash = hash_create( INITIAL_SIZE );
    
    assert_not_null( hash );
    
    _hash_insert_entries( hash );
    
    assert_int_equals( 3, hash->count );
    
    /* WHEN*/
    entry = hash_locate( hash, "" );
    
    /* THEN */
    assert_null( entry );
    
    hash_free( hash );
}


/*******************************************************************************
 * 
 * TEST HELPERS
 * 
 ******************************************************************************/

/**
 * Helper function to insert default entries into hash.
 * 
 * @param hash_t* hash the hash
 */
void _hash_insert_entries( hash_t* hash )
{
    assert_true( hash_insert( hash, "Tom\tEnglish\tThe meaning\n" ) );
    assert_true( hash_insert( hash, "John\tEnglish\tThe meaning" ) );
    assert_true( hash_insert( hash, "Marry\tEnglish\tThe meaning\n" ) );
}