/****************************************************************************
 * COSC 2123 - Algorithms & Analysis
 * Assignment       : 1
 * Filename         : cd_common_tests.c
 * Authors          : s9863998 - Tom Kaczocha
 *                  : 
 *                  : 
 ****************************************************************************/

#include "cd_common_tests.h"

/**
 * Test create entry.
 */
void test_entry_create()
{
    /* GIVEN */
    entry_t* entry = NULL;
    
    /* WHEN */
    entry = entry_create( "Tom\tEnglish\tEntry meaning" );
    
    /* THEN */
    assert_not_null( entry );
    
    if ( NULL == entry ) { return; }
    
    assert_str_equals( "Tom",           entry->name );
    assert_str_equals( "English",       entry->origin );
    assert_str_equals( "Entry meaning", entry->meaning );
    
    entry_free( entry );
}

/**
 * Test create entry with NULL input.
 */
void test_entry_create_null()
{
    /* GIVEN */
    entry_t* entry = NULL;
    
    /* WHEN */
    entry = entry_create( NULL );
    
    /* THEN */
    assert_null( entry );
}

/**
 * Test create entry with empty input.
 */
void test_entry_create_empty()
{
    /* GIVEN */
    entry_t* entry = NULL;
    
    /* WHEN */
    entry = entry_create( "" );
    
    /* THEN */
    assert_null( entry );
}

/**
 * Test entry compare.
 */
void test_entry_compare()
{
    /* GIVEN */
    entry_t* entry = NULL;
    
    entry = entry_create( "Tom\tEnglish\tEntry meaning" );
    
    /* WHEN - THEN */
    assert_true( entry_comp( "Tom", entry ) );
    
    assert_false( entry_comp( "",   entry ) );
    assert_false( entry_comp( NULL, entry ) );
    
    entry_free( entry );
}

/**
 * Test adding results to result buffer.
 */
void test_buffer_add()
{
    /* GIVEN */
    
    assert_null( results );
    
    /* WHEN */
    buffer_add( "First result\n" );
    buffer_add( "Second result\n" );
    buffer_add( "Third result\n" );
    
    /* THEN */
    assert_int_equals( 3, result_count );
    
    assert_str_equals( "First result\n",  results[ 0 ] );
    assert_str_equals( "Second result\n", results[ 1 ] );
    assert_str_equals( "Third result\n",  results[ 2 ] );
    
    free( results[ 0 ] );
    free( results[ 1 ] );
    free( results[ 2 ] );
    
    free( results );
}

/*******************************************************************************
 * 
 * OPTIONS TESTS
 * 
 ******************************************************************************/

/**
 * Test SET OPTIONS with 0 Values.
 */
void test_set_options_none()
{
    /* GIVEN */
    cd_options = 0;
    
    /* WHEN - THEN */
    assert_false( OPTION_SET( CD_HASH_FNV   ) );
    assert_false( OPTION_SET( CD_ARRAY_BIN  ) );
    assert_false( OPTION_SET( CD_VERBOSE    ) );
    assert_false( OPTION_SET( CD_ARRAY_SORT ) );
    assert_false( OPTION_SET( CD_LIST_SORT  ) );
}

/**
 * Test set verbose flag.
 */
void test_set_options_verbose()
{
    /* GIVEN */
    cd_options = 0;
    
    /* WHEN */
    cd_options |= CD_VERBOSE;
    
    /* THEN */
    assert_true( OPTION_SET( CD_VERBOSE    ) );
}

/**
 * Test set array binary search option.
 */
void test_set_option_array_binary()
{
    /* GIVEN */
    cd_options = 0;
    
    /* WHEN */
    cd_options |= CD_ARRAY_BIN;
    
    /* THEN */
    assert_true( OPTION_SET( CD_ARRAY_BIN ) );
}

/**
 * Test set hash fnv function option.
 */
void test_set_option_hash_fnv()
{
    /* GIVEN */
    cd_options = 0;
    
    /* WHEN */
    cd_options |= CD_HASH_FNV;
    
    /* THEN */
    assert_true( OPTION_SET( CD_HASH_FNV ) );
}

/**
 * Test set array sort option.
 */
void test_set_option_array_sort()
{
    /* GIVEN */
    cd_options = 0;
    
    /* WHEN */
    cd_options |= CD_ARRAY_SORT;
    
    /* THEN */
    assert_true( OPTION_SET( CD_ARRAY_SORT ) );
}

/**
 * Test set list sort option.
 */
void test_set_option_list_sort()
{
    /* GIVEN */
    cd_options = 0;
    
    /* WHEN */
    cd_options |= CD_LIST_SORT;
    
    /* THEN */
    assert_true( OPTION_SET( CD_LIST_SORT ) );
}

/**
 * Test set NOT verbose flag.
 */
void test_set_option_not_verbose()
{
    /* GIVEN */
    cd_options = 0;
    
    /* WHEN */
    cd_options |= ~CD_VERBOSE;
    
    /* THEN */
    assert_false( OPTION_SET( CD_VERBOSE    ) );
}

/**
 * Test set array NOT binary search option.
 */
void test_set_option_not_array_binary()
{
    /* GIVEN */
    cd_options = 0;
    
    /* WHEN */
    cd_options |= ~CD_ARRAY_BIN;
    
    /* THEN */
    assert_false( OPTION_SET( CD_ARRAY_BIN ) );
}

/**
 * Test set hash NOT fnv function option.
 */
void test_set_option_not_hash_fnv()
{
    /* GIVEN */
    cd_options = 0;
    
    /* WHEN */
    cd_options |= ~CD_HASH_FNV;
    
    /* THEN */
    assert_false( OPTION_SET( CD_HASH_FNV ) );
}

/**
 * Test set array NOT sort option.
 */
void test_set_option_not_array_sort()
{
    /* GIVEN */
    cd_options = 0;
    
    /* WHEN */
    cd_options |= ~CD_ARRAY_SORT;
    
    /* THEN */
    assert_false( OPTION_SET( CD_ARRAY_SORT ) );
}

/**
 * Test set list NOT sort option.
 */
void test_set_option_not_list_sort()
{
    /* GIVEN */
    cd_options = 0;
    
    /* WHEN */
    cd_options |= ~CD_LIST_SORT;
    
    /* THEN */
    assert_false( OPTION_SET( CD_LIST_SORT ) );
}

/**
 * Test set options default.
 */
void test_set_default_options()
{
    /* GIVEN */
    cd_options = 0;
    
    /* WHEN */
    cd_options = 
              CD_ARRAY_BIN   /* use binary search */
            | CD_HASH_FNV;    /* use FNV hash function */
    
    /* THEN */
    assert_true( OPTION_SET( CD_ARRAY_BIN ) );
    assert_true( OPTION_SET( CD_HASH_FNV ) );
}

/**
 * Test setting default options with array sort.
 */
void test_set_option_binary_sort()
{
    /* GIVEN */
    cd_options = 0;
    
    /* WHEN */
    cd_options = 
              CD_ARRAY_BIN   /* use binary search */
            | CD_HASH_FNV;    /* use FNV hash function */
    
    cd_options |= CD_ARRAY_SORT;
    
    /* THEN */
    assert_true( OPTION_SET( CD_ARRAY_BIN ) );
    assert_true( OPTION_SET( CD_ARRAY_SORT ) );
}

/**
 * Test setting default options with array sort.
 */
void test_set_option_toggle_seq_search()
{
    /* GIVEN */
    cd_options = 0;
    
    /* WHEN */
    cd_options = 
              CD_ARRAY_BIN   /* use binary search */
            | CD_HASH_FNV;    /* use FNV hash function */
    
    cd_options ^= CD_ARRAY_BIN;
    
    /* THEN */
    assert_false( OPTION_SET( CD_ARRAY_BIN ) );
}

/**
 * Test setting default options with asci hash function.
 */
void test_set_option_toggle_asci_hash_func()
{
    /* GIVEN */
    cd_options = 0;
    
    /* WHEN */
    cd_options = 
              CD_ARRAY_BIN   /* use binary search */
            | CD_HASH_FNV;    /* use FNV hash function */
    
    cd_options ^= CD_HASH_FNV;
    
    /* THEN */
    assert_false( OPTION_SET( CD_HASH_FNV ) );
}