/****************************************************************************
 * COSC 2123 - Algorithms & Analysis
 * Assignment       : 1
 * Filename         : cd_array_tests.h
 * Authors          : s9863998 - Tom Kaczocha
 *                  : 
 *                  : 
 ****************************************************************************/

#ifndef CD_ARRAY_TESTS_H
#define	CD_ARRAY_TESTS_H

#include "../cd_test.h"
#include "../cd_array.h"
#include "../cd_common.h"

#ifdef	__cplusplus
extern "C" {
#endif

void test_array_create();
void test_array_insert();
void test_array_insert_null();
void test_array_insert_empty_string();

void test_array_resize();
void test_array_sort();

void test_array_locate();
void test_array_locate_non_existent();
void test_array_locate_null();
void test_array_locate_empty();

void _array_insert_entries( array_t* array );

#ifdef	__cplusplus
}
#endif

#endif	/* CD_ARRAY_TESTS_H */