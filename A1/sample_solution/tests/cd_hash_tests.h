/****************************************************************************
 * COSC 2123 - Algorithms & Analysis
 * Assignment       : 1
 * Filename         : cd_hash_tests.h
 * Authors          : s9863998 - Tom Kaczocha
 *                  : 
 *                  : 
 ****************************************************************************/

#ifndef CD_HASH_TESTS_H
#define	CD_HASH_TESTS_H

#include "../cd_test.h"
#include "../cd_hash.h"
#include "../cd_common.h"

#ifdef	__cplusplus
extern "C" {
#endif

void test_hash_create();

void test_hash_insert();
void test_hash_insert_null();
void test_hash_insert_empty_string();

void test_hash_locate();
void test_hash_locate_non_existent();
void test_hash_locate_null();
void test_hash_locate_empty();

void _hash_insert_entries( hash_t* hash );

#ifdef	__cplusplus
}
#endif

#endif	/* CD_HASH_TESTS_H */