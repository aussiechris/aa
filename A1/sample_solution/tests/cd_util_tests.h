/****************************************************************************
 * COSC 2123 - Algorithms & Analysis
 * Assignment       : 1
 * Filename         : cd_util_tests.h
 * Authors          : s9863998 - Tom Kaczocha
 *                  : 
 *                  : 
 ****************************************************************************/

#ifndef CD_UTIL_TESTS_H
#define	CD_UTIL_TESTS_H

#include "../cd_common.h"
#include "../cd_util.h"
#include "../cd_test.h"

#ifdef	__cplusplus
extern "C" {
#endif

void test_str_dup();
void test_str_create();

void test_get_power2();

#ifdef	__cplusplus
}
#endif

#endif	/* CD_UTIL_TESTS_H */