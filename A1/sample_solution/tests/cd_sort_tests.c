/****************************************************************************
 * COSC 2123 - Algorithms & Analysis
 * Assignment       : 1
 * Filename         : cd_sort_tests.c
 * Authors          : s9863998 - Tom Kaczocha
 *                  : 
 *                  : 
 ****************************************************************************/

#include "cd_sort_tests.h"

/**
 * Test bubble sort with list of ints.
 */
void test_sort_bubble_int()
{
    /* GIVEN */
    int32_t **list = NULL;
    int32_t *item = NULL;
    
    list = malloc( sizeof( int32_t* ) * 5 );
    
    item = malloc( sizeof( int32_t ) );
    *item = 7;
    list[ 0 ] = item;
    
    item = malloc( sizeof( int32_t ) );
    *item = 5;
    list[ 1 ] = item;
    
    item = malloc( sizeof( int32_t ) );
    *item = 8;
    list[ 2 ] = item;
    
    item = malloc( sizeof( int32_t ) );
    *item = 4;
    list[ 3 ] = item;
    
    item = malloc( sizeof( int32_t ) );
    *item = 2;
    list[ 4 ] = item;
    
    
    /* WHEN */
    list = ( int32_t** )sort_bubble( ( void** )list, 5, &int_check );
    
    /* THEN */
    assert_int_equals( 2, *list[ 0 ] );
    assert_int_equals( 4, *list[ 1 ] );
    assert_int_equals( 5, *list[ 2 ] );
    assert_int_equals( 7, *list[ 3 ] );
    assert_int_equals( 8, *list[ 4 ] );
}

/**
 * Test bubble sort with list of strings.
 */
void test_sort_bubble_strings()
{
    /* GIVEN */
    char** list = NULL;
    
    /* WHEN */
    list = malloc( sizeof( char* ) * 3 );
    
    assert_not_null( list );
    
    list[ 0 ] = str_dup( "Chris" );
    list[ 1 ] = str_dup( "Tom" );
    list[ 2 ] = str_dup( "Adam" );
    
    list = ( char** )sort_bubble( ( void** )list, 3, &str_check );
    
    /* THEN */
    assert_str_equals( "Adam",  list[ 0 ] );
    assert_str_equals( "Chris", list[ 1 ] );
    assert_str_equals( "Tom",   list[ 2 ] );
}


/*******************************************************************************
 * 
 * TEST HELPERS
 * 
 ******************************************************************************/

/**
 * Compares two integers and swaps their positions if needed.
 * 
 * @param void* first  first integer
 * @param void* second second integer
 */
void int_check( void* first, void* second )
{
    int32_t i = 0;
    int32_t j = 0;
    int32_t t = 0;
    
    i = *( int* )first;
    j = *( int* )second;
    
    if ( i < j )
    {
        t               = i;
        *( int* )first  = j;
        *( int* )second = t;
    }
}

/**
 * Compares two strings and swaps their positions if needed.
 * 
 * @param void* first  first string
 * @param void* second second string
 */
void str_check( void* first, void* second )
{
    char* tmp = NULL;
    
    if ( strcmp( ( char* )first, ( char* )second ) < 0 )
    {
        tmp = str_dup( first  );
        
        first = realloc( first, strlen( second ) + 1 );
        strcpy( first, second );
        
        second = realloc( second, strlen( tmp ) + 1 );
        strcpy( second, tmp );
    }
}