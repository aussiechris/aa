/****************************************************************************
 * COSC 2123 - Algorithms & Analysis
 * Assignment       : 1
 * Filename         : cd_hash.h
 * Authors          : s9863998 - Tom Kaczocha
 *                  : 
 *                  : 
 ****************************************************************************/

#ifndef CD_HASH_H
#define	CD_HASH_H

#ifdef	__cplusplus
extern "C" {
#endif

#define MAX_LOAD_FACTOR 0.75

#include "cd_common.h"
#include "cd_util.h"

#define STATS_FILE "stats.txt"
    
typedef struct hash
{
    uint32_t  size;
    uint32_t  count;
    entry_t** table;
    uint64_t  tsize;
} hash_t;

hash_t*   hash_create   ( uint32_t       );
uint32_t  hash_calc_asc ( hash_t*, char* );
uint32_t  hash_calc_fnv ( hash_t*, char* );
BOOL      hash_insert   ( hash_t*, char* );
entry_t*  hash_locate   ( hash_t*, char* );
void      hash_display  ( hash_t*        );
void      hash_remove   ( hash_t*, char* );
void      hash_free     ( hash_t*        );

void      hash_get_clustering   ( hash_t*, int32_t*, int32_t* );
int32_t** hash_get_distribution ( hash_t*                     );
void      hash_free_distribution( hash_t*, int32_t**          );
void      write_distribution    ( hash_t*, int32_t**          );

#ifdef	__cplusplus
}
#endif

#endif	/* CD_HASH_H */