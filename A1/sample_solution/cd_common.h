/****************************************************************************
 * COSC 2123 - Algorithms & Analysis
 * Assignment       : 1
 * Filename         : cd_common.h
 * Authors          : s9863998 - Tom Kaczocha
 *                  : 
 *                  : 
 ****************************************************************************/

#ifndef CD_COMMON_H
#define	CD_COMMON_H

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdarg.h>

#include <string.h>
#include <unistd.h>
#include <sys/time.h>

#include "cd_time.h"

#ifdef	__cplusplus
extern "C" {
#endif

#ifndef boolean

enum boolean { FALSE = 0, TRUE = 1 };
typedef enum boolean BOOL;

#endif

extern int32_t  cd_options;
extern struct timeval* sort_times[ 3 ][ 2 ];
extern char**   results;
extern uint32_t result_count;
extern uint32_t buffer_size;

/* flags */
#define CD_HASH_FNV   0x000001 /* 1  - On = Use FNV func, Off = Use Asci func */
#define CD_ARRAY_BIN  0x000002 /* 2  - On = Binary Search, Off = Sequential Search */
#define CD_VERBOSE    0x000004 /* 4  - On = Display to stdout, Off = Only write to file */
#define CD_ARRAY_SORT 0x000008 /* 8  - On = Sorted array, Off = Array not sorted */
#define CD_LIST_SORT  0x000010 /* 16 - On = Sorted list, Off = List not sorted */
#define CD_HASH_SORT  0x000020 /* 32 - On = Sorted hash, Off = Hash not sorted */
#define CD_SORT_TIME  0x000040 /* 64 - On = Get Sort Times */

/* macro for checking if a flag has been set */
#ifndef OPTION_SET
#define OPTION_SET( option ) ( ( ( cd_options & option ) == option ) ? ( TRUE ) : ( FALSE ) )
#endif

#define PRINT_FOUND     "%15s - %10s - %s\n"
#define PRINT_NOT_FOUND "%15s              - No match found\n"
#define NOT_FOUND       -1
#define INITIAL_SIZE    8
#define MEGABITE        1024

typedef struct entry
{
    char*         name;
    char*         origin;
    char*         meaning;
    struct entry* next;
    uint32_t      size;
} entry_t;

entry_t* entry_create( char*           );
void     entry_free  ( entry_t*        );
BOOL     entry_comp  ( char*, entry_t* );
void     buffer_add  ( char*           );
void     save_results(                 );
void     entry_check ( void* , void*   );

#ifdef	__cplusplus
}
#endif

#endif	/* CD_COMMON_H */