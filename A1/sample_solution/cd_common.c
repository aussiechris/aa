/****************************************************************************
 * COSC 2123 - Algorithms & Analysis
 * Assignment       : 1
 * Filename         : cd_common.c
 * Authors          : s9863998 - Tom Kaczocha
 *                  : 
 *                  : 
 ****************************************************************************/

#include "cd_common.h"
#include "cd_util.h"

char**   results;
uint32_t result_count;
uint32_t buffer_size;

/**
 * Creates a new entry instance.
 * 
 * @param char* input the input string
 * 
 * @return entry_t* entry pointer
 */
entry_t* entry_create( char* input )
{
    entry_t* new  = NULL;
    char*    data = NULL;
    char*    str;
    
    if ( NULL == input || ( 0 == strlen( input ) ) )
    {
        return NULL;
    }
    
    data = str_dup( input );
    
    if ( '\n' == data[ strlen( data ) - 1 ] )
    {
        data[ strlen( data ) - 1] = '\0';
    }
    
    new = malloc( sizeof( entry_t ) );
    
    if ( NULL == new )
    {
        fprintf( stderr, "Failed to allocate new array entry.\n" );
        
        exit( EXIT_FAILURE );
    }
    
    new->next = NULL;
    new->size = sizeof( entry_t );
    
    str = strtok( data, "\t" );
    new->name = str_dup( str );
    new->size += sizeof( char ) * ( strlen( str ) + 1 );
    
    str = strtok( NULL, "\t" );
    new->origin = str_dup( str );
    new->size += sizeof( char ) * ( strlen( str ) + 1 );
    
    str = strtok( NULL, "\t" );
    new->meaning = str_dup( str );
    new->size += sizeof( char ) * ( strlen( str ) + 1 );
    
    free( data );
    
    return new;
}

/**
 * Deallocates memory for an entry.
 * 
 * @param entry_t* entry the entry to free
 */
void entry_free( entry_t* entry )
{
    free( entry->name );
    free( entry->origin );
    free( entry->meaning );
    free( entry );
}

/**
 * Checks if a key and entry match.
 * 
 * @param char*    name  the key we're looking for
 * @param entry_t* entry the entry to check
 * 
 * @return BOOL TRUE if matches, else FALSE
 */
BOOL entry_comp( char* name, entry_t* entry )
{
    if ( NULL == name || ( 0 == strlen( name ) ) || NULL == entry )
    {
        return FALSE;
    }
    
    if ( 0 == strcmp( name, entry->name ) )
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

void entry_check( void* entry1, void* entry2 )
{
    entry_t tmp;
    entry_t entry;
    
    if ( strcmp( ( ( entry_t* )entry1 )->name, ( ( entry_t* )entry2 )->name ) < 0 )
    {
        entry = *( entry_t* )entry1;
        
        tmp = entry;
        
        entry = *( entry_t* )entry2;
        
        *( entry_t* )entry1 = entry;
        
        entry = tmp;
        
        *( entry_t* )entry2 = entry;
    }
}

/**
 * Adds a result to a buffer if VERBOSE option is set.
 * 
 * @param char* result the result string
 */
void buffer_add( char* result )
{
    if ( TRUE == OPTION_SET( CD_VERBOSE ) )
    {
        printf( "%s", result );
    }
    
    if ( NULL == results )
    {
        buffer_size = INITIAL_SIZE;
        results = malloc( sizeof( char* ) * INITIAL_SIZE );
        result_count = 0;
    }

    results[ result_count ] = str_dup( result );

    result_count++;

    if ( result_count == buffer_size )
    {
        char** tmp = NULL;

        buffer_size *= 2;

        tmp = realloc( results, sizeof( char* ) * buffer_size );

        if ( NULL == tmp )
        {
            printf( "Could not reallocate result buffer.\n" );

            exit( EXIT_FAILURE );
        }

        results = tmp;
    }
}

/**
 * Writes results to file.
 */
void save_results()
{
    uint32_t i = 0;
    FILE* file;
    char* results_file = "data/results.txt";
    
    if ( NULL == results ) { return; }
    
    file = fopen( results_file, "w" );
    
    if ( NULL == file )
    {
        printf( "Failed to open results file.\n" );
                
        exit( EXIT_FAILURE );
    }
    
    while ( i < result_count )
    {
        if ( NULL == results || NULL == results[ i ] )
        {
            break;
        }
        
        fputs(results[ i ], file );
        
        free( results[ i ] );
        
        i++;
    }
    
    fclose( file );
    
    free( results );
}