/****************************************************************************
 * COSC 2123 - Algorithms & Analysis
 * Assignment       : 1
 * Filename         : cd_math.c
 * Authors          : s9863998 - Tom Kaczocha
 *                  : 
 *                  : 
 ****************************************************************************/

#include <math.h>

#include "cd_common.h"
#include "cd_array.h"
#include "cd_time.h"

extern times_t* times_sort_array;
extern times_t* times_sort_list;
extern times_t* times_sort_hash;

/**
 * Create a new array for entries.
 * 
 * @param uint32_t size array size
 * 
 * @return new array
 */
array_t* array_create ( uint32_t size )
{
    array_t* array = NULL;
    
    array = malloc( sizeof( array_t ) );
    
    if ( NULL == array )
    {
        fprintf( stderr, "Failed to create new array.\n" );
        
        exit( EXIT_FAILURE );
    }
    
    array->count  = 0;
    array->size   = size;
    array->items  = calloc( 1, sizeof( entry_t* ) * size );
    
    array->tsize  = sizeof( array_t );
    array->tsize += sizeof( entry_t* ) * size;
    
    if ( NULL == array->items )
    {
        fprintf( stderr, "Failed to allocate items array.\n" );
        
        exit( EXIT_FAILURE );
    }
    
    return array;
}

/**
 * Resizes the items list in the array.
 * 
 * @param array_t* array the array
 * @param uint32_t size  new size
 * 
 * @return BOOL TRUE on success, FALSE on error
 */
BOOL array_resize ( array_t* array, uint32_t size )
{
    entry_t** tmp = NULL;
    
    tmp = realloc( array->items, ( sizeof( entry_t* ) * size ) );
    
    if ( NULL == tmp )
    {
        fprintf( stderr, "Failed to reallocate items array.\n" );
        
        return FALSE;
    }
    
    array->tsize -= sizeof( entry_t* ) * array->size;
    array->tsize += sizeof( entry_t* ) * size;
    
    array->items = tmp;
    array->size = size;
    
    return TRUE;
}

/**
 * Insert an entry into the array sorted.
 * 
 * @param array_t* array the array
 * @param char*    input entry input
 * 
 * @return BOOL TRUE on success, FALSE on error
 */
BOOL array_insert ( array_t* array, char* input )
{
    entry_t* new = NULL;
    
    if ( NULL == input || ( 0 == strlen( input ) ) )
    {
        return FALSE;
    }
    
    /* resize items array if not enough space */
    if ( ( array->count + 1 ) == array->size )
    {
        if ( FALSE == array_resize( array, array->size * 2 ) )
        {
            return FALSE;
        }
    }
    
    new = entry_create( input );
    
    array->items[ array->count ] = new;
    array->tsize += new->size;
    array->count++;
    
    if ( TRUE == OPTION_SET( CD_ARRAY_SORT ) )
    {
        if ( TRUE == OPTION_SET( CD_SORT_TIME ) )
        {
            time_add_time( times_sort_array, time_gettime() );
        }
        
        array = array_sort( array );
        
        if ( TRUE == OPTION_SET( CD_SORT_TIME ) )
        {
            time_add_time( times_sort_array, time_gettime() );
        }
    }
    
    return TRUE;
}

/**
 * Returns a sorted array.
 * 
 * Uses bubble sort to sort entries.
 * 
 * @param array_t* array the array to sort
 * 
 * @return array_t* sorted array
 */
array_t* array_sort( array_t* list )
{
    int32_t i = 1;
    int32_t j = 0;
    entry_t* current; /* v */
    
    if ( 1 == list->count )
    {
        return list;
    }
    
    for ( i = 1; i < list->count; i++ )
    {
        current = list->items[ i ];
        
        j = i - 1;
     
        while ( j >= 0 && strcmp( list->items[ j ]->name, current->name ) > 0 )
        {
            list->items[ j + 1 ] = list->items[ j ];
            
            j--;
        }
        
        list->items[ j + 1 ] = current;
    }
    
    return list;
}

/**
 * Tries to find the correct entry by 'name'.
 * 
 * Uses sequential search.
 * 
 * @param array_t* array the array
 * @param char*    name  the name looked for
 * 
 * @return entry_t* the entry if found, or NULL
 */
int32_t array_locate_sequential( array_t* array, char* name )
{
    int32_t i = 0;
    
    if ( NULL == name || ( 0 == strlen( name ) ) )
    {
        return -1;
    }
    
    while ( i < array->count )
    {
        if ( TRUE == entry_comp( name, array->items[ i ] ) )
        {
            return i;
        }
        
        i++;
    }
    
    return -1;
}

/**
 * Tries to find the correct entry by 'name'.
 * 
 * Uses binary search.
 * 
 * @param array_t* array the array
 * @param char*    name  the name looked for
 * 
 * @return entry_t* the entry if found, or NULL
 */
int32_t array_locate_binary( array_t* array, char* name )
{
    int32_t start  = 0; /* l */
    int32_t end  = 0;   /* r */
    int32_t middle = 0; /* m */
    
    end = array->count - 1;
    
    if ( NULL == name || ( 0 == strlen( name ) ) )
    {
        return -1;
    }
    
    while ( start <= end )
    {
        middle = ( int ) floor( ( float ) ( ( end - start ) / 2 ) + start );
        
        if ( TRUE == entry_comp( name, array->items[ middle ] ) )
        {
            return middle;
        }
        else if ( strcmp( name, array->items[ middle ]->name ) < 0 )
        {
            end = middle - 1;
        }
        else
        {
            start = middle + 1;
        }
    }
    
    return -1;
}

/**
 * Deallocates array memory.
 * 
 * @param array_t* array the array
 */
void array_free( array_t* array )
{
    int32_t i = 0;
    
    while ( i < array->count )
    {
        if ( NULL != array->items[ i ] )
        {
            entry_free( array->items[ i ] );
        }
        
        i++;
    }
    
    free( array->items );
    free( array );
}