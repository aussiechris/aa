/****************************************************************************
 * COSC 2123 - Algorithms & Analysis
 * Assignment       : 1
 * Filename         : cd_sort.h
 * Authors          : s9863998 - Tom Kaczocha
 *                  : 
 *                  : 
 ****************************************************************************/

#ifndef CD_SORT_H
#define	CD_SORT_H

#include "cd_common.h"

#ifdef	__cplusplus
extern "C" {
#endif

void** sort_bubble   ( void**, int32_t, void ( *comp )( void*, void* ) );
void** sort_insertion( void**, int32_t, void ( *comp )( void*, void* ) );

#ifdef	__cplusplus
}
#endif

#endif	/* CD_SORT_H */