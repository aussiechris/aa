/****************************************************************************
 * COSC 2123 - Algorithms & Analysis
 * Assignment       : 1
 * Filename         : cd_list.h
 * Authors          : s9863998 - Tom Kaczocha
 *                  : 
 *                  : 
 ****************************************************************************/

#ifndef CD_LIST_H
#define	CD_LIST_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "cd_common.h"

typedef struct list
{
    uint32_t count;
    entry_t* head;
    entry_t* tail;
    uint64_t tsize;
} list_t;

list_t*  list_create (                );
BOOL     list_insert ( list_t*, char* );
entry_t* list_locate ( list_t*, char* );
void     list_display( list_t*        );
void     list_remove ( list_t*, char* );
void     list_free   ( list_t*        );

#ifdef	__cplusplus
}
#endif

#endif	/* CD_LIST_H */