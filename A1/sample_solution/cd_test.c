/****************************************************************************
 * COSC 2123 - Algorithms & Analysis
 * Assignment       : 1
 * Filename         : cd_test.c
 * Authors          : s9863998 - Tom Kaczocha
 *                  : 
 *                  : 
 ****************************************************************************/

#include <execinfo.h>

#include "cd_test.h"
#include "cd_time.h"
#include "cd_util.h"
#include "cd_sort.h"

test_t** tests;
test_t*  current_test;

static int test_count;
static int current_size;

/**
 * Initialises the test run.
 */
void test_runner_init()
{
    current_size = 8;
    
    tests = malloc( sizeof( test_t* ) * current_size );
    
    if ( NULL == tests )
    {
        printf( "%s: failed to allocate memory for tests.\n", __FUNCTION__ );
        
        exit( EXIT_FAILURE );
    }
    
    test_count = 0;
}

/**
 * Creates a new test.
 * 
 * @param test_func test_f test function pointer
 * 
 * @return test_t* new test pointer
 */
test_t* test_create( char* group, char* name, test_func_ptr test_f )
{
    test_t* test = NULL;
    
    test = malloc( sizeof( test_t ) );
    
    if ( NULL == test )
    {
        printf( "Failed to create test.\n" );
        
        exit( EXIT_FAILURE );
    }
    
    test->test       = test_f;
    test->status     = -1;
    test->start_time = 0;
    test->end_time   = 0;
    test->test_group = str_dup( group );
    test->test_name  = str_dup( name );
    
    return test;
}

/**
 * Adds a test to the test run.
 * 
 * @param test_func_ptr test function pointer
 */
void test_add( char* group, char* name, test_func_ptr test )
{
    tests[ test_count ] = test_create( group, name, test );
    
    test_count++;
    
    if ( test_count == current_size )
    {
        void* tmp;
        
        current_size *= 2;
        
        tmp = realloc( tests, sizeof( test_t* ) * current_size );
        
        if ( NULL == tmp )
        {
            exit( EXIT_FAILURE );
        }
        
        tests = tmp;
    }
}

/**
 * Runs all available tests.
 */
void test_run()
{
    uint32_t i = 0;
    
    while ( i < test_count )
    {
        current_test = tests[ i ];
        current_test->start_time = time_gettime_usec();
        current_test->test();
        current_test->end_time = time_gettime_usec();
        
        i++;
    }
}

/**
 * Displays the test results.
 */
void test_results()
{
    test_t* test    = NULL;
    uint32_t i      = 0;
    uint64_t start  = 0;
    uint64_t end    = 0;
    uint32_t passed = 0;
    uint32_t failed = 0;
    char*    group  = NULL;
    
    /* Sort Tests by Group */
    tests = ( test_t** )sort_bubble( ( void** )tests, test_count, &test_group_check );

    while ( i < test_count )
    {
        test = tests[ i ];
        
        if ( TRUE == test->status )
        {
            passed++;
        }
        else
        {
            failed++;
        }
        
        if ( 0 == start || test->start_time < start )
        {
            start = test->start_time;
        }
        
        if ( 0 == end || test->end_time > end )
        {
            end = test->end_time;
        }
        
        if ( NULL == group || ( 0 != strcmp( group, test->test_group ) ) )
        {
            group = test->test_group;
            
            printf( "\n%s\n---------------------------------------------\n", test->test_group );
        }
        
        printf( "%-60s [%s]\n", test->test_name, ( TRUE == test->status ) ? "OK" : "FAILED" );
        
        i++;
    }
    
    printf( "\n\n---------------------------------------------\n\n");
    printf( "Test Duration: %f\n\n", time_get_diff( start, end ) / 1000000.0 );
    printf( "All Tests: %d    Passed: %d    Failed: %d\n", ( passed + failed ), passed, failed );
    printf( "---------------------------------------------\n\n");
}

void test_group_check( void* test1, void* test2 )
{
    test_t tmp;
    test_t test;
    
    if ( strcmp( ( ( test_t* )test1 )->test_group, ( ( test_t* )test2 )->test_group ) < 0 )
    {
        test = *( test_t* )test1;
        
        tmp = test;
        
        test = *( test_t* )test2;
        
        *( test_t* )test1 = test;
        
        test = tmp;
        
        *( test_t* )test2 = test;
    }
}

/*******************************************************************************
 * 
 * ASSERTS - Strings
 * 
 ******************************************************************************/

/**
 * Asserts that two strings equal.
 * 
 * @param char* str1 the first string
 * @param char* str2 the second string
 */
void assert_str_equals( char* str1, char* str2 )
{
    if ( FALSE == current_test->status ) { return; }
    
    if ( 0 != strcmp( str1, str2 ) )
    {
        current_test->status = FALSE;
        
        return;
    }
    
    current_test->status = TRUE;
}

/**
 * Asserts that a string is not NULL.
 * 
 * @param char* str string to check
 */
void assert_str_not_null( char* str )
{
    if ( FALSE == current_test->status ) { return; }
    
    if ( NULL == str )
    {
        current_test->status = FALSE;
        
        return;
    }
    
    current_test->status = TRUE;
}

/**
 * Asserts that a string length equals 'size'.
 * 
 * @param char*   str  the string to check
 * @param int32_t size the size
 */
void assert_str_len_equals( char* str, int32_t size )
{
    if ( FALSE == current_test->status ) { return; }
    
    if ( strlen( str ) != size )
    {
        current_test->status = FALSE;
        
        return;
    }
    
    current_test->status = TRUE;
}

/**
 * Asserts that a string length is greater then 'size'.
 * 
 * @param char*   str  string to check
 * @param int32_t size the size
 */
void assert_str_len_greater_then( char* str, int32_t size )
{
    if ( FALSE == current_test->status ) { return; }
    
    if ( strlen( str ) <= size )
    {
        current_test->status = FALSE;
        
        return;
    }
    
    current_test->status = TRUE;
}


/*******************************************************************************
 * 
 * VOIDs
 * 
 ******************************************************************************/

/**
 * Asserts that item is not null.
 * 
 * @param void* item the item to check
 */
void assert_not_null( void* item )
{
    if ( FALSE == current_test->status ) { return; }
    
    if ( NULL == item )
    {   
        current_test->status = FALSE;
        
        return;
    }
    
    current_test->status = TRUE;
}

/**
 * Asserts that item is NULL.
 * 
 * @param void* item item to test
 */
void assert_null( void* item )
{
    if ( FALSE == current_test->status ) { return; }
    
    if ( NULL != item )
    {
        current_test->status = FALSE;
        
        return;
    }
    
    current_test->status = TRUE;
}


/*******************************************************************************
 * 
 * INTs
 * 
 ******************************************************************************/

/**
 * Asserts that two ints are equal.
 * 
 * @param int32_t i first int to check
 * @param int32_t j second int to check
 */
void assert_int_equals( int32_t i, int32_t j )
{
    if ( FALSE == current_test->status ) { return; }
    
    if ( i != j )
    {
        current_test->status = FALSE;
        
        return;
    }
    
    current_test->status = TRUE;
}

/**
 * Asserts that two ints are NOT equal.
 * 
 * @param int32_t i first int to check
 * @param int32_t j second int to check
 */
void assert_int_not_equals( int32_t i, int32_t j )
{
    if ( FALSE == current_test->status ) { return; }
    
    if ( i == j )
    {
        current_test->status = FALSE;
        
        return;
    }
    
    current_test->status = TRUE;
}

/*******************************************************************************
 * 
 * LONGs
 * 
 ******************************************************************************/

/**
 * Asserts that two ints are equal.
 * 
 * @param int64_t i first int to check
 * @param int64_t j second int to check
 */
void assert_long_equals( int64_t i, int64_t j )
{
    if ( FALSE == current_test->status ) { return; }
    
    if ( i != j )
    {
        current_test->status = FALSE;
        
        return;
    }
    
    current_test->status = TRUE;
}

/**
 * Asserts that two longs are NOT equal.
 * 
 * @param int64_t i first long to check
 * @param int64_t j second long to check
 */
void assert_long_not_equals( int64_t i, int64_t j )
{
    if ( FALSE == current_test->status ) { return; }
    
    if ( i == j )
    {
        current_test->status = FALSE;
        
        return;
    }
    
    current_test->status = TRUE;
}

/**
 * Asserts that 'greater' is greater then 'then'.
 * 
 * @param int32_t greater the greater then int
 * @param int32_t then    the assert variable
 */
void assert_int_greater_then( int32_t greater, int32_t then )
{
    if ( FALSE == current_test->status ) { return; }
    
    if ( greater <= then )
    {
        current_test->status = FALSE;
        
        return;
    }
    
    current_test->status = TRUE;
}

/**
 * Asserts that 'greater' is greater then 'then'.
 * 
 * @param int64_t greater the greater then long
 * @param int64_t then    the assert variable
 */
void assert_long_greater_then( int64_t greater, int64_t then )
{
    if ( FALSE == current_test->status ) { return; }
    
    if ( greater <= then )
    {
        current_test->status = FALSE;
        
        return;
    }
    
    current_test->status = TRUE;
}

/**
 * Asserts that status is TRUE.
 * 
 * @param BOOL status the status to test
 */
void assert_true( BOOL status )
{
    if ( FALSE == current_test->status ) { return; }
    
    if ( FALSE == status )
    {
        current_test->status = FALSE;
        
        return;
    }
    
    current_test->status = TRUE;
}


/**
 * Asserts that status is FALSE.
 * 
 * @param BOOL status the status to test
 */
void assert_false( BOOL status )
{
    if ( FALSE == current_test->status ) { return; }
    
    if ( TRUE == status )
    {
        current_test->status = FALSE;
        
        return;
    }
    
    current_test->status = TRUE;
}