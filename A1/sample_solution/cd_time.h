/****************************************************************************
 * COSC 2123 - Algorithms & Analysis
 * Assignment       : 1
 * Filename         : cd_time.h
 * Authors          : s9863998 - Tom Kaczocha
 *                  : 
 *                  : 
 ****************************************************************************/

#ifndef CD_TIME_H
#define	CD_TIME_H

#include "cd_common.h"

#ifdef	__cplusplus
extern "C" {
#endif

#ifndef MILLION
#define MILLION 1000000.0
#endif

typedef struct
{
    int32_t count;
    int32_t max;
    struct timeval** times;
} times_t;

struct  timeval* time_gettime();
int64_t time_gettime_usec();
float   time_gettime_sec();

float   time_time_to_sec( struct timeval* );
int64_t time_time_to_usec( struct timeval* );

int64_t time_get_diff( int64_t, int64_t );
float   time_get_diff_sec( int64_t, int64_t );


times_t* time_init_list();
times_t* time_add_time( times_t*, struct timeval* );
int64_t  time_get_list_total( times_t* );
void     time_free_list( times_t* );

#ifdef	__cplusplus
}
#endif

#endif	/* CD_TIME_H */