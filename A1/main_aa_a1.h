/***************************************************************************
* COSC1100 - Algorithms and Analysis
* Summer Semester 2014 Assignment #1 - Data Structure Performance
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1100
* Program Code     : BP094
***************************************************************************/
#ifndef MAIN_AA_A1_H
#define MAIN_AA_A1_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <sys/time.h>

/* specifies the file names to load */
#define NAMES_FILE "names.txt"
#define SEARCH_FILE "search.txt"

/* Specifies the input buffer for reading files */
#define INPUT_BUFFER 513
#define MAX_ITEM 500

/* typedef for boolean type */
/* supplied as example code by Paul */
/* from PT last semester */
typedef enum truefalse
{
    FALSE, TRUE
} BOOLEAN;

/* typedef for search list */
typedef struct searchlist_t
{
  struct searchlist_t *next;
  char* name;
} searchlist_t;

/* typedef for database structure */
typedef struct
{
  struct list_t *linkedlist;
  struct arrayhead_t *sortedarray;
  struct hashhead_t *hashtable;
  struct searchlist_t *searchlist;
} database_t;

#include "utility.h"
#include "linked_list.h"
#include "sorted_array.h"
#include "hash_table.h"
#endif