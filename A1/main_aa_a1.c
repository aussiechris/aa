/***************************************************************************
* COSC1100 - Algorithms and Analysis
* Summer Semester 2014 Assignment #1 - Data Structure Performance
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1100
* Program Code     : BP094
***************************************************************************/

#include "main_aa_a1.h"

int main()
{
    database_t db;
        
    /* load names */
    if(!load_names_file(NAMES_FILE, &db))
    {
        printf("Oh Noes! There was an error loading names data!\nExiting...\n");
        return EXIT_FAILURE;
    }
    
    /* load search */
    if(!load_search_file(SEARCH_FILE, &db))
    {
        printf("Oh Noes! There was an error loading search data!\nExiting...\n");
        return EXIT_FAILURE;
    }
    
    /* complete search */
    search_names(&db);
    
    /* output data summary */
    printf ("\nLinked list uses %i Bytes of memory\n",db.linkedlist->memsize);
    printf ("Linked list has %i entries\n",db.linkedlist->count);
    if (db.linkedlist->count>0)
        printf ("Linked list uses an average of %i Bytes per entry\n",db.linkedlist->memsize/db.linkedlist->count);
    printf("Linked list sorted using Bubble Sort.\n");
    printf("The sort took %.5f sec and %i passes\n", db.linkedlist->sort_time,db.linkedlist->sort_count);
    printf("Search of an unsorted linked list took %.5f sec\n", db.linkedlist->unsorted_search);
    printf("Search of a sorted linked list took %.5f sec\n", db.linkedlist->sorted_search);
    
    printf ("\nArray uses %i Bytes of memory\n",db.sortedarray->memsize);
    printf ("Array has %i entries\n",db.sortedarray->count);
    if (db.sortedarray->count>0)
        printf ("Array uses an average of %i Bytes per entry\n",db.sortedarray->memsize/db.sortedarray->count);
    printf("Array sorted using Comb Sort.\n");
    printf("The sort took %.5f sec and %i passes\n", db.sortedarray->sort_time,db.sortedarray->sort_count);
    printf("Search of an unsorted array took %.5f sec\n", db.sortedarray->unsorted_search);
    printf("Search of a sorted array took %.5f sec\n", db.sortedarray->sorted_search);
    
    printf ("\nHash table uses %i Bytes of memory\n",db.hashtable->memsize);
    printf ("Hash table has %i entries in a table of %i slots\n",db.hashtable->count,HASHTABLESIZE);
    if (db.hashtable->count>0)
        printf ("Hash table uses an average of %i Bytes per entry\n",db.hashtable->memsize/db.hashtable->count);
    hash_stats(&db);
    printf("Hash table assumed pre-sorted input.\n");
    printf("The sort took 0 sec and 0 passes\n");
    printf("Search of an unsorted hash table took %.5f sec\n", db.hashtable->unsorted_search);
    printf("Search of a sorted hash table took %.5f sec\n", db.hashtable->sorted_search);
    
    /* free memory */
    if(!free_db(&db))
    {
        printf("Oh Noes! There was an error clearing memory!\nExiting...\n");
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;
}
