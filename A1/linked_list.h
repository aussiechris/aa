/***************************************************************************
* COSC1100 - Algorithms and Analysis
* Summer Semester 2014 Assignment #1 - Data Structure Performance
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1100
* Program Code     : BP094
***************************************************************************/
#ifndef LLIST_H
#define LLIST_H

#include "main_aa_a1.h"

typedef struct node_t
{
    struct node_t *next;
    char* name;
    char* origin;
    char* meaning;
} node_t;

typedef struct list_t
{
    int count;
    int memsize;
    double unsorted_search;
    double sorted_search;
    double sort_time;
    int sort_count;
    struct node_t *head;
} list_t;

BOOLEAN initialize_list(database_t * db);
BOOLEAN load_list(database_t * db, char* line);
void free_list(database_t * db);
void bubble_sort_list (database_t * db);
void comb_sort_list (database_t * db);
void search_list(database_t * db, char* name);
void search_sorted_list(database_t * db, char* name);


#endif