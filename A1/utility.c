/***************************************************************************
* COSC1100 - Algorithms and Analysis
* Summer Semester 2014 Assignment #1 - Data Structure Performance
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1100
* Program Code     : BP094
***************************************************************************/

#include "utility.h"

unsigned long gettime()
{
    /*****************************************************
    **gettime()
    * Get the time used for timing searches
    * 
    */
    struct timeval tp;
    gettimeofday(&tp,NULL);
    return (tp.tv_sec*1E6) + tp.tv_usec;
}

BOOLEAN load_names_file(char * namesfile, database_t * db)
{
    /*****************************************************
    **load_names_file()
    * Load a names from the supplied namesfile and insert 
    * them into the database db.
    * 
    */
    
    int names = 0;
    char line[INPUT_BUFFER+1];  
    struct node_t * this;
    FILE *fp;
    
    /* Open names file and return any errors */
    if (!(fp = fopen(namesfile, "r")))
    {
        printf ("Error opening names file %s\n",namesfile);
        return FALSE;
    }
    printf ("Successfully opened %s\n",namesfile);
    
    /* Initialise linked list */
    if(!initialize_list(db))
    {
        printf("Linked list could not be initialized\n");
        return FALSE;
    }
    
    /* Insert data from namesfile into linked list */
    while (fgets(line, INPUT_BUFFER, fp))
    {
        if(line[0])
        {
            if(!load_list(db, line))
            {
                printf ("Error adding a node to the linked list.\n");
                return FALSE;
            }
            names++;
        }            
    }
    fclose(fp);
    
    /* Initialise array and hash table data structures */
    if(!initialize_array(db))
    {
        printf("Sorted array could not be initialized\n");
        return FALSE;
    }
    if(!initialize_hash(db))
    {
        printf("Hash table could not be initialized\n");
        return FALSE;
    }
    
    /* Copy each name from the unsorted list to the array */  
    this = db->linkedlist->head;
    while(this)
    {
        insert_array(db,this->name,this->origin,this->meaning);
        this = this->next;
    }
    
    /* Sort and compare the data */
    bubble_sort_list(db);
    comb_sort_array(db);
    
    /* Copy each name from the sorted list to the hash table */  
    this = db->linkedlist->head;
    while(this)
    {
        insert_hash(db,this->name,this->origin,this->meaning);
        this = this->next;
    }
    
    printf ("Loaded %i names OK!\n",names);
    return TRUE;
}

BOOLEAN free_db(database_t * db)
{
    /*****************************************************
    **free_db()
    * Free memory for each data structure in the database
    * 
    */
    
    free_list(db);
    free_array(db);
    free_hash(db);
    free_search(db);
    
    return TRUE;
}

BOOLEAN load_search_file(char * searchfile, database_t * db)
{
    /*****************************************************
    **load_search_file()
    * Load a search from the supplied searchfile and insert 
    * them into the database db.
    * 
    */
    
    int names = 0;
    char line[INPUT_BUFFER+1], *item=NULL;
    struct searchlist_t *new;
    size_t itemsize;
    FILE *fp;
    
    /* Open search file and return any errors */
    if (!(fp = fopen(searchfile, "r")))
    {
        printf ("\nError opening search file %s\n",searchfile);
        return FALSE;
    }
    printf ("\nSuccessfully opened %s\n",searchfile);
    
    /* initialise search list */
            db->searchlist=NULL;
    
    /* Insert data from searchfile into search list */
    while (fgets(line, INPUT_BUFFER, fp))
    {
        /* allocate memory for new name */
        if(!(new = malloc(sizeof(struct node_t))))
            return FALSE;
        
        /* insert the new search item at the start of the search list */
        new->next=db->searchlist;
        db->searchlist=new;
        
        /* parse each line and insert it into the search list */
        item = strtok (line,"\n\r");
        itemsize = ((strlen(item)+1)*sizeof(char));
        if(!(new->name = malloc(itemsize)))
            return FALSE;
        strcpy(new->name,item);
        
        /* increment names counter */
        names++;
        
    }
    fclose(fp);
    
    printf ("Loaded %i search names OK!\n",names);
    return TRUE;
}

void free_search(database_t * db)
{
    /*****************************************************
    **free_search()
    * Free all memory used for the search list
    * 
    */
    
    struct searchlist_t * next;
    struct searchlist_t * this;
    
    /* set first node */
    this = db->searchlist;
    
    /* free each node and it's data */
    while(this)
    {
        next = this->next;
        free(this->name);
        free(this);
        this = next;
    }
}

void search_names(database_t * db)
{
    /*****************************************************
    **search_names()
    * Search all data structures for each name in the search list
    * 
    */
    
    struct searchlist_t * next;
    struct searchlist_t * this;
    unsigned long starttime, endtime;
    
    /* set first node */
    this = db->searchlist;
    
    /* search linked list for each node in the search list */
    starttime=gettime();
    while(this)
    {
        next = this->next;
        search_list(db, this->name);
        this = next;
    }
    endtime=gettime();
    db->linkedlist->unsorted_search=(double)(endtime-starttime)/1E6;
    printf("Search of the unsorted linked list took %.2f sec\n", db->linkedlist->unsorted_search);
    
    /* set first node again*/
    this = db->searchlist;
    
    /* search sorted linked list for each node in the search list */
    starttime=gettime();
    while(this)
    {
        next = this->next;
        search_sorted_list(db, this->name);
        this = next;
    }
    endtime=gettime();
    db->linkedlist->sorted_search=(double)(endtime-starttime)/1E6;
    printf("Search of the sorted linked list took %.2f sec\n", db->linkedlist->sorted_search);
    
    /* set first node again*/
    this = db->searchlist;
    
    /* search unsorted array for each node in the search list */
    starttime=gettime();
    while(this)
    {
        next = this->next;
        search_array(db, this->name);
        this = next;
    }
    endtime=gettime();
    db->sortedarray->unsorted_search=(double)(endtime-starttime)/1E6;
    printf("Search of the unsorted array list took %.5f sec\n", db->sortedarray->unsorted_search);
    
    /* set first node again*/
    this = db->searchlist;
    
    /* search unsorted array for each node in the search list */
    starttime=gettime();
    while(this)
    {
        next = this->next;
        search_sorted_array(db, this->name);
        this = next;
    }
    endtime=gettime();
    db->sortedarray->sorted_search=(double)(endtime-starttime)/1E6;
    printf("Search of the sorted array list took %.5f sec\n", db->sortedarray->sorted_search);
    
    /* set first node again*/
    this = db->searchlist;
    
    /* search hash table for each node in the search list */
    starttime=gettime();
    while(this)
    {
        next = this->next;
        search_hash(db, this->name);
        this = next;
    }
    endtime=gettime();
    db->hashtable->unsorted_search=(double)(endtime-starttime)/1E6;
    printf("Search of the unsorted hash table took %.5f sec\n", db->hashtable->unsorted_search);
    
    /* set first node again*/
    this = db->searchlist;
    
    /* search sorted hash table for each node in the search list */
    starttime=gettime();
    while(this)
    {
        next = this->next;
        search_sorted_hash(db, this->name);
        this = next;
    }
    endtime=gettime();
    db->hashtable->sorted_search=(double)(endtime-starttime)/1E6;
    printf("Search of a sorted hash table took %.5f sec\n", db->hashtable->sorted_search);
    
}