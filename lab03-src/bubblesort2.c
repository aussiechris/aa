#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/time.h>

typedef struct node_t
{
  int value;
  struct node_t *next;
} node_t;
typedef struct
{
  int count;
  struct node_t *head;
  struct node_t *tail;

} list_t;

unsigned long
gettime()
{
  struct timeval tp;
  gettimeofday(&tp,NULL);
  return (tp.tv_sec*1E6) + tp.tv_usec;
}

void
list_init (list_t * plst)
{
  plst->count = 0;
  plst->head = NULL;
}

void
list_free (list_t * plst)
{
  node_t *current;
  node_t *next;

  /* Deallocate memory used by list nodes */
  current = plst->head;
  while (current != NULL)
    {
      next = current->next;
      free (current);
      current = next;
    }
}

void
list_print (list_t * plst)
{
  node_t *current;

  current = plst->head;
  while (current != NULL)
    {
      fprintf (stdout, "%d\n", current->value);
      current = current->next;
    }
}

void
list_input (list_t * plst)
{
  int num;
  node_t *new;
  new = NULL;
  while (fscanf (stdin, "%d", &num) == 1)
    {
      if ((new = malloc (sizeof (node_t))) == NULL)
	{
	  fprintf (stderr, "Memory allocation for list insert failed! "
		   "Aborting data entry !\n");
	  exit (EXIT_FAILURE);
	}
      new->value = num;
      new->next = plst->head;
      plst->head = new;
      plst->count++;
    }
}

int
compare (node_t * t1, node_t * t2)
{
  int tmp;
  if (t2->value > t1->value)
    {
      tmp = t2->value;
      t2->value = t1->value;
      t1->value = tmp;
      return 1;
    }
  return 0;
}

void
bubbleSort2 (list_t * plst)
{
  node_t *temp1, *temp2;

  int len;
  int len2;
  int swapped;
  int tmp;

  len = plst->count;
  do
    {
      swapped = 0;
      temp2 = plst->head;
      temp1 = plst->head;
      for (len2 = 0; len2 < len - 1; len2++)
	{
	  temp2 = temp1;
	  temp1 = temp1->next;
	  tmp = compare (temp1, temp2);

	  if (tmp == 1)
	    swapped = 1;
	}
      len--;
    }
  while (swapped);
}

int
main ()
{

  unsigned long start, end;
  list_t lst;

  list_init (&lst);

  list_input (&lst);

  start = gettime();
  bubbleSort2 (&lst);
  end = gettime();

  list_print (&lst);
  printf("time taken = %.2f sec\n", (double)(end-start)/1E6);

  list_free (&lst);

  return EXIT_SUCCESS;
}
