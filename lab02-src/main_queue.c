#include <stdio.h>
#include <stdlib.h>

#include "queue.h"

static int
get_next_operation(FILE * fp, int *number)
{
	char            buffer[INPUT_BUFFER_SIZE];

	if (fscanf(fp, "%s", buffer) == 1) {
		if (buffer[0] == '*') {
			return OP_QUEUE_DEQUEUE;
		} else if (sscanf(buffer, "%d", number) == 1) {
			return OP_QUEUE_ENQUEUE;
		} else {
			fprintf(stderr, "Do not recognize %s\n", buffer);
			exit(EXIT_FAILURE);
		}
	} else {
		if (feof(fp)) {
			return OP_QUEUE_END;
		} else {
			fprintf(stderr, "IO error.\n");
			exit(EXIT_FAILURE);
		}
	}
}

int 
main()
{
	queue_t         que;
	int             num;
	int             op;

	queue_init(&que);

  
	while ((op = get_next_operation(stdin, &num)) != OP_QUEUE_END) {
		if (op == OP_QUEUE_ENQUEUE) {
			queue_enqueue(&que, num);
		} else if (op == OP_QUEUE_DEQUEUE) {
			if (queue_is_empty(&que)) {
				printf("DEQUEUE from empty queue.\n");
			} else {
				printf("%d\n", queue_dequeue(&que));
			}
		} else {
			fprintf(stderr, "Unexpected operation.\n");
			queue_free(&que);
			exit(EXIT_FAILURE);
		}
	}

	queue_free(&que);
	return EXIT_SUCCESS;
}

