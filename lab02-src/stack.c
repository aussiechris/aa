#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#include "stack.h"

void
stack_init(stack_t * pst, size_t size)
{
	/* Allocate and check memory for stack */
	assert(pst != NULL);
	pst->items = malloc(sizeof(int) * size);
	assert(pst->items != NULL);
	
	/* Initialise stack_t values */
	pst->size = size;
	pst->top=0;
}

void
stack_push(stack_t * pst, int item)
{
	assert(pst != NULL);
	if (pst->top == (int)pst->size)
	{
		/* allocate more memory of size INITIAL_STACK_SIZE */
		pst->items = realloc(pst->items, sizeof(int) * (pst->size += INITIAL_STACK_SIZE));
		
	}
	assert(pst->items != NULL);
	
	/* push value to stack  */
	pst->items[pst->top++]=item;
}

int
stack_pop(stack_t * pst)
{
	assert(pst != NULL);
	
	/* pop value from stack */
	return pst->items[--pst->top];
}

int
stack_is_empty(stack_t * pst)
{
	assert(pst != NULL);
	
	if (pst->top==0)
		return STACK_EMPTY;
	return STACK_NOT_EMPTY;
}

void
stack_free(stack_t * pst)
{
	assert(pst != NULL);
	
	free(pst->items);

}

