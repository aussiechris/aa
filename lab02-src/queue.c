#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#include "queue.h"

void
queue_init(queue_t * pque)
{
	assert(pque != NULL);
	/* initialise queue header */
	pque->count=0;
	pque->front=NULL;
	pque->rear=NULL;
}

void
queue_enqueue(queue_t * pque, int item)
{
	assert(pque != NULL);
	
	if (pque->count==0)
	{
		pque->front=malloc(sizeof(node_t));
		pque->rear=pque->front;
		assert(pque->rear != NULL);
		pque->front->previous=NULL;
		/*
		printf("Created first node\n");*/
		
	} else {
		pque->rear->next=malloc(sizeof(node_t));
		assert(pque->rear->next != NULL);
		pque->rear->next->previous=pque->rear;
		pque->rear=pque->rear->next;
		/*
		printf("Created node %i\n", pque->count+1);*/
	}
	pque->rear->next=NULL;
	pque->rear->value=item;
	pque->count++;
}

int
queue_dequeue(queue_t * pque)
{
	int value=pque->front->value;
	assert(pque != NULL);
	if (pque->count == 1)
	{
		free(pque->front);
		pque->front=NULL;
		pque->rear=NULL;
	
	} else {
		pque->front=pque->front->next;
		free(pque->front->previous);
		pque->front->previous=NULL;
	}
	pque->count--;
	/*
	printf("Dequeued node. %i remaining...\n", pque->count);*/
	return value;
}

int
queue_is_empty(queue_t * pque)
{
	assert(pque != NULL);
	
	if (pque->count==0)
	{
		return 1;
	} else {
		return 0;
	}
}

void
queue_free(queue_t * pque)
{
	assert(pque != NULL);
	while (pque->count)
	{
		queue_dequeue(pque);
	}
}

