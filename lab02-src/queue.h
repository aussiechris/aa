/* queue.h */

#ifndef QUEUE_H

#define INPUT_BUFFER_SIZE 100

#define OP_QUEUE_ENQUEUE	0
#define OP_QUEUE_DEQUEUE	1
#define OP_QUEUE_END		2

typedef struct node_t {
	int             value;
	struct node_t  *next;
	struct node_t  *previous;
}               node_t;

typedef struct {
	int             count;
	struct node_t  *front;
	struct node_t  *rear;
}               queue_t;

void            queue_init(queue_t *);
int             queue_is_empty(queue_t *);
void            queue_enqueue(queue_t *, int);
int             queue_dequeue(queue_t *);
void            queue_free(queue_t *);

#endif
