#ifndef _LIBMATCH_H_
#define _LIBMATCH_H_

#ifdef __cplusplus
extern "C" {
#endif /** cplusplus **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/inttypes.h>

#define GETOPT_FINISHED -1

void print_usage (const char *program);

int match_alphacount(const char* text,int size);

int match_brute(const char* T,int n,const char* P,int m);
int match_horspool(const char* T,int n,const char* P,int m);
int match_kmp(const char* T,int n,const char* P,int m);
int match_bndm(const char* T,int n,const char* P,int m);
int match_shiftor(const char* T,int n,const char* P,int m);


#ifdef __cplusplus
}
#endif /** cplusplus **/

#endif 

