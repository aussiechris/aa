#include "libmatch.h"

#include <sys/time.h>
#include <math.h>

extern hrtime_t gethrtime();

enum mode_t
{
  UNKNOWN,
  BRUTE,
  HORSPOOL,
  KMP,
  BNDM,
  SHIFTOR
};

int
main(int argc, char **argv)
{
	FILE           *in;
	char           *filename;
    char           *pattern;
    char           *text;
	hrtime_t        start;
	hrtime_t        end;
    mode_t          algorithm;
    int             size;
    int             matches;
    int             opt;
    
    algorithm = UNKNOWN;
    filename = NULL;
    pattern = NULL;
    opt = GETOPT_FINISHED;
    /* parse command line options */
	while ((opt = getopt(argc, argv, "a:f:p:h")) != GETOPT_FINISHED) {
		switch (opt) {
		case 'a':
			if (strcmp(optarg, "brute") == 0) {
				algorithm = BRUTE;
			} else if (strcmp(optarg, "horspool") == 0) {
				algorithm = HORSPOOL;
			} else if (strcmp(optarg, "kmp") == 0) {
				algorithm = KMP;
			} else if (strcmp(optarg, "shiftor") == 0) {
				algorithm = SHIFTOR;
			} else if (strcmp(optarg, "bndm") == 0) {
				algorithm = BNDM;
			} else {
				fprintf(stderr, "ERROR: mode <%s> unknown!\n", optarg);
                print_usage(argv[0]);
				exit(EXIT_FAILURE);
			}
			break;
        case 'f':
            filename = optarg;
            break;
        case 'p':
            pattern = optarg;
            break;
		case 'h':
		default:
			print_usage(argv[0]);
			exit(EXIT_FAILURE);
		}
	}

    if(filename == NULL || pattern == NULL || algorithm == UNKNOWN ) {
        fprintf(stderr, "ERROR: insufficient parameters!\n");
        print_usage(argv[0]);
        exit(EXIT_FAILURE);
    }
    
	/* read the file */
	if ((in = fopen(filename, "r")) == NULL) {
		perror("fopen() input file");
		exit(EXIT_FAILURE);
	} else {
        fseek(in,0,SEEK_END);
        size = ftell(in);
        fseek(in,0,SEEK_SET);
        
        if( (text=malloc(size*sizeof(char))) == NULL ) {
            perror("malloc() input file");
            exit(EXIT_FAILURE);
        }
        
        if( fread(text,sizeof(char),size,in) != size ) {
            perror("read input file");
            exit(EXIT_FAILURE);
        }
        fclose(in);
	}
    
    /* output stats */
    fprintf(stdout,"textfile %s length %d alphabet size: %d\n",filename,size,
                match_alphacount(text,size));
    fprintf(stdout,"pattern %s length %d alphabet size: %d\n",pattern,
            strlen(pattern),match_alphacount(pattern,strlen(pattern)));
            
    /* run algorithm */
    start = gethrtime();
    
    switch(algorithm) {
        case BRUTE:
            matches = match_brute(text,size,pattern,strlen(pattern));
            break;
        case HORSPOOL:
            matches = match_horspool(text,size,pattern,strlen(pattern));
            break;
        case KMP:
            matches = match_kmp(text,size,pattern,strlen(pattern));
            break;
        case BNDM:
            matches = match_bndm(text,size,pattern,strlen(pattern));
            break;
        case SHIFTOR:
            matches = match_shiftor(text,size,pattern,strlen(pattern));
            break;
    }
    
    end = gethrtime();

    fprintf(stdout,"matches: %d\n",matches);
    fprintf(stdout,"Time taken: %.2f msec\n",((float)(end-start))/1000000);
    
	/* clean up the mess */
    free(text);
    
	return (EXIT_SUCCESS);
}
