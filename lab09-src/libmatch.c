#include "libmatch.h"

/*** basic command line parsing ***/
void
print_usage(const char *program)
{
	fprintf(stderr, "USAGE: %s [options] -t <textfile> -p <pattern>\n", program);
	fprintf(stderr, "  -a method [brute, horspool, kmp, shiftor]\n");
    fprintf(stderr, "  -f file to search in\n");
    fprintf(stderr, "  -p pattern\n");
	fprintf(stderr, "  -h Display usage information\n");
	fprintf(stderr, "\n");
	fprintf(stderr, "EXAMPLE: %s -a brute -f test1.dat -p mississipi\n",
		program);
	fprintf(stderr, "\n");
	return;
}

/* alphabet count */
int
match_alphacount(const char* text,int size)
{
    int alphacount;
    int seen[256];
    int i;
    memset(seen,0,256*sizeof(int));
    alphacount = 0;
    for(i=0;i<size;i++) {
        if( seen[ text[i] ] == 0 ) {
            alphacount++;
            seen[ text[i] ] = 1;
        }
    }
    return alphacount;
}

/*** the match functions ***/

/* perform the brute force algorithm  */
int
match_brute(const char* T,int n,const char* P,int m)
{
    int matches;
    int i,j;
    
    fprintf(stderr,"TODO: match_brute() PLEASE IMPLEMENT ME!\n");
    
    return matches;
}

/* perform the horspool algorithm  */
int
match_horspool(const char* T,int n,const char* P,int m)
{
    int matches;
    int i,k;
    int* S;
    matches = 0;
    
    fprintf(stderr,"TODO: match_horspool() PLEASE IMPLEMENT ME!\n");

    return matches;
}

/* perform the kmp algorithm  */
int*
kmp_table(const char* P,int m)
{
    int* S;
    int i,k;
    
    if( (S=malloc((m+1)*sizeof(int))) == NULL) {
        perror("kmp malloc()");
        exit(EXIT_FAILURE);
    }
    
    i=0;
    k=-1;
    S[i]=k;
    while (i<m)
    {
        while (k>=0 && P[i]!=P[k]) k=S[k];
        i++;k++;
        S[i]=k;
    }
    return S;
}

int
match_kmp(const char* T,int n,const char* P,int m)
{
    int matches;
    int i,k;
    matches = 0;
    int* S;
    
    /* preprocessing*/
    S = kmp_table(P,m);
    
    /* matching */
    i = 0;
    k = 0;
    while( i <= n - 1 ) {
        
        while (k>=0 && T[i]!=P[k]) k=S[k];
        i++;k++;
        
        if(k==m)  {
            matches++;
            k = S[k]; /* shift */
        }
    }
    
    free(S);
    return matches;
}

/* perform the shiftor algorithm  */
uint32_t*
shiftor_table(const char* P,int m,uint32_t* limit)
{
    uint32_t j, lim,i;
    uint32_t* S;
    
    if( (S=malloc(256*sizeof(uint32_t))) == NULL) {
        perror("shiftor malloc()");
        exit(EXIT_FAILURE);
    }
    
    for (i = 0; i < 256; i++) S[i] = ~0;

    lim = i = 0;
    j = 1;
    for (i = 0;i<m;i++) { 
      S[P[i]] &= ~j; 
      lim |= j; 
      j <<= 1;
    }
    
    lim = ~(lim>>1); 
    
    *limit = lim;
    return S; 
}

int
match_shiftor(const char* T,int n,const char* P,int m)
{
    uint32_t limit,state,i,j;
    int matches;
    matches = 0;
    uint32_t* S;
    
    if(m>32) {
        fprintf(stderr,"pattern size > 32 not supported!\n");
        exit(EXIT_FAILURE);
    }
    
    /* preprocessing*/
    S = shiftor_table(P,m,&limit);
    
    /* matching */
    state = ~0;
    for(j=0;j<n;j++) {
        state = (state<<1) | S[T[j]]; /* shift or */
        if(state < limit) matches++;
    }
    
    free(S);
    return matches;
}

/* perform the bndm algorithm  */
int*
bndm_table(const char* P,int m)
{
    int i,s;
    int* S;
    
    if( (S=malloc(256*sizeof(int))) == NULL) {
        perror("bndm malloc()");
        exit(EXIT_FAILURE);
    }
    
    memset(S,0,256*sizeof(int));
    
    s = 1;
    for(i=m-1;i>=0;i--) {
        S[P[i]] |= s;
        s <<= 1;
    }
    return S;
}

int
match_bndm(const char* T,int n,const char* P,int m)
{
    int i,j,d,last;
    int matches;
    matches = 0;
    int* S;
    
    if(m>32) {
        fprintf(stderr,"pattern size > 32 not supported!\n");
        exit(EXIT_FAILURE);
    }
    
    /* preprocessing*/
    S = bndm_table(P,m);
    
    /* matching */
    j = 0;
    while(j <= n-m) {
        i=m-1; last=m;
        d = ~0;
        while (i>=0 && d!=0) {
            d &= S[T[j+i]];
            i--;
            if(d!=0) {
                if(i>=0) last = i+1;
                else matches++;
            }
            d <<= 1;
        }
        j += last;
    }

    free(S);
    return matches;
}

