#ifndef _BST_H
#define _BST_H

#define END  			0
#define INSERT			1
#define PRINT_INORDER		2
#define PRINT_PREORDER		3
#define PRINT_POSTORDER		4
#define HEIGHT			5
#define SEARCH			6
#define REMOVE			7
#define MAX			8
#define MIN			9
#define PRINT_ASCII 	   	10

typedef struct bst_t {
	int value;
	struct bst_t* left;
	struct bst_t* right;
	struct bst_t* parent;
} bst_t;

bst_t* bst_insert(bst_t*, int);
bst_t* bst_remove(bst_t*, int);
void bst_free(bst_t*);
void bst_print_inorder(bst_t*);
void bst_print_preorder(bst_t*);
void bst_print_postorder(bst_t*);
bst_t*  bst_search(bst_t*, int);
bst_t*  bst_min(bst_t*);
bst_t*  bst_max(bst_t*);
int bst_height(bst_t*);


#endif /* _BST_H */
