#include <stdio.h>
#include <stdlib.h>
#include <readline/readline.h>
#include <readline/history.h>

#include "bst.h"
#include "print_ascii.h"

#define INPUT_BUFFER_SIZE 100

void
print_help(FILE* fp)
{
    fprintf(fp,"Available commands:\n");
    fprintf(fp,"    insert <number>\n");
    fprintf(fp,"    remove <number>\n");
    fprintf(fp,"    search <number>\n");
    fprintf(fp,"    min\n");
    fprintf(fp,"    max\n");
    fprintf(fp,"    height\n");
    fprintf(fp,"    inorder\n");
    fprintf(fp,"    preorder\n");
    fprintf(fp,"    postorder\n");
    fprintf(fp,"    print_ascii\n");
    fprintf(fp,"    end/exit/quit\n");
}

static int
get_next_operation(FILE *fp, int *number)
{
    char buffer[INPUT_BUFFER_SIZE];
    char command[INPUT_BUFFER_SIZE];
    int op = -1;
	
    while( op == -1 && fgets(buffer,INPUT_BUFFER_SIZE,fp) != NULL ) {
        if( sscanf(buffer,"%s",command) == 1 ) {

            if( strncmp(command,"insert",6) == 0 ) {
               if( sscanf(buffer,"%s %d",command,number) == 2)
                    op = INSERT;
            }
            else if( strncmp(command,"remove",6) == 0 ) {
               if( sscanf(buffer,"%s %d",command,number) == 2)
                    op = REMOVE;
            }
            else if( strncmp(command,"search",6) == 0 ) {
               if( sscanf(buffer,"%s %d",command,number) == 2)
                    op = SEARCH;
            }
            else if( strncmp(command,"min",3) == 0 ) op = MIN;
            else if( strncmp(command,"max",3) == 0 ) op = MAX;
            else if( strncmp(command,"end",3) == 0 ) op = END;
            else if( strncmp(command,"exit",4) == 0 ) op = END;
            else if( strncmp(command,"quit",4) == 0 ) op = END;
            else if( strncmp(command,"height",6) == 0 ) op = HEIGHT;
            else if( strncmp(command,"inorder",7) == 0 ) op = PRINT_INORDER;
            else if( strncmp(command,"preorder",8) == 0 ) op = PRINT_PREORDER;
            else if( strncmp(command,"postorder",9) == 0 ) op = PRINT_POSTORDER;
            else if( strncmp(command,"print_ascii",11) == 0 ) op = PRINT_ASCII;
	        else
	        {
                print_help(fp);
	        }
        }
        if(op == -1 ) {
		    fprintf(stderr, "Did not recognize command. Enter valid command.\n");
        }
	}
    return op;
}

int
main(int argc, char **argv)
{
	int op;
	int number;
	bst_t* bst;
	bst_t* tmp;

    	print_help(stdout);

	bst = NULL;
	while ((op = get_next_operation(stdin, &number)) != END)
	{
		if (op == INSERT) {
            bst = bst_insert(bst, number);
        }
		else if (op == REMOVE) {
            bst = bst_remove(bst, number);
        }
		else if (op == SEARCH) {
            tmp = bst_search(bst, number);
            if(tmp) fprintf(stdout,"Found (%d)\n",number);
            else fprintf(stdout,"Not Found (%d)\n",number);
        }
		else if (op == PRINT_INORDER) {
            bst_print_inorder(bst);
        }
		else if (op == PRINT_PREORDER) bst_print_preorder(bst);
		else if (op == PRINT_POSTORDER) bst_print_postorder(bst);
		else if (op == HEIGHT) { 
            fprintf(stdout,"Height (%d)\n", bst_height(bst));
        }
		else if (op == MIN) {
			bst_t* tmp = bst_min(bst);
			if(tmp) fprintf(stdout,"Min (%d)\n", tmp->value);
			else fprintf(stdout,"Min (N/A)\n");
		}
		else if (op == MAX) {
			bst_t* tmp = bst_max(bst);
			if(tmp) fprintf(stdout,"Max (%d)\n", tmp->value);
			else fprintf(stdout,"Max (N/A)\n");
		}
		else if (op == PRINT_ASCII) print_ascii_tree(bst);
		else { fprintf(stderr, "Unexpected operation.\n"); exit(EXIT_FAILURE); }
	}
	
	bst_free(bst);
		
	return EXIT_SUCCESS;
}

