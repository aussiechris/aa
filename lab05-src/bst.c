#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#include "bst.h"

bst_t*
bst_insert(bst_t* pbst, int new)
{
    if (pbst == NULL) {
        pbst = malloc(sizeof(struct bst_t));
        if (pbst == NULL) {
            perror("bst_insert: malloc");
            exit(EXIT_FAILURE);
        }
        pbst->value =  new;
        pbst->left = NULL;
        pbst->right = NULL;
    }
    else if (new < pbst->value)
        pbst->left = bst_insert(pbst->left, new);
    else if (new > pbst->value)
        pbst->right = bst_insert(pbst->right, new);

    return pbst;
}

bst_t*
bst_remove(bst_t* pbst, int del)
{
	fprintf(stderr, "bst_remove() NYI.\n");
	exit(EXIT_FAILURE);
}

void 
bst_free(bst_t* pbst)
{
    if (pbst != NULL) {
        bst_free(pbst->left);
        bst_free(pbst->right);
        free(pbst);
    }
}

void 
bst_print_inorder(bst_t* pbst)
{
    if(pbst) {
        bst_print_inorder(pbst->left);
        fprintf(stdout,"%d\n",pbst->value);
        bst_print_inorder(pbst->right);
    }
}

void 
bst_print_preorder(bst_t* pbst)
{
	fprintf(stderr, "bst_print_preorder() NYI.\n");
	exit(EXIT_FAILURE);
}

void 
bst_print_postorder(bst_t* pbst)
{
	fprintf(stderr, "bst_print_postorder() NYI.\n");
	exit(EXIT_FAILURE);
}

bst_t* 
bst_search(bst_t* pbst, int qry)
{
	fprintf(stderr, "bst_search() NYI.\n");
	exit(EXIT_FAILURE);
}

bst_t* 
bst_min(bst_t* pbst)
{
	fprintf(stderr, "bst_min() NYI.\n");
	exit(EXIT_FAILURE);
}

bst_t* 
bst_max(bst_t* pbst)
{
	fprintf(stderr, "bst_max() NYI.\n");
	exit(EXIT_FAILURE);
}

int 
bst_height(bst_t* pbst)
{
	fprintf(stderr, "bst_height() NYI.\n");
	exit(EXIT_FAILURE);
}

