#!/bin/sh
echo "Adding new files to the repository"
git add .
echo "...Add done!"

echo "Commiting changes"
echo "Commit named: $@"
git commit -a -m $@
echo "...Commit done!"

echo "Pushing changes to the repository"
echo "You will need to enter the password"
git push
echo "...Push done!"

echo "All Tasks Complete!"
echo "Please check for any errors :D"
